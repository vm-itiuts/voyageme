﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VoyageMe.BLL.Services.BookStrategies;
using VoyageMe.Common.Models;

namespace VoyageMe.BLL.Factories
{
    public class BookStrategyFactory
    {
        #region Private Instance Fields
        private readonly IServiceProvider _serviceProvider;
        #endregion
        public BookStrategyFactory(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public IBookStrategy Create(BookType bookType)
        {
            IBookStrategy result;
            switch (bookType)
            {
                case BookType.Auto:
                    result = ActivatorUtilities.CreateInstance<AutoBookStrategy>(_serviceProvider);
                    break;
                case BookType.Manually:
                    result = ActivatorUtilities.CreateInstance<ManualBookStrategy>(_serviceProvider);
                    break;
                case BookType.WithCode:
                    result = ActivatorUtilities.CreateInstance<WithCodeBooKStrategy>(_serviceProvider);
                    break;
                default:
                    throw new ArgumentException();
            }

            return result;
        }
    }
}
