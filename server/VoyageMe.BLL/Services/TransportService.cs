﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VoyageMe.BLL.Abstract.Services;
using VoyageMe.Common.Models;
using VoyageMe.DAL.Abstract.Repositories;

namespace VoyageMe.BLL.Services
{
    public class TransportService : ITransportService
    {
        private readonly ITransportRepository _transportRepo;
        private readonly ITransportImageRepository _transportImageRepo;

        public TransportService(
            ITransportRepository transportRepo,
            ITransportImageRepository transportImageRepo)
        {
            _transportRepo = transportRepo;
            _transportImageRepo = transportImageRepo;
        }

        public async Task<Transport> CreateTransportAsync(Transport transport)
        {
            using (IRepositoryTransaction transaction = _transportRepo.BeginTransaction())
            {
                try
                {
                    await Task.Run(() =>
                    {
                        _transportRepo.Insert(transport, transaction);
                        foreach (TransportImage image in transport.Images)
                        {
                            image.TransportId = transport.Id;
                            _transportImageRepo.Insert(image, transaction);
                        }
                    });
                    transaction.Commit();
                }
                catch (Exception)
                {
                    transaction.Rollback();
                    throw;
                }
            }

            return transport;
        }

        public async Task<Transport> UpdateTransportAsync(Transport transport)
        {
            using (IRepositoryTransaction transaction = _transportRepo.BeginTransaction())
            {
                try
                {
                    await Task.Run(() =>
                    {
                        _transportRepo.Update(transport, transaction);
                        //TODO: implement more efficient way to update images
                        IEnumerable<TransportImage> oldImages = _transportImageRepo
                            .FindAll(x => x.TransportId == transport.Id, transaction);
                        foreach (TransportImage image in oldImages)
                        {
                            _transportImageRepo.Delete(image.Id, transaction);
                        }
                        foreach (TransportImage image in transport.Images)
                        {
                            image.TransportId = transport.Id;
                            _transportImageRepo.Insert(image, transaction);
                        }
                    });
                    transaction.Commit();
                }
                catch (Exception)
                {
                    transaction.Rollback();
                    throw;
                }
            }

            return transport;
        }

        public Task DeleteTransportAsync(int id)
        {
            return _transportRepo.DeleteAsync(id);
        }

        public Task<IEnumerable<Transport>> GetUserTransportsAsync(int userId)
        {
            return _transportRepo.FindAllAsync<TransportImage>(x => x.OwnerId == userId, x => x.Images);
        }
    }
}
