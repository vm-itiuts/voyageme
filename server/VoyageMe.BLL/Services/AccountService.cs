﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

using VoyageMe.Common.Models;
using VoyageMe.DAL.Abstract.Repositories;
using VoyageMe.BLL.Abstract.Services;
using VoyageMe.Common.Models.Security;

namespace VoyageMe.BLL.Services
{
    public class AccountService : IAccountService
    {
        private readonly IAccountRepository _accountRepo;

        public AccountService(IAccountRepository accountRepo)
        {
            _accountRepo = accountRepo;
        }

        #region Public instance methods
        public Task<Account> GetAccountWithUserByEmailAsync(string email)
        {
            if (email == null)
            {
                throw new ArgumentNullException(nameof(email));
            }

            Task<Account> account = this.FindAccountAsync(a => a.Email == email, true);

            return account;
        }

        public Task<Account> GetAccountWithUserByIdAsync(int id)
        {
            Task<Account> account = this.FindAccountAsync(a => a.Id == id, true);

            return account;
        }

        public Task<Account> GetAccountWithUserByExternalIdAsync(string externalId, ExternalAuthenticationProvider provider)
        {
            if (externalId == null)
            {
                throw new ArgumentNullException(nameof(externalId));
            }

            Expression<Func<Account, bool>> predicate = account => account.ExternalId == externalId && account.ExternalAuthProviderId == (int)provider;
            Task<Account> result = this.FindAccountAsync(predicate, true);

            return result;
        }

        public async Task<List<Permission>> GetPermissionListAsync(int accountId)
        {
            List<Permission> permissions =
                (await _accountRepo.FindAsync<Permission>(a => a.Id == accountId, a => a.Permissions)).Permissions;

            return permissions;
        }

        public Task AddExternalAccountToExisting(Account existingAccount, string externalId, ExternalAuthenticationProvider provider)
        {
            if (existingAccount == null)
            {
                throw new ArgumentNullException(nameof(existingAccount));
            }

            existingAccount.ExternalId = externalId;
            existingAccount.ExternalAuthProvider = provider;

            return _accountRepo.UpdateAsync(existingAccount);
        }
        #endregion Public instance methods

        #region Private instance methods
        private Task<Account> FindAccountAsync(Expression<Func<Account, bool>> predicate, bool includeUser)
        {
            if (predicate == null)
            {
                throw new ArgumentNullException(nameof(predicate));
            }

            Task<Account> account = includeUser
                ? _accountRepo.FindAsync<User>(predicate, a => a.User)
                : _accountRepo.FindAsync(predicate);

            return account;
        }
        #endregion Private instance methods
    }
}
