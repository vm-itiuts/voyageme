﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VoyageMe.BLL.Abstract.Services;
using VoyageMe.Common.Models;
using VoyageMe.Common.Pagination;
using VoyageMe.DAL.Abstract.Repositories;

namespace VoyageMe.BLL.Services
{
    public class FeedbackService : IFeedbackService
    {
        private readonly IPendingFeedbackRepository _pendingFeedbackRepo;
        private readonly IFeedbackRepository _feedbackRepo;
        private readonly IUserRepository _userRepo;
        private readonly IRouteRepository _routeRepo;

        public FeedbackService(
            IPendingFeedbackRepository pendingFeedbackRepo,
            IFeedbackRepository feedbackRepo,
            IUserRepository userRepo,
            IRouteRepository routeRepo)
        {
            _pendingFeedbackRepo = pendingFeedbackRepo;
            _feedbackRepo = feedbackRepo;
            _userRepo = userRepo;
            _routeRepo = routeRepo;
        }

        public async Task<Feedback> CreateFeedbackAsync(Feedback feedback)
        {
            using (IRepositoryTransaction transaction = _feedbackRepo.BeginTransaction())
            {
                try
                {
                    PendingFeedback pendingFeedback = await _pendingFeedbackRepo
                        .FindAsync(x => x.RouteId == feedback.RouteId
                            && x.PassengerId == feedback.PassengerId
                            && x.DriverId == feedback.DriverId, transaction);
                    await _pendingFeedbackRepo.DeleteAsync(pendingFeedback.Id, transaction);
                    feedback.AddDate = DateTime.UtcNow;
                    await _feedbackRepo.InsertAsync(feedback, transaction);
                    feedback.Route = await _routeRepo.FindAsync(x => x.Id == feedback.RouteId, transaction);
                    transaction.Commit();
                }
                catch (Exception)
                {
                    transaction.Rollback();
                    throw;
                } 
            }

            return feedback;
        }

        public async Task SaveFeedbacksAsLooked(IEnumerable<PendingFeedback> feedbacks)
        {
            foreach (PendingFeedback feedback in feedbacks)
            {
                feedback.Looked = true;
                await _pendingFeedbackRepo.UpdateAsync(feedback);
            }
        }

        public Task<bool> DeletePendingFeedbackAsync(int id)
        {
            return _pendingFeedbackRepo.DeleteAsync(id);
        }

        public Task<IEnumerable<Feedback>> GetDriverFeedbacksAsync(int driverId, Pagination pagination)
        {
            return _feedbackRepo.FindAllAsync<User>(x => x.DriverId == driverId, x => x.Passenger, pagination);
        }

        public Task<IEnumerable<PendingFeedback>> GetPendingFeedbacksAsync(int userId, Pagination pagination)
        {
            return _pendingFeedbackRepo.FindAllAsync<Route>(x => x.PassengerId == userId && x.Looked == false,
                x => x.Route, pagination);
        }
    }
}
