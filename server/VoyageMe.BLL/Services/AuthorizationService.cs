﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using VoyageMe.Common.Models;
using VoyageMe.Common.Validation;
using VoyageMe.Common.Validation.Errors;
using VoyageMe.DAL.Abstract.Repositories;
using VoyageMe.BLL.Abstract.Services;

namespace VoyageMe.BLL.Services
{
    public class AuthorizationService : IAuthorizationService
    {
        private readonly ISessionRepository _sessionRepository;

        public AuthorizationService(ISessionRepository sessionRepository)
        {
            _sessionRepository = sessionRepository;
        }

        #region Public instance methods
        public Task CreateSession(int accountId, string accessToken, string refreshToken)
        {
            #region Input parameters check
            if (refreshToken == null)
            {
                throw new ArgumentNullException(nameof(refreshToken));
            }

            if (accessToken == null)
            {
                throw new ArgumentNullException(nameof(accessToken));
            }
            #endregion Input parameters check

            DateTime now = DateTime.UtcNow;

            Session session = new Session()
            {
                AccessToken = accessToken,
                RefreshToken = refreshToken,
                AccountId = accountId,
                AddDate = now,
                RefreshTokenExpirationDate = now.AddMonths(3), // TODO: constant
                SessionActive = true
            };

            return _sessionRepository.InsertAsync(session);
        }

        public async Task<AuthSessionValidationResult> ValidateSessionAsync(string accessToken, string refreshToken)
        {
            #region Input parameters check
            if (refreshToken == null)
            {
                throw new ArgumentNullException(nameof(refreshToken));
            }

            if (accessToken == null)
            {
                throw new ArgumentNullException(nameof(accessToken));
            }
            #endregion Input parameters check

            AuthError error = null;

            Session session = 
                await _sessionRepository.FindAsync<Account>(record => record.RefreshToken == refreshToken && record.AccessToken == accessToken, record => record.Account);

            if (session != null)
            {
                if (session.SessionActive == false)
                {
                    error = AuthError.CreateRefreshTokenAlreadyUsedError();
                }
                else if (session.RefreshTokenExpirationDate < DateTime.UtcNow)
                {
                    error = AuthError.CreateRefreshTokenExpiredError();
                    await this.CloseSessionAsync(session);
                }
            }
            else
            {
                error = AuthError.CreateRefreshAndAccessTokensMistatchError();
            }

            AuthSessionValidationResult validationResult = error == null 
                ? AuthSessionValidationResult.CreateSuccess(session) 
                : AuthSessionValidationResult.CreateError(error);

            return validationResult;
        }

        public async Task CloseSessionAsync(int accountId, string accessToken)
        {
            Session session = await this.GetSessionAsync(accountId, accessToken);
            await this.CloseSessionAsync(session);
        }

        public Task CloseSessionAsync(Session session)
        {
            #region Input parameters check
            if (session == null)
            {
                throw new ArgumentNullException(nameof(session));
            }
            #endregion Input parameters check

            session.SessionActive = false;
            return _sessionRepository.UpdateAsync(session);
        }

        public async Task CloseAllSessionAsyncFor(int accountId)
        {
            IEnumerable<Session> activeSessions = await _sessionRepository.FindAllAsync(session => session.AccountId == accountId && session.SessionActive);

            foreach(Session session in activeSessions)
            {
                session.SessionActive = false;
                await _sessionRepository.UpdateAsync(session);
            }
        }
        #endregion Public instance methods

        #region Private instance methods
        private Task<Session> GetSessionAsync(int accountId, string accessToken)
        {
            #region Input parameters check
            if (accessToken == null)
            {
                throw new ArgumentNullException(nameof(accessToken));
            }
            #endregion Input parameters check

            Task<Session> session = _sessionRepository.FindAsync(s => s.AccountId == accountId && s.AccessToken == accessToken);

            return session;
        }
        #endregion Private instance methods
    }
}
