﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VoyageMe.BLL.Abstract.Enums;
using VoyageMe.Common.Models;
using VoyageMe.DAL.Abstract.Repositories;

namespace VoyageMe.BLL.Services.BookStrategies
{
    public class WithCodeBooKStrategy : IBookStrategy
    {
        #region Private Instance Fields
        private readonly IRoutePassengersRepository _routePassengerRepo;
        #endregion

        public WithCodeBooKStrategy(IRoutePassengersRepository routePassengerRepo)
        {
            _routePassengerRepo = routePassengerRepo;
        }

        public Task<BookResult> BookAsync(RoutePassenger passenger)
        {
            return Task.Run(() =>
            {
                BookResult result = _routePassengerRepo.Insert(passenger)
                    ? BookResult.WithCode
                    : BookResult.Error;

                return result;
            });
        }

        public async Task<ConfirmResult> ConfirmAsync(int id, int? code)
        {
            ConfirmResult result;
            RoutePassenger passenger = await _routePassengerRepo.FindAsync<Route>(x => x.Id == id, x => x.Route);
            if (code.HasValue && passenger.Route.Code == code.Value)
            {
                passenger.Approved = true;
                await _routePassengerRepo.UpdateAsync(passenger);
                result = ConfirmResult.Success;
            }
            else
            {
                result = ConfirmResult.Error;
            }

            return result;
        }
    }
}
