﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VoyageMe.BLL.Abstract.Enums;
using VoyageMe.Common.Models;
using VoyageMe.DAL.Abstract.Repositories;

namespace VoyageMe.BLL.Services.BookStrategies
{
    public class ManualBookStrategy : IBookStrategy
    {
        #region Private Instance Fields
        private readonly IRoutePassengersRepository _routePassengerRepo;
        #endregion

        public ManualBookStrategy(IRoutePassengersRepository routePassengerRepo)
        {
            _routePassengerRepo = routePassengerRepo;
        }

        public Task<BookResult> BookAsync(RoutePassenger passenger)
        {
            return Task.Run(() =>
            {
                BookResult result = _routePassengerRepo.Insert(passenger)
                    ? BookResult.Manual
                    : BookResult.Error;

                return result;
            });
        }

        public Task<ConfirmResult> ConfirmAsync(int id, int? code)
        {
            return Task.Run(() =>
            {
                RoutePassenger passenger = _routePassengerRepo.Find(x => x.Id == id);
                passenger.Approved = true;

                ConfirmResult result = _routePassengerRepo.Update(passenger)
                    ? ConfirmResult.Success
                    : ConfirmResult.Error;
                return result;
            });
        }
    }
}
