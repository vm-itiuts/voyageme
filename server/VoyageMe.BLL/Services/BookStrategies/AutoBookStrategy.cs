﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VoyageMe.BLL.Abstract.Enums;
using VoyageMe.Common.Models;
using VoyageMe.DAL.Abstract.Repositories;

namespace VoyageMe.BLL.Services.BookStrategies
{
    internal class AutoBookStrategy : IBookStrategy
    {
        #region Private Instance Fields
        private readonly IRoutePassengersRepository _routePassengerRepo;
        #endregion

        public AutoBookStrategy(IRoutePassengersRepository routePassengerRepo)
        {
            _routePassengerRepo = routePassengerRepo;
        }

        public Task<BookResult> BookAsync(RoutePassenger passenger)
        {
            return Task.Run(() =>
            {
                passenger.Approved = true;
                BookResult result = _routePassengerRepo.Insert(passenger)
                    ? BookResult.Auto
                    : BookResult.Error;

                return result;
            });
        }

        public Task<ConfirmResult> ConfirmAsync(int id, int? code)
        {
            return Task.FromResult(ConfirmResult.NotSupported);
        }
    }
}
