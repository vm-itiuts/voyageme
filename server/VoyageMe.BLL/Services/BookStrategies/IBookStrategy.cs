﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VoyageMe.BLL.Abstract.Enums;
using VoyageMe.Common.Models;

namespace VoyageMe.BLL.Services.BookStrategies
{
    public interface IBookStrategy
    {
        Task<BookResult> BookAsync(RoutePassenger passenger);

        Task<ConfirmResult> ConfirmAsync(int id, int? code);
    }
}
