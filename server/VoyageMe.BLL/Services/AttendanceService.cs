﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VoyageMe.BLL.Abstract.Services;
using VoyageMe.BLL.DomainEvents.Abstract;
using VoyageMe.BLL.DomainEvents.Concrete.Events;
using VoyageMe.Common.Models;
using VoyageMe.DAL.Abstract.Repositories;

namespace VoyageMe.BLL.Services
{
    public class AttendanceService : IAttendanceService
    {
        private readonly IRoutePassengersRepository _routePassengerRepo;
        private readonly IEventManager _eventManager;

        public AttendanceService(
            IRoutePassengersRepository routePassengerRepo,
            IEventManager eventManager)
        {
            _routePassengerRepo = routePassengerRepo;
            _eventManager = eventManager;
        }

        public Task<IEnumerable<RoutePassenger>> GetPassengersAsync(int routeId)
        {
            return _routePassengerRepo.FindAllAsync<User>
                (x => x.RouteId == routeId && x.Approved.Value, x => x.Passenger);
        }

        public async Task ConfirmAttendance(int routeId, int passengerId)
        {
            RoutePassenger passenger;
            using (IRepositoryTransaction transaction = _routePassengerRepo.BeginTransaction())
            {
                try
                {
                    passenger = await _routePassengerRepo.FindAsync<Route>
                        (x => x.RouteId == routeId && x.PassengerId == passengerId, x => x.Route, transaction);
                    passenger.Attended = true;
                    await _routePassengerRepo.UpdateAsync(passenger, transaction);
                    transaction.Commit();
                }
                catch (Exception)
                {
                    transaction.Rollback();
                    throw;
                }
                await _eventManager.Raise(new PassengerAttendedEvent(passenger.PassengerId,
                                                passenger.Route.DriverId,
                                                passenger.RouteId));
            }
        }

        public async Task RejectAttendance(int routeId, int passengerId)
        {
            using (IRepositoryTransaction transaction = _routePassengerRepo.BeginTransaction())
            {
                try
                {
                    await Task.Run(() =>
                    {
                        RoutePassenger passenger = _routePassengerRepo
                            .Find(x => x.RouteId == routeId && x.PassengerId == passengerId, transaction);
                        passenger.Attended = false;
                        _routePassengerRepo.Update(passenger, transaction);
                    });
                    transaction.Commit();
                }
                catch (Exception)
                {
                    transaction.Rollback();
                    throw;
                }
                await _eventManager.Raise(new PassengerNotAttendedEvent(passengerId));
            }
        }
    }
}
