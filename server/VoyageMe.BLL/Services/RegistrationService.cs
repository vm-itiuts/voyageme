﻿using System;
using System.Threading.Tasks;

using VoyageMe.Common.Models;
using VoyageMe.BLL.Abstract.Services;
using VoyageMe.DAL.Abstract.Repositories;

namespace VoyageMe.BLL.Services
{
    public class RegistrationService : IRegistrationService
    {
        private readonly IAccountRepository _accountRepo;
        private readonly IUserRepository _userRepo;

        public RegistrationService(IAccountRepository accountRepo, IUserRepository userRepo)
        {
            if (accountRepo == null)
            {
                throw new ArgumentNullException(nameof(accountRepo));
            }

            if (userRepo == null)
            {
                throw new ArgumentNullException(nameof(userRepo));
            }

            _accountRepo = accountRepo;
            _userRepo = userRepo;
        }

        #region Public instance methods
        public async Task RegisterUserAsync(Account account, User user)
        {
            if (account == null)
            {
                throw new ArgumentNullException(nameof(account));
            }

            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }

            using (IRepositoryTransaction transaction = _accountRepo.BeginTransaction())
            {
                try
                {
                    await _accountRepo.InsertAsync(account, transaction);

                    user.Id = account.Id;
                    await _userRepo.InsertAsync(user, transaction);

                    transaction.Commit();
                }
                catch
                {
                    transaction.Rollback();
                    throw;
                }
            }
        }

        public async Task<bool> IsAccountExistsAsync(string email)
        {
            Account account = await _accountRepo.FindAsync(a => a.Email == email);

            return account != null;
        }
        #endregion Public instance methods
    }
}
