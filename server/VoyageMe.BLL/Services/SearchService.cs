﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using VoyageMe.BLL.Abstract.Services;
using VoyageMe.Common.Models;
using VoyageMe.Common.Pagination;
using VoyageMe.DAL.Abstract.Repositories;
using VoyageMe.Common.Extensions;

namespace VoyageMe.BLL.Services
{
    public class SearchService : ISearchService
    {
        private readonly IRouteSearchRepository _searchRepo;

        public SearchService(IRouteSearchRepository searchRepo)
        {
            _searchRepo = searchRepo;
        }

        public Task<IEnumerable<Route>> FindRoutesAsync(string startPlaceId, string endPlaceId,
            Pagination pagination, DateTime? date = null)
        {
            Expression<Func<Route, bool>> dateExpression = date.HasValue
                ? (Expression<Func<Route, bool>>)(x => x.StartDateTime == date.Value)
                : x => x.StartDateTime >= DateTime.UtcNow.Date;

            Expression<Func<Route, bool>> freeSeatsExpression =
                x => (x.SeatsNumber - x.Passengers
                    .Where(r => r.Approved ?? false)
                    .Sum(r => r.BookedSeats) > 0) || !x.Passengers.Any();

            Expression<Func<Route, bool>> predicate =
                x => (x.StartPlaceId == startPlaceId && x.EndPlaceId == endPlaceId
                || (x.StartPlaceId == startPlaceId && x.WayPoints.Any(w => w.PlaceId == endPlaceId))
                || (x.WayPoints.Any(w => w.PlaceId == startPlaceId) && x.EndPlaceId == endPlaceId));

            predicate = predicate.And(dateExpression).And(freeSeatsExpression);

            Task<IEnumerable<Route>> result = _searchRepo.FindAllAsync(predicate, pagination,
                x => x.WayPoints, x => x.Driver, x => x.Passengers, x => x.Transport);

            return result;
        }

        public Task<IEnumerable<Route>> FindAdditionalRoutesAsync(string startPlaceId, string endPlaceId,
            Pagination pagination, DateTime? date = null)
        {
            Task<IEnumerable<Route>> result = _searchRepo
                .FindAllAsync(route => GetRouteFromWayPoints(route, startPlaceId, endPlaceId, date),
                    pagination, x => x.WayPoints, x => x.Driver, x => x.Passengers, x => x.Transport);

            return result;
        }

        private bool GetRouteFromWayPoints(Route route, string startPlaceId, string endPlaceId, DateTime? date)
        {
            WayPoint start = route.WayPoints.FirstOrDefault(x => x.PlaceId == startPlaceId);
            WayPoint end = route.WayPoints.FirstOrDefault(x => x.PlaceId == endPlaceId);
            bool result = start != null && end != null && start.OrderNumber < end.OrderNumber
                && (date.HasValue && route.StartDateTime == date.Value || route.StartDateTime >= DateTime.UtcNow.Date)
                && (!route.Passengers.Any()
                    || route.SeatsNumber - route.Passengers
                        .Where(r => r.Approved ?? false)
                        .Sum(r => r.BookedSeats) > 0);

            return result;
        }
    }
}