﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VoyageMe.BLL.Abstract.Enums;
using VoyageMe.BLL.Abstract.Services;
using VoyageMe.BLL.Factories;
using VoyageMe.BLL.Services.BookStrategies;
using VoyageMe.Common.Models;
using VoyageMe.DAL.Abstract.Repositories;

namespace VoyageMe.BLL.Services
{
    public class BookService : IBookService
    {
        #region Non-Public Instance Fields
        private readonly IRoutePassengersRepository _routePassengerRepo;
        private readonly BookStrategyFactory _bookStrategyFactory;
        #endregion

        public BookService(
            IRoutePassengersRepository routePassengerRepo,
            BookStrategyFactory bookStrategyFactory)
        {
            _routePassengerRepo = routePassengerRepo;
            _bookStrategyFactory = bookStrategyFactory;
        }

        public Task RejectAsync(int id)
        {
            return Task.Run(() =>
            {
                RoutePassenger passenger = _routePassengerRepo.Find(x => x.Id == id);
                passenger.Approved = false;

                _routePassengerRepo.Update(passenger);
            });
        }

        public Task<bool> DeleteAsync(int id)
        {
            Task<bool> result = _routePassengerRepo.DeleteAsync(id);
            return result;
        }

        public Task<BookResult> BookAsync(RoutePassenger passenger, BookType bookType)
        {
            IBookStrategy bookStrategy = _bookStrategyFactory.Create(bookType);
            return bookStrategy.BookAsync(passenger);
        }

        public Task<ConfirmResult> ConfirmAsync(int id, int? code, BookType bookType)
        {
            IBookStrategy bookStrategy = _bookStrategyFactory.Create(bookType);
            return bookStrategy.ConfirmAsync(id, code);
        }
    }
}
