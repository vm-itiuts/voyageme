﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VoyageMe.BLL.Abstract.Services;
using VoyageMe.DAL.Abstract.Repositories;
using VoyageMe.Common.Models;
using System.Data;

namespace VoyageMe.BLL.Services
{
    public class RouteService : IRouteService
    {
        #region Private Instance Fields
        private readonly IRouteRepository _routeRepo;
        private readonly IRoutePassengersRepository _routePassengerRepo;
        private readonly IWayPointRepository _wayPointRepo;
        private readonly ITransportRepository _transportRepo;
        private readonly IUserRepository _userRepo;
        #endregion

        #region Public Instance Constuctor
        public RouteService(
            IRouteRepository routeRepo,
            IRoutePassengersRepository routePassangerRepo,
            IWayPointRepository wayPointRepo,
            ITransportRepository transportRepo,
            IUserRepository userRepo)
        {
            _routeRepo = routeRepo;
            _routePassengerRepo = routePassangerRepo;
            _wayPointRepo = wayPointRepo;
            _transportRepo = transportRepo;
            _userRepo = userRepo;
        }
        #endregion

        #region Public Instance Methods

        public Task<Route> GetRouteAsync(int id)
        {
            return Task.Run(() =>
            {
                Route route =  _routeRepo.Find<WayPoint>(x => x.Id == id, r => r.WayPoints);
                route.Transport = _transportRepo.Find<TransportImage>
                    (x => x.Id == route.TransportId, t => t.Images);
                route.Driver = _userRepo.Find<Account>(x => x.Id == route.DriverId, x => x.Account);
                route.Passengers = _routePassengerRepo.FindAll<User>
                    (x => x.RouteId == route.Id, p => p.Passenger).ToList();
                return route;
            });
        }

        public async Task<Route> CreateRouteAsync(Route route)
        {
            using (IRepositoryTransaction transaction = _routeRepo.BeginTransaction())
            {
                try
                {
                    await Task.Run(() =>
                    {
                        route.AddDate = DateTime.UtcNow;
                        _routeRepo.Insert(route, transaction);
                        foreach (WayPoint wayPoint in route.WayPoints)
                        {
                            wayPoint.RouteId = route.Id;
                            _wayPointRepo.Insert(wayPoint, transaction);
                        }
                        route.Transport = _transportRepo.Find<TransportImage>(x => x.Id == route.TransportId,
                            x => x.Images, transaction);
                    });
                    transaction.Commit();
                }
                catch (Exception)
                {
                    transaction.Rollback();
                    throw;
                }
            }

            return route;
        }

        public async Task<Route> UpdateRouteAsync(Route route)
        {
            using (IRepositoryTransaction transaction = _routeRepo.BeginTransaction())
            {
                try
                {
                    await Task.Run(() =>
                    {
                        route.AddDate = DateTime.UtcNow;//TODO: default AddDate causes date overflow exception
                        _routeRepo.Update(route, transaction);

                        IEnumerable<WayPoint> wayPoints = _wayPointRepo.FindAll(x => x.RouteId == route.Id, transaction);
                        //TODO: implement more efficient way to update waypoints
                        foreach (WayPoint wayPoint in wayPoints)
                        {
                            _wayPointRepo.Delete(wayPoint.Id, transaction);
                        }
                        foreach (WayPoint wayPoint in route.WayPoints)
                        {
                            wayPoint.RouteId = route.Id;
                            _wayPointRepo.Insert(wayPoint, transaction);
                        }
                    });
                    transaction.Commit();
                }
                catch (Exception)
                {
                    transaction.Rollback();
                    throw;
                }
            }

            return route;
        }

        public Task<bool> DeleteRouteAsync(int id)
        {
            return _routeRepo.DeleteAsync(id);
        }
        #endregion
    }
}
