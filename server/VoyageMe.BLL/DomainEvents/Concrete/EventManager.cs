﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using VoyageMe.BLL.DomainEvents.Abstract;

namespace VoyageMe.BLL.DomainEvents.Concrete
{
    public class EventManager : IEventManager
    {
		private readonly IServiceProvider _serviceProvider;

		public EventManager(IServiceProvider serviceProvider)
		{
			_serviceProvider = serviceProvider;
		}



		public async Task Raise<T>(T domainEvent) where T : IDomainEvent
		{
			IEnumerable<IDomainEventHandler<T>> eventHandlers = _serviceProvider.GetServices<IDomainEventHandler<T>>();
			foreach (IDomainEventHandler<T> hanlder in eventHandlers)
			{
				await hanlder.Handle(domainEvent);
			}
		}
	}
}
