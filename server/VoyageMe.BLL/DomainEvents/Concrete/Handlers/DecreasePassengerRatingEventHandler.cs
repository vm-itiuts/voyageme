﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VoyageMe.BLL.DomainEvents.Abstract;
using VoyageMe.BLL.DomainEvents.Concrete.Events;
using VoyageMe.Common.Models;
using VoyageMe.DAL.Abstract.Repositories;

namespace VoyageMe.BLL.DomainEvents.Concrete.Handlers
{
    public class DecreasePassengerRatingEventHandler : IDomainEventHandler<PassengerNotAttendedEvent>
    {
		private readonly IUserRepository _userRepo;

		public DecreasePassengerRatingEventHandler(IUserRepository userRepo)
		{
			_userRepo = userRepo;
		}

		public async Task Handle(PassengerNotAttendedEvent domainEvnet)
		{
			User passenger = await _userRepo.FindAsync(x => x.Id == domainEvnet.PassengerId);
			passenger.PositiveRank++;
			await _userRepo.UpdateAsync(passenger);
		}
	}
}
