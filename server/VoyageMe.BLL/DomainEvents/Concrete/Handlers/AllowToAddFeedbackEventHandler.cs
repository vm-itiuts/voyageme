﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VoyageMe.BLL.DomainEvents.Abstract;
using VoyageMe.BLL.DomainEvents.Concrete.Events;
using VoyageMe.Common.Models;
using VoyageMe.DAL.Abstract.Repositories;

namespace VoyageMe.BLL.DomainEvents.Concrete.Handlers
{
    public class AllowToAddFeedbackEventHandler : IDomainEventHandler<PassengerAttendedEvent>
    {
        private readonly IPendingFeedbackRepository _pendingFeedbackRepo;

        public AllowToAddFeedbackEventHandler(IPendingFeedbackRepository pendingFeedbackRepo)
        {
            _pendingFeedbackRepo = pendingFeedbackRepo;
        }

        public Task Handle(PassengerAttendedEvent domainEvent)
        {
            PendingFeedback feedback = new PendingFeedback
            {
                PassengerId = domainEvent.PassengerId,
                DriverId = domainEvent.DriverId,
                RouteId = domainEvent.RouteId
            };

            return _pendingFeedbackRepo.InsertAsync(feedback);
        }
    }
}
