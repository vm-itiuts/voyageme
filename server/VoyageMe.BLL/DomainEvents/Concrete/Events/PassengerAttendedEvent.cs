﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VoyageMe.BLL.DomainEvents.Abstract;

namespace VoyageMe.BLL.DomainEvents.Concrete.Events
{
	public class PassengerAttendedEvent : IDomainEvent
	{
		public int PassengerId { get; }

		public int DriverId { get; }

		public int RouteId { get; }

		public PassengerAttendedEvent(int passengerId, int driverId, int routeId)
		{
			PassengerId = passengerId;
			DriverId = driverId;
			RouteId = routeId;
		}
	}
}
