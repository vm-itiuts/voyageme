﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VoyageMe.BLL.DomainEvents.Abstract;

namespace VoyageMe.BLL.DomainEvents.Concrete.Events
{
    public class PassengerNotAttendedEvent : IDomainEvent
    {
		public int PassengerId { get; }

		public PassengerNotAttendedEvent(int passengerId)
		{
			PassengerId = passengerId;
		}
	}
}
