﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VoyageMe.BLL.DomainEvents.Abstract
{
    public interface IEventManager
    {
		Task Raise<T>(T domainEvent) where T : IDomainEvent;
    }
}
