﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VoyageMe.BLL.DomainEvents.Abstract
{
    public interface IDomainEventHandler<in T> where T: IDomainEvent
    {
		Task Handle(T domainEvent);
    }
}
