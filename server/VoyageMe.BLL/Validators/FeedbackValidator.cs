﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VoyageMe.BLL.Abstract.Validators;
using VoyageMe.Common.Models;
using VoyageMe.Common.Validation;
using VoyageMe.Common.Validation.Errors;
using VoyageMe.DAL.Abstract.Repositories;

namespace VoyageMe.BLL.Validators
{
    public class FeedbackValidator : IFeedbackValidator
    {
        private readonly IPendingFeedbackRepository _pendingFeedbackRepo;

        public FeedbackValidator(
            IPendingFeedbackRepository pendingFeedbackRepo)
        {
            _pendingFeedbackRepo = pendingFeedbackRepo;
        }

        public async Task<ValidationResult> ValidateByIdAsync(int id, int passengerId)
        {
            ValidationResult result = new ValidationResult();
            PendingFeedback pendingFeedback = await _pendingFeedbackRepo.FindAsync(x => x.Id == id);

            if (pendingFeedback == null)
            {
                result.AddError(FeedbackError.CreateNotFoundError());
            }
            else if (pendingFeedback.PassengerId != passengerId)
            {
                result.AddError(FeedbackError.CreateWrongPassengerError());
            }

            return result;
        }

        public async Task<ValidationResult> ValidateByRouteIdAsync(int routeId, int passengerId)
        {
            ValidationResult result = new ValidationResult();
            PendingFeedback pendingFeedback = await _pendingFeedbackRepo.FindAsync(x => x.RouteId == routeId
                && x.PassengerId == passengerId);

            if (pendingFeedback == null)
            {
                result.AddError(FeedbackError.CreateFeedbackPermissionError());
            }

            return result;
        }
    }
}
