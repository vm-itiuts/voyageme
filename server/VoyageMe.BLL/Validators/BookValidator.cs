﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VoyageMe.BLL.Abstract.Models.Booking;
using VoyageMe.BLL.Abstract.Validators;
using VoyageMe.Common.Models;
using VoyageMe.Common.Validation;
using VoyageMe.Common.Validation.Errors;
using VoyageMe.DAL.Abstract.Repositories;

namespace VoyageMe.BLL.Validators
{
    public class BookValidator : IBookValidator
    {
        #region Private Instance Fields
        private readonly IRouteRepository _routeRepo;
        private readonly IDriverValidator _driverValidator;
        #endregion

        public BookValidator(
            IRouteRepository routeRepo,
            IDriverValidator driverValidator)
        {
            _routeRepo = routeRepo;
            _driverValidator = driverValidator;
        }

        public async Task<ValidationResult> ValidateNewBookAsync(NewBookModel model)
        {
            ValidationResult result = new ValidationResult();
            Route route = await _routeRepo.FindAsync<RoutePassenger>(x => x.Id == model.RouteId, x => x.Passengers);
            if (route == null)
            {
                result.AddError(RouteError.CreateRouteNotFoundError());
                return result;
            }
            if (IsBookTypeValid(route, model.BookType) == false)
            {
                result.AddError(BookingError.CreateBookTypeError());
            };
            if (IsFreeSeatsEnough(route, model.NumberOfSeats) == false)
            {
                result.AddError(BookingError.CreateFreeSeatsError());
            };
            return result;
        }

        public async Task<ValidationResult> ValidateConfirmationAsync(BookingConfirmationModel model)
        {
            ValidationResult result = new ValidationResult();
            Route route = await _routeRepo.FindAsync<RoutePassenger>(x => x.Id == model.RouteId, x => x.Passengers);
            if(route == null)
            {
                result.AddError(RouteError.CreateRouteNotFoundError());
                return result;
            }

            if (IsBookTypeValid(route, model.BookType))
            {
                if (model.BookType == BookType.Manually)
                {
                    ValidationResult routeOwnerValidationResult = _driverValidator.ValidateRouteOwner(route, model.UserId);
                    if (routeOwnerValidationResult.IsValid == false)
                    {
                        result.AddRangeErrors(routeOwnerValidationResult.Errors);
                    }
                }
                else if (model.BookType == BookType.WithCode)
                {
                    if (IsPassengerFound(route, model.UserId) == false)
                    {
                        result.AddError(BookingError.CreatePassengerNotFoundError());
                    }
                }
            }
            else
            {
                result.AddError(BookingError.CreateBookTypeError());
            }

            return result;
        }

        public async Task<ValidationResult> ValidateRejectionAsync(BookingRejectionModel model)
        {
            ValidationResult result = new ValidationResult();
            Route route = await _routeRepo.FindAsync<RoutePassenger>(x => x.Id == model.RouteId, x => x.Passengers);
            if (route == null)
            {
                result.AddError(RouteError.CreateRouteNotFoundError());
                return result;
            }

            ValidationResult routeOwnerValidationResult = _driverValidator.ValidateRouteOwner(route, model.UserId);
            if (routeOwnerValidationResult.IsValid == false)
            {
                result.AddRangeErrors(routeOwnerValidationResult.Errors);
            }
            else if (IsPassengerFound(route, model.UserId) == false)
            {
                result.AddError(BookingError.CreatePassengerNotFoundError());
            }

            return result;
        }

        #region Non-Public Instance Methods
        private bool IsBookTypeValid(Route route, BookType bookType)
        {
            bool result = route.BookType == bookType;
            return result;
        }

        private bool IsFreeSeatsEnough(Route route, int numberOfSeats)
        {
            int freeSeats = route.SeatsNumber - route.Passengers.Aggregate(0, (current, next) => current + next.BookedSeats);

            bool result = freeSeats >= numberOfSeats;
            return result;
        }

        private bool IsPassengerFound(Route route, int passengerId)
        {
            bool result = route.Passengers.Any(x => x.PassengerId == passengerId);
            return result;
        }
        #endregion
    }
}
