﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VoyageMe.BLL.Abstract.Validators;
using VoyageMe.Common.Models;
using VoyageMe.Common.Validation;
using VoyageMe.Common.Validation.Errors;
using VoyageMe.DAL.Abstract.Repositories;

namespace VoyageMe.BLL.Validators
{
    public class DriverValidator : IDriverValidator
    {
        private readonly IRouteRepository _routeRepo;
        private readonly ITransportRepository _transportRepo;

        public DriverValidator(
            IRouteRepository routeRepo,
            ITransportRepository transportRepo)
        {
            _routeRepo = routeRepo;
            _transportRepo = transportRepo;
        }

        public async Task<ValidationResult> ValidateRouteOwnerAsync(int routeId, int driverId)
        {
            ValidationResult result = new ValidationResult();
            Route route = await _routeRepo.FindAsync(x => x.Id == routeId);
            if(route == null)
            {
                result.AddError(RouteError.CreateRouteNotFoundError());
                return result;
            }

            result = ValidateRouteOwner(route, driverId);

            return result;
        }

        public ValidationResult ValidateRouteOwner(Route route, int driverId)
        {
            ValidationResult result = new ValidationResult();
            if(route.DriverId != driverId)
            {
                result.AddError(BookingError.CreateRouteOwnerError());
            }

            return result;
        }

        public async Task<ValidationResult> ValidateTransportOwnerAsync(int transportId, int driverId)
        {
            ValidationResult result = new ValidationResult();
            Transport transport = await _transportRepo.FindAsync(x => x.Id == transportId);
            if(transport == null)
            {
                result.AddError(TransportError.CreateTransportNotFoundError());
                return result;
            }

            if(transport.OwnerId != driverId)
            {
                result.AddError(TransportError.CreateTransportOwnerError());
            }

            return result;
        }
    }
}
