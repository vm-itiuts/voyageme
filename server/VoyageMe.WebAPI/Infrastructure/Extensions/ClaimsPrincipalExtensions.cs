﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using VoyageMe.WebAPI.Infrastructure.Constants.Claims;

namespace VoyageMe.WebAPI.Infrastructure.Extensions
{
    public static class ClaimsPrincipalExtensions
    {
        public static int GetId(this ClaimsPrincipal user)
        {
            int result = int.Parse(user.FindFirst(IdentityClaimNames.Id).Value);
            return result;
        }
    }
}
