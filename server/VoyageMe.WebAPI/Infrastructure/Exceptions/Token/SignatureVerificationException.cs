﻿using System;

namespace VoyageMe.WebAPI.Infrastructure.Exceptions.Token
{
    public class SignatureVerificationException : Exception
    {
        public SignatureVerificationException() 
            : base()
        {
        }

        public SignatureVerificationException(string message)
            : base(message)
        {
        }

        public SignatureVerificationException(string message, Exception innerException) 
            : base(message ,innerException)
        {
        }
    }
}
