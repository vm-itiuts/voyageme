﻿using System;

namespace VoyageMe.WebAPI.Infrastructure.Exceptions.Token
{
    public class TokenExpiredException : Exception
    {
        public TokenExpiredException() 
            : base()
        {
        }

        public TokenExpiredException(string message) 
            : base(message)
        {
        }
    }
}
