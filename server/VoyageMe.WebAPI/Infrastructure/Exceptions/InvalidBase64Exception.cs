﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VoyageMe.WebAPI.Infrastructure.Exceptions
{
    public class InvalidBase64Exception : Exception
    {
        public InvalidBase64Exception() : base()
        {
        }

        public InvalidBase64Exception(string message) : base(message)
        {
        }
    }
}
