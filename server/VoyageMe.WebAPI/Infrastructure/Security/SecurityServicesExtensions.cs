﻿using Microsoft.Extensions.DependencyInjection;

using VoyageMe.WebAPI.Infrastructure.Security.Interface;
using VoyageMe.WebAPI.Infrastructure.Security.Concrete;
using VoyageMe.WebAPI.Infrastructure.Security.Interface.Token;
using VoyageMe.WebAPI.Infrastructure.Security.Concrete.Token;
using VoyageMe.WebAPI.Infrastructure.Security.ExternalAuthentication;

namespace VoyageMe.WebAPI.Infrastructure.Security
{
    public static class SecurityServicesExtensions
    {
        public static IServiceCollection AddSecurityServices(this IServiceCollection services)
        {
            services.AddTransient<ISaltGenerator, SaltGenerator>();
            services.AddTransient<IPasswordManager, PasswordManager>();
            services.AddScoped<IAccessTokenManager, JwtTokenManager>();
            services.AddScoped<IRefreshTokenManager, RefreshTokenManager>();
            services.AddScoped<IAuthResponseGenerator, AuthResponseGenerator>();
            services.AddScoped<ExternalAuth>();

            return services;
        }
    }
}
