﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

using Newtonsoft.Json;
using VoyageMe.Common.Models;
using VoyageMe.WebAPI.Infrastructure.Constants.Claims;
using VoyageMe.WebAPI.Infrastructure.Exceptions.Token;
using VoyageMe.WebAPI.Infrastructure.Security.Interface.Token;
using VoyageMe.WebAPI.Infrastructure.Options;

namespace VoyageMe.WebAPI.Infrastructure.Security.Concrete.Token
{
    /// <summary>
    /// Provides methods for encoding and decoding JSON Web Tokens.
    /// </summary>
    public class JwtTokenManager : IAccessTokenManager
    {
        #region Private Instance Fields
        private readonly IDictionary<JwtHashAlgorithm, Func<byte[], byte[], byte[]>> HashAlgorithms;

        private static readonly DateTime UnixEpoch = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
        #endregion Private Instance Fields

        #region Public Instance .ctor
        public JwtTokenManager()
        {
            HashAlgorithms = new Dictionary<JwtHashAlgorithm, Func<byte[], byte[], byte[]>>
            {
                { JwtHashAlgorithm.HS256, (key, value) => { using (var sha = new HMACSHA256(key)) { return sha.ComputeHash(value); } } },
                { JwtHashAlgorithm.HS384, (key, value) => { using (var sha = new HMACSHA384(key)) { return sha.ComputeHash(value); } } },
                { JwtHashAlgorithm.HS512, (key, value) => { using (var sha = new HMACSHA512(key)) { return sha.ComputeHash(value); } } }
            };
        }
        #endregion Public Instance .ctor

        #region Public Instance Methods

        public string GenerateToken(Account accountData, JwtAuthenticationOptions authenticationOptions)
        {
            if (accountData == null)
            {
                throw new ArgumentNullException(nameof(accountData));
            }

            if (authenticationOptions == null)
            {
                throw new ArgumentNullException(nameof(authenticationOptions));
            }

            DateTime now = DateTime.UtcNow;

            // https://tools.ietf.org/html/rfc7519
            var payload = new Dictionary<string, object>()
            {
                // todo: replace email to id?
                { JwtRegisteredClaimNames.Sub, accountData.Email }, // Subject
                { JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString() }, // JWT ID
                { JwtRegisteredClaimNames.Iss, authenticationOptions.TokenIssuer }, // Issuer
                { JwtRegisteredClaimNames.Nbf, this.ToUnixEpochDate(now) }, // Not Before
                { JwtRegisteredClaimNames.Exp, this.ToUnixEpochDate(now.Add(authenticationOptions.TokenExpiresIn)) }, // Expiration Time
                { JwtRegisteredClaimNames.Iat, this.ToUnixEpochDate(now) }, // Issued At ()
                { IdentityClaimNames.Email, accountData.Email }, // todo: remove? 
                { SecurityClaimNames.PhoneConfirmed, accountData.PhoneConfirmed },
                { SecurityClaimNames.EmailConfirmed, accountData.EmailConfirmed },
                { IdentityClaimNames.Id, accountData.Id }
            };

            if (accountData.Permissions != null && accountData.Permissions.Any())
            {
                string permissions = string.Join(",", accountData.Permissions.Select(p => p.Name));

                payload.Add(SecurityClaimNames.Permissions, permissions);
            }

            string accessToken = this.Encode(payload, authenticationOptions.SecretKey, JwtHashAlgorithm.HS256);

            return accessToken;
        }

        /// <summary>
        /// Given a JWT, decode it and return the payload as an object).
        /// </summary>
        /// <typeparam name="T">The <see cref="Type"/> to return</typeparam>
        /// <param name="token">The JWT.</param>
        /// <param name="key">The key that was used to sign the JWT.</param>
        /// <param name="verify">Whether to verify the signature (default is true).</param>
        /// <returns>An object representing the payload.</returns>
        /// <exception cref="SignatureVerificationException">Thrown if the verify parameter was true and the signature was NOT valid or if the JWT was signed with an unsupported algorithm.</exception>
        public T DecodeToObject<T>(string token, string key, bool verify = true)
        {
            var payloadJson = Decode(token, Encoding.UTF8.GetBytes(key), verify);
            var payloadData = JsonConvert.DeserializeObject<T>(payloadJson);
            return payloadData;
        }
        #endregion Public Instance Methods

        #region Private Instance Methods
        private string Encode(object payload, string key, JwtHashAlgorithm algorithm)
        {
            byte[] keyBytes = Encoding.UTF8.GetBytes(key);

            var segments = new List<string>();
            var header = new Dictionary<string, object>()
            {
                { "typ", "JWT" },
                { "alg", algorithm.ToString() }
            };

            byte[] headerBytes = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(header));
            byte[] payloadBytes = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(payload));

            segments.Add(Base64Wrapper.Encode(headerBytes));
            segments.Add(Base64Wrapper.Encode(payloadBytes));

            var stringToSign = string.Join(".", segments.ToArray());

            var bytesToSign = Encoding.UTF8.GetBytes(stringToSign);

            byte[] signature = HashAlgorithms[algorithm](keyBytes, bytesToSign);
            segments.Add(Base64Wrapper.Encode(signature));

            return string.Join(".", segments.ToArray());
        }

        /// <summary>
        /// Given a JWT, decode it and return the JSON payload.
        /// </summary>
        /// <param name="token">The JWT.</param>
        /// <param name="key">The key bytes that were used to sign the JWT.</param>
        /// <param name="verify">Whether to verify the signature (default is true).</param>
        /// <returns>A string containing the JSON payload.</returns>
        /// <exception cref="SignatureVerificationException">Thrown if the verify parameter was true and the signature was NOT valid or if the JWT was signed with an unsupported algorithm.</exception>
        /// <exception cref="TokenExpiredException">Thrown if the token has expired.</exception>
        private string Decode(string token, byte[] key, bool verify = true)
        {
            var parts = token.Split('.');
            if (parts.Length != 3)
            {
                throw new SignatureVerificationException("Token must consist from 3 delimited by dot parts");
            }
            var header = parts[0];
            var payload = parts[1];
            var crypto = Base64Wrapper.Decode(parts[2]);

            var headerJson = Encoding.UTF8.GetString(Base64Wrapper.Decode(header));
            var payloadJson = Encoding.UTF8.GetString(Base64Wrapper.Decode(payload));
            Dictionary<string, object> headerData;
            try
            {
                headerData = JsonConvert.DeserializeObject<Dictionary<string, object>>(headerJson);
            }
            catch (JsonReaderException e)
            {
                throw new SignatureVerificationException(e.Message, e);
            }
            if (verify)
            {
                var bytesToSign = Encoding.UTF8.GetBytes(string.Concat(header, ".", payload));
                var algorithm = (string)headerData["alg"];

                var signature = HashAlgorithms[GetHashAlgorithm(algorithm)](key, bytesToSign);
                var decodedCrypto = Convert.ToBase64String(crypto);
                var decodedSignature = Convert.ToBase64String(signature);

                Verify(decodedCrypto, decodedSignature, payloadJson);
            }

            return payloadJson;
        }

        private void Verify(string decodedCrypto, string decodedSignature, string payloadJson)
        {
            if (decodedCrypto != decodedSignature)
            {
                throw new SignatureVerificationException(string.Format("Invalid signature. Expected {0} got {1}", decodedCrypto, decodedSignature));
            }

            // verify exp claim https://tools.ietf.org/html/draft-ietf-oauth-json-web-token-32#section-4.1.4
            var payloadData = JsonConvert.DeserializeObject<Dictionary<string, object>>(payloadJson);
            if (payloadData.ContainsKey("exp") && payloadData["exp"] != null)
            {
                // safely unpack a boxed int 
                int exp;
                try
                {
                    exp = Convert.ToInt32(payloadData["exp"]);
                }
                catch (Exception)
                {
                    throw new SignatureVerificationException("Claim 'exp' must be an integer.");
                }

                var secondsSinceEpoch = Math.Round((DateTime.UtcNow - UnixEpoch).TotalSeconds);
                if (secondsSinceEpoch >= exp)
                {
                    throw new TokenExpiredException();
                }
            }
        }

        private static JwtHashAlgorithm GetHashAlgorithm(string algorithm)
        {
            switch (algorithm)
            {
                case "HS256": return JwtHashAlgorithm.HS256;
                case "HS384": return JwtHashAlgorithm.HS384;
                case "HS512": return JwtHashAlgorithm.HS512;
                default: throw new SignatureVerificationException("Algorithm not supported.");
            }
        }

        private long ToUnixEpochDate(DateTime date)
        {
            long result = (long)Math.Round((date.ToUniversalTime() - new DateTimeOffset(1970, 1, 1, 0, 0, 0, TimeSpan.Zero)).TotalSeconds);

            return result;
        }
    }
    #endregion Private Instance Methods
}
