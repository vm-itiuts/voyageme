﻿using System;
using System.Text;
using System.Security.Cryptography;
using VoyageMe.WebAPI.Infrastructure.Security.Interface.Token;

namespace VoyageMe.WebAPI.Infrastructure.Security.Concrete.Token
{
    public class RefreshTokenManager : IRefreshTokenManager
    {
        public string GenerateToken()
        {
            byte[] randomPart = new byte[128 / 8];

            using (RandomNumberGenerator rng = RandomNumberGenerator.Create())
            {
                rng.GetBytes(randomPart);
            }

            DateTime tokenCreated = DateTime.UtcNow;
            byte[] createdPart = Encoding.UTF8.GetBytes(tokenCreated.ToString("ddMMyyhhmm"));

            string token = Base64Wrapper.Encode(randomPart) + Base64Wrapper.Encode(createdPart);

            return token;
        }
    }
}
