﻿using System;
using System.Security.Cryptography;

using VoyageMe.WebAPI.Infrastructure.Security.Interface;

namespace VoyageMe.WebAPI.Infrastructure.Security.Concrete
{
    class SaltGenerator : ISaltGenerator
    {
        public byte[] GenerateSalt()
        {
            byte[] salt = new byte[128 / 8]; // 128-bit salt

            using (RandomNumberGenerator rng = RandomNumberGenerator.Create())
            {
                rng.GetBytes(salt);
            }

            return salt;
        }

        public string ConvertToString(byte[] salt)
        {
            if (salt == null)
            {
                throw new ArgumentNullException(nameof(salt));
            }

            string result = Convert.ToBase64String(salt);

            return result;
        }
    }
}
