﻿using System;
using Microsoft.AspNetCore.Cryptography.KeyDerivation;

using VoyageMe.WebAPI.Infrastructure.Security.Interface;

namespace VoyageMe.WebAPI.Infrastructure.Security.Concrete
{
    class PasswordManager : IPasswordManager
    {
        public string HashPassword(string password, byte[] salt)
        {
            if (password == null)
            {
                throw new ArgumentNullException(nameof(password));
            }

            if (salt == null)
            {
                throw new ArgumentNullException(nameof(salt));
            }

            byte[] passwordHash = KeyDerivation.Pbkdf2(password, salt, KeyDerivationPrf.HMACSHA256, 10000, 256 / 8);
            string passwordHashString = Convert.ToBase64String(passwordHash);

            return passwordHashString;
        }

        public string HashPassword(string password, string salt)
        {
            if (salt == null)
            {
                throw new ArgumentNullException(nameof(salt));
            }

            byte[] saltBytes = Convert.FromBase64String(salt);
            string passwordHashString = this.HashPassword(password, saltBytes);

            return passwordHashString;
        }

        public bool VerifyPassword(string password, string passwordHash, string salt)
        {
            if (passwordHash == null)
            {
                throw new ArgumentNullException(nameof(password));
            }

            bool verificationResult = passwordHash == this.HashPassword(password, salt);

            return verificationResult;
        }
    }
}
