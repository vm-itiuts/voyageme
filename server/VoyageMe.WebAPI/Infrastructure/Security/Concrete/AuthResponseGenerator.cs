﻿using System;
using Microsoft.Extensions.Options;

using VoyageMe.Common.Models;
using VoyageMe.Common.Validation.Errors;
using VoyageMe.WebAPI.Models.Authentication;
using VoyageMe.WebAPI.Infrastructure.Security.Interface;
using VoyageMe.WebAPI.Infrastructure.Security.Interface.Token;
using VoyageMe.WebAPI.Infrastructure.Options;

namespace VoyageMe.WebAPI.Infrastructure.Security.Concrete
{
    public class AuthResponseGenerator : IAuthResponseGenerator
    {
        private readonly IAccessTokenManager _jwtTokenManager;
        private readonly IRefreshTokenManager _refreshTokenManager;
        private readonly JwtAuthenticationOptions _authenticationOptions;

        public AuthResponseGenerator(IAccessTokenManager jwtTokenManager, IRefreshTokenManager refreshTokenManager, IOptions<JwtAuthenticationOptions> authenticationOptions)
        {
            _jwtTokenManager = jwtTokenManager;
            _refreshTokenManager = refreshTokenManager;
            _authenticationOptions = authenticationOptions.Value;
        }

        public SuccessfulAuthResponse GenerateSuccess(Account accountData, User userData)
        {
            if (accountData == null)
            {
                throw new ArgumentNullException(nameof(accountData));
            }

            if (userData == null)
            {
                throw new ArgumentNullException(nameof(userData));
            }

            string accessToken = _jwtTokenManager.GenerateToken(accountData, _authenticationOptions);
            string refreshToken = _refreshTokenManager.GenerateToken();

            string firstName = userData.FirstName;

            SuccessfulAuthResponse successResponse = new SuccessfulAuthResponse()
            {
                AccessToken= accessToken,
                RefreshToken = refreshToken,
                ExpiresIn = (int)_authenticationOptions.TokenExpiresIn.TotalMinutes,
                FirstName = firstName
                // more info ?
            };

            return successResponse;
        }

        public FailedAuthResponse GenerateError(AuthError error)
        {
            if (error == null)
            {
                throw new ArgumentNullException(nameof(error));
            }

            FailedAuthResponse errorResponse = new FailedAuthResponse(error);

            return errorResponse;
        }
    }
}
