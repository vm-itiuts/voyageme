﻿using VoyageMe.Common.Models;
using VoyageMe.Common.Validation.Errors;
using VoyageMe.WebAPI.Models.Authentication;

namespace VoyageMe.WebAPI.Infrastructure.Security.Interface
{
    public interface IAuthResponseGenerator
    {
        SuccessfulAuthResponse GenerateSuccess(Account accountData, User userData);

        FailedAuthResponse GenerateError(AuthError error);
    }
}
