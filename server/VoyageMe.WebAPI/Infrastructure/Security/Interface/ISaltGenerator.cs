﻿namespace VoyageMe.WebAPI.Infrastructure.Security.Interface
{
    public interface ISaltGenerator
    {
        byte[] GenerateSalt();

        string ConvertToString(byte[] salt);
    }
}
