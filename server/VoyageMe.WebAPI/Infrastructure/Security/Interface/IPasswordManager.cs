﻿namespace VoyageMe.WebAPI.Infrastructure.Security.Interface
{
    public interface IPasswordManager
    {
        string HashPassword(string password, byte[] salt);

        string HashPassword(string password, string salt);

        bool VerifyPassword(string password, string passwordHash, string salt);
    }
}
