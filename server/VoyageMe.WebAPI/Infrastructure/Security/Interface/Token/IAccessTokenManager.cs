﻿using VoyageMe.Common.Models;
using VoyageMe.WebAPI.Infrastructure.Options;

namespace VoyageMe.WebAPI.Infrastructure.Security.Interface.Token
{
    public interface IAccessTokenManager
    {
        string GenerateToken(Account accountData, JwtAuthenticationOptions authenticationOptions);

        T DecodeToObject<T>(string token, string key, bool verify = true);
    }
}