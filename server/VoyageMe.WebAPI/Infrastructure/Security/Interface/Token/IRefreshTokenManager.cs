﻿namespace VoyageMe.WebAPI.Infrastructure.Security.Interface.Token
{
    public interface IRefreshTokenManager
    {
        string GenerateToken();
    }
}
