﻿namespace VoyageMe.WebAPI.Infrastructure.Security.Interface.Token
{
    public enum JwtHashAlgorithm
    {
        HS256,
        HS384,
        HS512
    }
}
