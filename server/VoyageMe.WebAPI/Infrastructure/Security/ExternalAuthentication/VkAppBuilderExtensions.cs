﻿using System;
using Microsoft.AspNetCore.Builder;

namespace VoyageMe.WebAPI.Infrastructure.Security.ExternalAuthentication
{
    public static class VkAppBuilderExtensions
    {
        public static IApplicationBuilder UseVkAuthentication(this IApplicationBuilder app, VkOptions options)
        {
            if (options == null)
            {
                throw new ArgumentNullException(nameof(options));
            }

            app.UseOAuthAuthentication(options);

            return app;
        }
    }
}
