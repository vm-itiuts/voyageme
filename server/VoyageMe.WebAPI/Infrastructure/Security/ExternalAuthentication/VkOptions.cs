﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Builder;

namespace VoyageMe.WebAPI.Infrastructure.Security.ExternalAuthentication
{
    public class VkOptions : OAuthOptions
    {
        public string ApiVersion { get; } = "5.58";

        public ICollection<string> Fields { get; } = new HashSet<string>();

        public VkOptions()
        {
            AuthenticationScheme = "VK";
            DisplayName = AuthenticationScheme;
            CallbackPath = "/signin-vk";
            AuthorizationEndpoint = "https://oauth.vk.com/authorize";
            TokenEndpoint = "https://oauth.vk.com/access_token";
            UserInformationEndpoint = "https://api.vk.com/method/users.get";
            ClaimsIssuer = "oauth2-vk";
        }
    }
}
