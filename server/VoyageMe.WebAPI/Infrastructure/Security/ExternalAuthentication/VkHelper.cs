﻿using System;

using Newtonsoft.Json.Linq;

namespace VoyageMe.WebAPI.Infrastructure.Security.ExternalAuthentication
{
    static class VkHelper
    {
        #region Public static methods
        public static string GetId(JObject user)
        {
            VkHelper.ThrowIfArgumentNull(user);

            return user.Value<string>("id");
        }

        public static string GetFirstName(JObject user)
        {
            VkHelper.ThrowIfArgumentNull(user);

            return user.Value<string>("first_name");
        }

        public static string GetLastName(JObject user)
        {
            VkHelper.ThrowIfArgumentNull(user);

            return user.Value<string>("last_name");
        }

        public static string GetGender(JObject user)
        {
            VkHelper.ThrowIfArgumentNull(user);

            return user.Value<string>("sex");
        }

        public static string GetBirthday(JObject user)
        {
            VkHelper.ThrowIfArgumentNull(user);

            return user.Value<string>("bdate");
        }

        public static string GetPicture(JObject user)
        {
            VkHelper.ThrowIfArgumentNull(user);

            string pictureLink = user.Value<string>("photo_max_orig");

            // check on default vk image(dog)
            if (pictureLink != null && pictureLink == "http://vk.com/images/camera_a.gif")
            {
                pictureLink = null;
            }

            return pictureLink;
        }
        #endregion Public static methods

        #region Private static methods
        private static void ThrowIfArgumentNull(object argument)
        {
            if (argument == null)
            {
                throw new ArgumentNullException(nameof(argument));
            }
        }
        #endregion Private static methods
    }
}
