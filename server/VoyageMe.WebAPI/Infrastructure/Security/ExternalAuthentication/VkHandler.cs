﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Authentication.OAuth;
using System.Net.Http;
using Microsoft.AspNetCore.WebUtilities;
using Newtonsoft.Json.Linq;
using System.Security.Claims;

namespace VoyageMe.WebAPI.Infrastructure.Security.ExternalAuthentication
{
    class VkHandler
    {
        public async Task CreatingVkAuthTicket(OAuthCreatingTicketContext context)
        {
            VkOptions options = (VkOptions)context.Options;

            string endpoint = QueryHelpers.AddQueryString(options.UserInformationEndpoint, "access_token", context.AccessToken);
            endpoint = QueryHelpers.AddQueryString(endpoint, "fields", this.FormatFields(options));
            endpoint = QueryHelpers.AddQueryString(endpoint, "v", options.ApiVersion);

            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, endpoint);
            HttpResponseMessage response = await context.Backchannel.SendAsync(request);

            response.EnsureSuccessStatusCode();

            JObject payload = JObject.Parse(await response.Content.ReadAsStringAsync());

            JObject user = payload.Value<JArray>("response")
                .FirstOrDefault()
                ?.ToObject<JObject>();

            if (user != null)
            {
                this.AddClaimsToIdentity(context, user);
            }
            else
            {
                // todo: throw exception
            }
        }

        private void AddClaimsToIdentity(OAuthCreatingTicketContext context, JObject user)
        {
            ClaimsIdentity identity = context.Identity;

            string id = VkHelper.GetId(user);
            if(!string.IsNullOrEmpty(id))
            {
                identity.AddClaim(new Claim(ClaimTypes.NameIdentifier, id, ClaimValueTypes.String, context.Options.ClaimsIssuer));
            }

            string firstName = VkHelper.GetFirstName(user);
            if(!string.IsNullOrEmpty(firstName))
            {
                identity.AddClaim(new Claim(ClaimTypes.GivenName, firstName, ClaimValueTypes.String, context.Options.ClaimsIssuer));
            }

            string lastName = VkHelper.GetLastName(user);
            if(!string.IsNullOrEmpty(lastName))
            {
                identity.AddClaim(new Claim(ClaimTypes.Surname, lastName, ClaimValueTypes.String, context.Options.ClaimsIssuer));
            }

            string birthday = VkHelper.GetBirthday(user);
            if(!string.IsNullOrEmpty(birthday))
            {
                identity.AddClaim(new Claim(ClaimTypes.DateOfBirth, birthday, ClaimValueTypes.String, context.Options.ClaimsIssuer));
            }

            string gender = VkHelper.GetGender(user);
            if(!string.IsNullOrEmpty(gender))
            {
                identity.AddClaim(new Claim(ClaimTypes.Gender, gender, ClaimValueTypes.String, context.Options.ClaimsIssuer));
            }

            string picture = VkHelper.GetPicture(user);
            if(!string.IsNullOrEmpty(picture))
            {
                // TODO: Use contant instead of urn:vme:picture_url
                identity.AddClaim(new Claim("urn:vme:picture_url", picture, ClaimValueTypes.String, context.Options.ClaimsIssuer));
            }
        }

        private string FormatFields(VkOptions options)
        {
            return string.Join(",", options.Fields);
        }
    }
}
