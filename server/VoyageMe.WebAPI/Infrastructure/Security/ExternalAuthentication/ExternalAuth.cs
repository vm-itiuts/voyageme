﻿using System;
using System.Globalization;
using System.Security.Claims;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.OAuth;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;

using VoyageMe.Common.Models;
using VoyageMe.Common.Models.Security;
using VoyageMe.BLL.Abstract.Services;
using VoyageMe.WebAPI.Infrastructure.Security.Interface;
using VoyageMe.WebAPI.Models.Authentication;

namespace VoyageMe.WebAPI.Infrastructure.Security.ExternalAuthentication
{
    public class ExternalAuth
    {
        private readonly IAuthResponseGenerator _authResponseGenerator;
        private readonly IAccountService _accountService;
        private readonly IRegistrationService _registrationService;
        private readonly IAuthorizationService _authorizationService;

        private const string _signInScheme = "Bearer";

        private const string _pictureUrlClaimName = "urn:vme:picture_url";

        public ExternalAuth(
            IAuthResponseGenerator authResponseGenerator,
            IAccountService accountService,
            IRegistrationService registrationService,
            IAuthorizationService authorizationService)
        {
            _authResponseGenerator = authResponseGenerator;
            _accountService = accountService;
            _registrationService = registrationService;
            _authorizationService = authorizationService;
        }

        #region Public instance methods
        public FacebookOptions CreateFacebookOptions(IConfigurationSection configuration)
        {
            if (configuration == null)
            {
                throw new ArgumentNullException(nameof(configuration));
            }

            FacebookOptions options = new FacebookOptions()
            {
                AppId = configuration["Facebook:AppId"], // AppId == ClientId
                AppSecret = configuration["Facebook:AppSecret"], // AppSecret == ClientSecret
                SignInScheme = _signInScheme,
                Events = new OAuthEvents()
                {
                    OnCreatingTicket = context => 
                    {
                        string pictureUrl = context.User
                            .GetValue("picture")
                            .SelectToken("data")
                            .Value<string>("url");

                        string gender = context.User.Value<string>("gender");

                        context.Identity.AddClaim(new Claim(_pictureUrlClaimName, pictureUrl));
                        context.Identity.AddClaim(new Claim(ClaimTypes.Gender, gender)); // todo: add to database

                        return Task.CompletedTask;
                    },

                    OnTicketReceived = contenxt => this.HandleTicketReceived(contenxt, ExternalAuthenticationProvider.Facebook),
                    OnRemoteFailure = this.HandleRemoteFailure
                }
            };

            options.Scope.Add("user_birthday");

            options.Fields.Add("id");
            options.Fields.Add("gender");
            options.Fields.Add("picture");
            options.Fields.Add("birthday");

            return options;
        }

        public GoogleOptions CreateGoogleOptions(IConfigurationSection configuration)
        {
            if (configuration == null)
            {
                throw new ArgumentNullException(nameof(configuration));
            }

            GoogleOptions options = new GoogleOptions()
            {
                ClientId = configuration["Google:ClientId"],
                ClientSecret = configuration["Google:ClientSecret"],
                SignInScheme = _signInScheme,
                Events = new OAuthEvents()
                {
                    OnCreatingTicket = context =>
                    {
                        string pictureUrl = context.User
                            .GetValue("image")
                            .Value<string>("url");

                        string gender = context.User.Value<string>("gender"); // todo: add to database
                        string dateOfBirh = context.User.Value<string>("birthday"); // format: 0000-06-17 (yyyy-MM-dd)

                        context.Identity.AddClaim(new Claim(_pictureUrlClaimName, pictureUrl));
                        context.Identity.AddClaim(new Claim(ClaimTypes.Gender, gender));
                        context.Identity.AddClaim(new Claim(ClaimTypes.DateOfBirth, dateOfBirh));

                        return Task.CompletedTask;
                    },
                    OnTicketReceived = context => this.HandleTicketReceived(context, ExternalAuthenticationProvider.Google),
                    OnRemoteFailure = this.HandleRemoteFailure
                }
            };

            options.Scope.Add("https://www.googleapis.com/auth/plus.login");
            options.Scope.Add("https://www.googleapis.com/auth/user.birthday.read");

            return options;
        }

        public VkOptions CreateVkOptions(IConfigurationSection configuration)
        {
            VkOptions options = new VkOptions()
            {
                ClientId = configuration["Vk:ClientId"],
                ClientSecret = configuration["Vk:ClientSecret"],
                SignInScheme = _signInScheme,
                Events = new OAuthEvents()
                {
                    OnCreatingTicket = async context =>
                    {
                        VkHandler handler = new VkHandler();
                        await handler.CreatingVkAuthTicket(context);
                    },
                    OnTicketReceived = context => this.HandleTicketReceived(context, ExternalAuthenticationProvider.Vk),
                    OnRemoteFailure = this.HandleRemoteFailure
                }
            };

            options.Fields.Add("contacts");
            options.Fields.Add("bdate");
            options.Fields.Add("sex");
            options.Fields.Add("photo_max_orig");

            return options;
        }
        #endregion Public instance methods

        #region Private instance methods
        private Task HandleRemoteFailure(FailureContext context)
        {
            context.Response.Redirect($"/error?{context.Failure.Message}");
            context.HandleResponse();

            return Task.CompletedTask;
        }

        private async Task HandleTicketReceived(TicketReceivedContext context, ExternalAuthenticationProvider provider)
        {
            context.HandleResponse();
            context.Response.Clear(); // clear response cookies

            string externalId = context.Principal.FindFirst(ClaimTypes.NameIdentifier).Value;
            Account account = await _accountService.GetAccountWithUserByExternalIdAsync(externalId, provider);

            if (account == null)
            {
                Account externalAccount = this.ExtractAccount(context); // data come from external auth provider
                User externalUser = this.ExtractUser(context, provider);

                externalAccount.ExternalAuthProvider = provider;

                if (externalAccount.Email != null)
                {
                    Account internalAccount = await _accountService.GetAccountWithUserByEmailAsync(externalAccount.Email);
                    User internalUser;

                    if (internalAccount != null) // Account with specified email exists. => User already registered on site using another way. => Need to merge internal and external account
                    {
                        internalUser = internalAccount.User;

                        await _accountService.AddExternalAccountToExisting(internalAccount, externalAccount.ExternalId, provider);
                    }
                    else
                    {
                        await _registrationService.RegisterUserAsync(externalAccount, externalUser);
                    }
                }
                else
                {
                    await _registrationService.RegisterUserAsync(externalAccount, externalUser);
                }

                account = await _accountService.GetAccountWithUserByExternalIdAsync(externalAccount.ExternalId, provider);
            }

            SuccessfulAuthResponse response = _authResponseGenerator.GenerateSuccess(account, account.User);
            await _authorizationService.CreateSession(account.Id, response.AccessToken, response.RefreshToken);

            context.Response.Cookies.Append("vme-ext-auth-resp", response.ToJson()); // todo: maybe better solution is override ToString() ?
            context.Response.Redirect(context.ReturnUri);
        }

        private Account ExtractAccount(TicketReceivedContext context)
        {
            ClaimsPrincipal principal = context.Principal;

            Account account = new Account() // todo: replace to view model?
            {
                Email = principal.FindFirst(ClaimTypes.Email)?.Value,
                ExternalId = principal.FindFirst(ClaimTypes.NameIdentifier).Value,
                CreationDate = DateTime.UtcNow
            };

            return account;
        }

        private User ExtractUser(TicketReceivedContext context, ExternalAuthenticationProvider provider)
        {
            ClaimsPrincipal principal = context.Principal;

            User user = new User() // todo: replace to view model?
            {
                FirstName = principal.FindFirst(ClaimTypes.GivenName).Value,
                SecondName = principal.FindFirst(ClaimTypes.Surname).Value,
                PhotoUrl = principal.FindFirst(_pictureUrlClaimName)?.Value
            };

            user.DateOfBirth = this.ExtractDateOfBirth(context, provider);

            return user;
        }

        private DateTime? ExtractDateOfBirth(TicketReceivedContext context, ExternalAuthenticationProvider provider)
        {
            DateTime? result = null;

            string dateFormat = null;
            string regexPattern = null;

            switch(provider)
            {
                case ExternalAuthenticationProvider.Vk:
                    dateFormat = "dd.MM.yyyy";
                    regexPattern = "(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\\d\\d";
                    break;
                case ExternalAuthenticationProvider.Facebook:
                    dateFormat = "MM/dd/yyyy";
                    regexPattern = "(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)\\d\\d";
                    break;
                case ExternalAuthenticationProvider.Google:
                    dateFormat = "yyyy-MM-dd";
                    regexPattern = "(19|20)\\d\\d[- /.](0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])"; 
                    break;
            }

            string dateOfBirthString = context.Principal.FindFirst(ClaimTypes.DateOfBirth)?.Value;
            bool dateOfBirthPresented = dateOfBirthString != null && Regex.IsMatch(dateOfBirthString, regexPattern);

            if (dateOfBirthPresented)
            {
                result = DateTime.ParseExact(dateOfBirthString, dateFormat, new CultureInfo("ru-ru"));
            }

            return result;
        }
        #endregion Private instance methods
    }
}
