﻿using AutoMapper;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VoyageMe.WebAPI.Infrastructure.AutoMapperConfig
{
    public static class AutoMapperServicesExtensions
    {
        public static IServiceCollection AddAutoMapper(this IServiceCollection services)
        {
            MapperConfiguration config = new MapperConfiguration(cfg => cfg.AddProfile<AutoMapperProfileConfiguration>());

            services.AddSingleton(config.CreateMapper());

            return services;
        }
    }
}
