﻿using AutoMapper;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VoyageMe.BLL.Abstract.Models.Booking;
using VoyageMe.Common.Models;
using VoyageMe.WebAPI.Models.Authentication;
using VoyageMe.WebAPI.Models.Booking;
using VoyageMe.WebAPI.Models.Feedback;
using VoyageMe.WebAPI.Models.Route;
using VoyageMe.WebAPI.Models.Search;
using VoyageMe.WebAPI.Models.Transport;

namespace VoyageMe.WebAPI.Infrastructure.AutoMapperConfig
{
    public class AutoMapperProfileConfiguration : Profile
    {
        public AutoMapperProfileConfiguration()
        {
            CreateRouteViewModelMaps();
            CreateRouteInputModelMaps();

            CreateTransportModelMaps();

            CreateSearchViewModelMaps();

            CreateBookingInputModelMaps();

            CreateUserAndAccountMaps();

            CreateFeedbackModelMaps();
        }

        private void CreateRouteInputModelMaps()
        {
            CreateMap<RouteInputModel, Route>();
            CreateMap<WayPointInputModel, WayPoint>();
        }

        private void CreateRouteViewModelMaps()
        {
            CreateMap<Route, Models.Route.RouteViewModel>();
            CreateMap<User, UserViewModel>().AfterMap((s, d) => d.Email = s.Account?.Email);
            CreateMap<WayPoint, WayPointViewModel>();
            CreateMap<RoutePassenger, RoutePassengerViewModel>();
        }

        private void CreateSearchViewModelMaps()
        {
            CreateMap<Route, RouteSummaryViewModel>()
                .AfterMap((s, d) => d.FreeSeats = s.SeatsNumber - s.Passengers.Where(x => x.Approved ?? false)
                    .Aggregate(0, (current, next) => current + next.BookedSeats));
            CreateMap<User, UserSummaryViewModel>();
        }

        private void CreateBookingInputModelMaps()
        {
            CreateMap<NewBookInputModel, NewBookModel>();
            CreateMap<NewBookInputModel, RoutePassenger>().AfterMap((s, d) => d.BookedSeats = s.NumberOfSeats);
            CreateMap<BookingConfirmationInputModel, BookingConfirmationModel>();
            CreateMap<BookingRejectionInputModel, BookingRejectionModel>();
        }

        private void CreateUserAndAccountMaps()
        {
            CreateMap<RegistrationViewModel, Account>().AfterMap((s, d) => d.CreationDate = DateTime.UtcNow);
            CreateMap<RegistrationViewModel, User>();
        }

        private void CreateTransportModelMaps()
        {
            CreateMap<Transport, TransportViewModel>().AfterMap((s, d) => d.Images = s.Images.Select(x => x.Url));
            CreateMap<TransportInputModel, Transport>()
                .ForMember(x => x.Images, opt => opt.MapFrom(x => x.Images.Select(url => new TransportImage { Url = url })));
        }

        private void CreateFeedbackModelMaps()
        {
            CreateMap<Feedback, FeedbackViewModel>();
            CreateMap<PendingFeedback, PendingFeedbackViewModel>();
            CreateMap<FeedbackInputModel, Feedback>();
            CreateMap<Route, Models.Feedback.RouteViewModel>();
        }
    }
}
