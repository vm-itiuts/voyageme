﻿using System;

namespace VoyageMe.WebAPI.Infrastructure.Options
{
    public class JwtAuthenticationOptions
    {
        public string SecretKey { get; set; }

        public string TokenIssuer { get; set; }

        public int TokenExpiresInMinutes { get; set; }

        public TimeSpan TokenExpiresIn
        {
            get
            {
                return TimeSpan.FromMinutes(this.TokenExpiresInMinutes);
            }
        }
    }
}
