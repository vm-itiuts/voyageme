﻿namespace VoyageMe.WebAPI.Infrastructure.Constants.Authorization
{
    public static class AuthorizationPolicyNames
    {
        public const string PhoneConfirmedOnly = "PhoneConfirmedOnly";
    }
}
