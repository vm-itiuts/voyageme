﻿namespace VoyageMe.WebAPI.Infrastructure.Constants.Claims
{
    public static class SecurityClaimNames
    {
        public const string Permissions = "Permissions";
        public const string PhoneConfirmed = "PhoneConfirmed";
        public const string EmailConfirmed = "EmailConfirmed";
    }
}
