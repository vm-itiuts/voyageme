﻿namespace VoyageMe.WebAPI.Infrastructure.Constants.Claims
{
    public static class IdentityClaimNames
    {
        public const string Email = "Email";
        public const string Name = "Name";
        public const string Id = "Id";
    }
}
