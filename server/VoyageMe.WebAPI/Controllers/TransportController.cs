﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using VoyageMe.BLL.Abstract.Services;
using AutoMapper;
using VoyageMe.Common.Models;
using VoyageMe.BLL.Abstract.Validators;
using VoyageMe.Common.Validation;
using VoyageMe.WebAPI.Infrastructure.Extensions;
using VoyageMe.WebAPI.Models.Transport;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace VoyageMe.WebAPI.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class TransportController : Controller
    {
        private readonly ITransportService _transportService;
        private readonly IDriverValidator _driverValidator;
        private readonly IMapper _mapper;

        public TransportController(
            ITransportService transportService,
            IDriverValidator driverValidator,
            IMapper mapper)
        {
            _transportService = transportService;
            _driverValidator = driverValidator;
            _mapper = mapper;
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            int userId = User.GetId();
            IEnumerable<TransportViewModel> result = _mapper.Map<IEnumerable<TransportViewModel>>
                (await _transportService.GetUserTransportsAsync(userId));

            return Json(result);
        }


        [HttpPost]
        public async Task<IActionResult> Post([FromBody]TransportInputModel model)
        {
            model.OwnerId = User.GetId();
            if (ModelState.IsValid == false)
            {
                return BadRequest(ModelState);
            }

            TransportViewModel transport = _mapper.Map<TransportViewModel>
                (await _transportService.CreateTransportAsync(_mapper.Map<Transport>(model)));

            return Json(transport);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, [FromBody]TransportInputModel model)
        {
            model.OwnerId = User.GetId();
            if (ModelState.IsValid == false)
            {
                return BadRequest(ModelState);
            }
            ValidationResult validationResult = await _driverValidator
                .ValidateTransportOwnerAsync(model.Id, model.OwnerId);
            if (validationResult.IsValid == false)
            {
                return BadRequest(validationResult.Errors);
            }
            TransportViewModel transport = _mapper.Map<TransportViewModel>
                (await _transportService.UpdateTransportAsync(_mapper.Map<Transport>(model)));

            return Json(transport);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            int userId = User.GetId();
            ValidationResult validationResult = await _driverValidator.ValidateTransportOwnerAsync(id, userId);
            if (validationResult.IsValid == false)
            {
                return BadRequest(validationResult.Errors);
            }
            await _transportService.DeleteTransportAsync(id);

            return Ok();
        }
    }
}
