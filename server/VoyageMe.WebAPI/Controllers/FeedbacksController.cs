﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using VoyageMe.BLL.Abstract.Services;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using VoyageMe.BLL.Abstract.Validators;
using VoyageMe.Common.Models;
using VoyageMe.Common.Pagination;
using VoyageMe.Common.Validation;
using VoyageMe.WebAPI.Infrastructure.Extensions;
using VoyageMe.WebAPI.Models.Feedback;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace VoyageMe.WebAPI.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class FeedbacksController : Controller
    {
        private readonly IFeedbackService _feedbackService;
        private readonly IFeedbackValidator _feedbackValidator;
        private readonly IMapper _mapper;

        public FeedbacksController(
            IFeedbackService feedbackService,
            IFeedbackValidator feedbackValidator,
            IMapper mapper)
        {
            _feedbackService = feedbackService;
            _feedbackValidator = feedbackValidator;
            _mapper = mapper;
        }

        [AllowAnonymous]
        [HttpGet("driver/{userId}")]
        public async Task<IActionResult> GetDriverFeedbacks(int userId, Pagination pagination)
        {
            IEnumerable<FeedbackViewModel> feedbacks = _mapper.Map<IEnumerable<FeedbackViewModel>>
                (await _feedbackService.GetDriverFeedbacksAsync(userId, pagination));
            return Json(feedbacks);
        }

        [HttpGet("pending")]
        public async Task<IActionResult> GetPendingFeedbacks(Pagination pagination)
        {
            int userId = User.GetId();
            IEnumerable<PendingFeedbackViewModel> feedbacks = _mapper.Map<IEnumerable<PendingFeedbackViewModel>>
                (await _feedbackService.GetPendingFeedbacksAsync(userId, pagination));
            return Json(feedbacks);
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody]FeedbackInputModel model)
        {
            model.PassengerId = User.GetId();
            if(ModelState.IsValid == false)
            {
                return BadRequest(ModelState);
            }

            ValidationResult validationResult = 
                await _feedbackValidator.ValidateByRouteIdAsync(model.RouteId, model.PassengerId);
            if(validationResult.IsValid == false)
            {
                return BadRequest(validationResult.Errors);
            }

            FeedbackViewModel feedback = _mapper.Map<FeedbackViewModel>
                (await _feedbackService.CreateFeedbackAsync(_mapper.Map<Feedback>(model)));

            return Json(feedback);
        }

        [HttpPut("asLooked")]
        public async Task<IActionResult> Put([FromBody] IEnumerable<PendingFeedbackViewModel> feedbacks)
        {
            if(ModelState.IsValid == false)
            {
                return BadRequest(ModelState);
            }

            await _feedbackService.SaveFeedbacksAsLooked(_mapper.Map<IEnumerable<PendingFeedback>>
                (feedbacks));

            return Ok();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            ValidationResult validationResult = await _feedbackValidator.ValidateByIdAsync(id, User.GetId());
            if(validationResult.IsValid == false)
            {
                return BadRequest(validationResult.Errors);
            }

            await _feedbackService.DeletePendingFeedbackAsync(id);

            return Ok();
        }
    }
}
