﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using AutoMapper;
using VoyageMe.BLL.Abstract.Services;
using VoyageMe.BLL.Abstract.Validators;
using VoyageMe.Common.Models;
using VoyageMe.Common.Validation;
using VoyageMe.WebAPI.Infrastructure.Extensions;
using VoyageMe.WebAPI.Models.Route;

namespace VoyageMe.WebAPI.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class RouteController : Controller
    {
        private readonly IRouteService _routeService;
        private readonly IDriverValidator _driverValidator;
        private readonly IMapper _mapper;

        public RouteController(
            IRouteService routeService,
            IDriverValidator driverValidator,
            IMapper mapper)
        {
            _routeService = routeService;
            _driverValidator = driverValidator;
            _mapper = mapper;
        }

        // GET api/values/5
        [AllowAnonymous]
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            RouteViewModel route = _mapper.Map<RouteViewModel>(await _routeService.GetRouteAsync(id));

            return Json(route);
        }

        // POST api/values
        [HttpPost]
        public async Task<IActionResult> Post([FromBody]RouteInputModel model)
        {
            model.DriverId = User.GetId();
            if (ModelState.IsValid == false)
            {
                return BadRequest(ModelState);
            }

            RouteViewModel result = _mapper.Map<RouteViewModel>
                (await _routeService.CreateRouteAsync(_mapper.Map<Route>(model)));

            return Json(result);
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, [FromBody]RouteInputModel model)
        {
            model.DriverId = User.GetId();
            if (ModelState.IsValid == false)
            {
                return BadRequest(ModelState);
            }

            ValidationResult validationResult = await _driverValidator
                .ValidateRouteOwnerAsync(model.Id, model.DriverId);
            if (validationResult.IsValid == false)
            {
                return BadRequest(validationResult.Errors);
            }
            RouteViewModel result = _mapper.Map<RouteViewModel>
                (await _routeService.UpdateRouteAsync(_mapper.Map<Route>(model)));

            return Json(result);
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            int driverId = User.GetId();
            ValidationResult validationResult = await _driverValidator.ValidateRouteOwnerAsync(id, driverId);
            if (validationResult.IsValid == false)
            {
                return BadRequest(validationResult.Errors);
            }
            bool result = await _routeService.DeleteRouteAsync(id);

            return Ok();
        }
    }
}
