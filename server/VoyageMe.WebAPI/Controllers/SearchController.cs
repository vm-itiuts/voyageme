﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using AutoMapper;
using VoyageMe.BLL.Abstract.Services;
using VoyageMe.Common.Models;
using VoyageMe.WebAPI.Models.Search;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace VoyageMe.WebAPI.Controllers
{
    [Route("api/[controller]")]
    public class SearchController : Controller
    {
        private readonly ISearchService _searchService;
        private readonly IMapper _mapper;

        public SearchController(ISearchService searchService, IMapper mapper)
        {
            _searchService = searchService;
            _mapper = mapper;
        }

        [HttpGet]
        public async Task<IActionResult> Get(SearchInputModel model)
        {
            IEnumerable<RouteSummaryViewModel> result = _mapper.Map<IEnumerable<RouteSummaryViewModel>>
                (await _searchService.FindRoutesAsync(model.StartPlaceId, model.EndPlaceId, model.Pagination, model.Date));

            return Json(result);
        }

        [HttpGet]
        [Route("additionalRoutes")]
        public async Task<IActionResult> GetAdditionalRoutes(SearchInputModel model)
        {
            IEnumerable<RouteSummaryViewModel> result = _mapper.Map<IEnumerable<RouteSummaryViewModel>>
                (await _searchService.FindAdditionalRoutesAsync(model.StartPlaceId, model.EndPlaceId, model.Pagination, model.Date));

            return Json(result);
        }
    }
}