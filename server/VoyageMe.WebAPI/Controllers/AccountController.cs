﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Authentication;
using AutoMapper;

using VoyageMe.Common.Models;
using VoyageMe.Common.Validation;
using VoyageMe.BLL.Abstract.Services;
using VoyageMe.WebAPI.Infrastructure.Security.Interface;
using VoyageMe.WebAPI.Infrastructure.Constants.Claims;
using VoyageMe.WebAPI.Models.Authentication;

namespace VoyageMe.WebAPI.Controllers
{
    [Route("api")]
    public class AccountController : Controller
    {
        private readonly ISaltGenerator _saltGenerator;
        private readonly IPasswordManager _passwordManager;
        private readonly IAuthResponseGenerator _authResponseGenerator;
        private readonly IMapper _mapper;

        private readonly IRegistrationService _registrationService;
        private readonly IAccountService _accountService;
        private readonly IAuthorizationService _authorizationService;


        public AccountController(
            IRegistrationService registrationService,
            IAccountService accountService,
            IAuthorizationService authorizationService,
            ISaltGenerator saltGenerator,
            IPasswordManager passwordManager,
            IAuthResponseGenerator authResponseGenerator,
            IMapper mapper)
        {
            _saltGenerator = saltGenerator;
            _passwordManager = passwordManager;
            _authResponseGenerator = authResponseGenerator;
            _mapper = mapper;

            _registrationService = registrationService;
            _accountService = accountService;
            _authorizationService = authorizationService;
        }

        //[HttpPost("register")]
        [HttpPost]
        [Route("register")]
        public async Task<IActionResult> Register([FromBody] RegistrationViewModel model)
        {
            if (this.ModelState.IsValid == false)
            {
                return this.BadRequest(ModelState);
            }

            if (await _registrationService.IsAccountExistsAsync(model.Email))
            {
                return StatusCode(StatusCodes.Status409Conflict, "User with specified email already exist.");
            }

            Account account = _mapper.Map<Account>(model);
            User user = _mapper.Map<User>(model);

            byte[] salt = _saltGenerator.GenerateSalt();
            account.Salt = _saltGenerator.ConvertToString(salt);
            account.PasswordHash = _passwordManager.HashPassword(model.Password, salt);

            await _registrationService.RegisterUserAsync(account, user);

            SuccessfulAuthResponse successResponse = _authResponseGenerator.GenerateSuccess(account, user);
            await _authorizationService.CreateSession(account.Id, successResponse.AccessToken, successResponse.RefreshToken);

            return this.Ok(successResponse); // Maybe send Created (204) status code?
        }

        [HttpPost]
        [Route("login")]
        public async Task<IActionResult> Login([FromBody] LoginViewModel model)
        {
            IActionResult result;

            if (this.ModelState.IsValid)
            {
                Account account = await _accountService.GetAccountWithUserByEmailAsync(model.Email);
                User user;

                if (account != null && _passwordManager.VerifyPassword(model.Password, account.PasswordHash, account.Salt))
                {
                    // todo: The line below has commented because we don't have any permissions at this moment. (performance enhancement)
                    // account.Permissions = await _accountService.GetPermissionListAsync(account.Id); 
                    user = account.User;

                    SuccessfulAuthResponse successResponse = _authResponseGenerator.GenerateSuccess(account, user);
                    await _authorizationService.CreateSession(account.Id, successResponse.AccessToken, successResponse.RefreshToken);

                    result = this.Ok(successResponse);
                }
                else
                {
                    this.ModelState.AddModelError("", "Wrong email or password");

                    result = this.BadRequest(this.ModelState);
                }
            }
            else
            {
                result = this.BadRequest(this.ModelState);
            }

            return result;
        }

        [HttpPost]
        [Route("logout")]
        [Microsoft.AspNetCore.Authorization.Authorize]
        public async Task<IActionResult> Logout()
        {
            string accessToken = this.ExtractAccessTokenFromHeaders();
            int currentUserId = int.Parse(this.User.FindFirst(IdentityClaimNames.Id).Value);

            await _authorizationService.CloseSessionAsync(currentUserId, accessToken);

            return this.Ok();
        }

        [HttpPost]
        [Route("unp/auth/refresh")] // unp == unprotected
        public async Task<IActionResult> RefreshToken([FromBody] RefreshTokenViewModel model)
        {
            string accessToken = this.ExtractAccessTokenFromHeaders();

            if (accessToken == null)
            {
                return this.Unauthorized();
            }

            IActionResult response;
            if (model != null && this.ModelState.IsValid)
            {
                AuthSessionValidationResult sessionValidationResult = await _authorizationService.ValidateSessionAsync(accessToken, model.RefreshToken);

                if (sessionValidationResult.Success)
                {
                    Session session = sessionValidationResult.AuthSession;

                    Account account = session.Account;
                    User user = (await _accountService.GetAccountWithUserByIdAsync(account.Id)).User; // TODO: here extra join. Maybe _userService ?

                    await _authorizationService.CloseSessionAsync(session);

                    SuccessfulAuthResponse successResponse = _authResponseGenerator.GenerateSuccess(account, user);
                    await _authorizationService.CreateSession(account.Id, successResponse.AccessToken, successResponse.RefreshToken);

                    response = this.Ok(successResponse);
                }
                else
                {
                    FailedAuthResponse errorResponse = _authResponseGenerator.GenerateError(sessionValidationResult.Error);
                    response = this.BadRequest(errorResponse);
                }
            }
            else
            {
                response = this.BadRequest("refreshToken doesn't presented");
            }

            return response;
        }

        [Microsoft.AspNetCore.Authorization.Authorize]
        [HttpPost]
        [Route("auth/closesessions")]
        public async Task<IActionResult> CloseAllSessions()
        {
            int currentUserId = int.Parse(this.User.FindFirst(IdentityClaimNames.Email).Value);

            await _authorizationService.CloseAllSessionAsyncFor(currentUserId);

            Account account = await _accountService.GetAccountWithUserByIdAsync(currentUserId);
            User user = account.User;

            SuccessfulAuthResponse response = _authResponseGenerator.GenerateSuccess(account, user);
            await _authorizationService.CreateSession(account.Id, response.AccessToken, response.RefreshToken);

            return this.Ok(response);
        }

        [HttpPost]
        [Route("[controller]/[action]")] // todo: is /api/ method?
        public IActionResult ExternalLogin(string provider, string returnUrl = null)
        {
            // Request a redirect to the external login provider.
            string redirectUrl = returnUrl ?? "/";
            AuthenticationProperties properties = this.ConfigureExternalAuthenticationProperties(provider, redirectUrl);

            return this.Challenge(properties, provider);
        }

        // Copied from Microsoft.AspNetCore.Identity.SignInManager<TUser>
        private AuthenticationProperties ConfigureExternalAuthenticationProperties(string provider, string redirectUrl)
        {
            var properties = new AuthenticationProperties { RedirectUri = redirectUrl };
            properties.Items["LoginProvider"] = provider;

            return properties;
        }

        private string ExtractAccessTokenFromHeaders()
        {
            string token = null;

            string[] tokenParts = this.HttpContext.Request.Headers["Authorization"].ToString().Split(' ');

            if (tokenParts.Length > 1 && tokenParts[0].Equals("Bearer", StringComparison.OrdinalIgnoreCase))
            {
                token = tokenParts[1];
            }

            return token;
        }
    }
}
