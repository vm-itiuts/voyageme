﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using AutoMapper;
using VoyageMe.BLL.Abstract.Enums;
using VoyageMe.BLL.Abstract.Models.Booking;
using VoyageMe.BLL.Abstract.Services;
using VoyageMe.BLL.Abstract.Validators;
using VoyageMe.Common.Models;
using VoyageMe.Common.Validation;
using VoyageMe.WebAPI.Infrastructure.Constants.Claims;
using VoyageMe.WebAPI.Models.Booking;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace VoyageMe.WebAPI.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class BookingController : Controller
    {
        #region Private Instance Fields
        private readonly IBookService _bookService;
        private readonly IBookValidator _bookValidator;
        private readonly IMapper _mapper;
        #endregion

        public BookingController(
            IBookService bookService,
            IBookValidator bookValidator,
            IMapper mapper)
        {
            _bookService = bookService;
            _bookValidator = bookValidator;
            _mapper = mapper;
        }

        [HttpPost]
        [Route("book")]
        public async Task<IActionResult> Book([FromBody]NewBookInputModel model)
        {
            model.PassengerId = int.Parse(User.FindFirst(IdentityClaimNames.Id).Value);
            if (ModelState.IsValid == false)
            {
                return BadRequest(ModelState);
            }

            ValidationResult validationResult = 
                await _bookValidator.ValidateNewBookAsync(_mapper.Map<NewBookModel>(model));
            if (validationResult.IsValid == false)
            {
                return BadRequest(validationResult.Errors);
            }

            BookResult result = await _bookService.BookAsync(_mapper.Map<RoutePassenger>(model), model.BookType);

            return Json(new { Result = result.ToString() });
        }

        [HttpPost]
        [Route("confirm")]
        public async Task<IActionResult> Confirm([FromBody]BookingConfirmationInputModel model)
        {
            model.UserId = int.Parse(User.FindFirst(IdentityClaimNames.Id).Value);
            if (ModelState.IsValid == false)
            {
                return BadRequest(ModelState);
            }

            ValidationResult validationResult = 
                await _bookValidator.ValidateConfirmationAsync(_mapper.Map<BookingConfirmationModel>(model));
            if (validationResult.IsValid == false)
            {
                return BadRequest(validationResult.Errors);
            }

            ConfirmResult result = await _bookService.ConfirmAsync(model.BookingId, model.Code, model.BookType);

            return Json(new { Result = result.ToString() });
        }

        [HttpPost]
        [Route("reject")]
        public async Task<IActionResult> Reject([FromBody] BookingRejectionInputModel model)
        {
            model.UserId = int.Parse(User.FindFirst(IdentityClaimNames.Id).Value);
            if (ModelState.IsValid == false)
            {
                return BadRequest(ModelState);
            }

            ValidationResult validationResult = 
                await _bookValidator.ValidateRejectionAsync(_mapper.Map<BookingRejectionModel>(model));
            if(validationResult.IsValid == false)
            {
                return BadRequest(validationResult.Errors);
            }

            await _bookService.RejectAsync(model.BookingId);

            return Ok();
        }
    }
}
