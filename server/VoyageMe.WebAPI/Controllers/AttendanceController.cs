﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using AutoMapper;
using VoyageMe.BLL.Abstract.Services;
using VoyageMe.BLL.Abstract.Validators;
using VoyageMe.Common.Validation;
using VoyageMe.WebAPI.Infrastructure.Extensions;
using VoyageMe.WebAPI.Models.Attendance;
using VoyageMe.WebAPI.Models.Route;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace VoyageMe.WebAPI.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class AttendanceController : Controller
    {
        private readonly IAttendanceService _attendanceService;
        private readonly IDriverValidator _driverValidator;
        private readonly IMapper _mapper;

        public AttendanceController(
            IAttendanceService attendanceService,
            IDriverValidator driverValidator,
            IMapper mapper)
        {
            _attendanceService = attendanceService;
            _driverValidator = driverValidator;
            _mapper = mapper;
        }

        [HttpPost]
        [Route("confirm")]
        public async Task<IActionResult> Confirm([FromBody]AttendanceInputModel model)
        {
            model.DriverId = User.GetId();
            if (ModelState.IsValid == false)
            {
                return BadRequest(ModelState);
            }

            ValidationResult validationResult = await _driverValidator
                .ValidateRouteOwnerAsync(model.RouteId, model.DriverId);
            if (validationResult.IsValid == false)
            {
                return BadRequest(validationResult.Errors);
            }

            await _attendanceService.ConfirmAttendance(model.RouteId, model.PassengerId);

            return Ok();
        }

        [HttpPost]
        [Route("reject")]
        public async Task<IActionResult> Reject(AttendanceInputModel model)
        {
            model.DriverId = User.GetId();
            if (ModelState.IsValid == false)
            {
                return BadRequest(ModelState);
            }

            ValidationResult validationResult = await _driverValidator
                .ValidateRouteOwnerAsync(model.RouteId, model.DriverId);
            if (validationResult.IsValid == false)
            {
                return BadRequest(validationResult.Errors);
            }

            await _attendanceService.RejectAttendance(model.RouteId, model.PassengerId);

            return Ok();
        }

        [HttpGet]
        [Route("{id}/passengers")]
        public async Task<IActionResult> PassengerByRouteId(int id)
        {
            int driverId = User.GetId();
            ValidationResult validationResult = await _driverValidator.ValidateRouteOwnerAsync(id, driverId);
            if (validationResult.IsValid == false)
            {
                return BadRequest(validationResult.Errors);
            }
            IEnumerable<RoutePassengerViewModel> result = _mapper.Map<IEnumerable<RoutePassengerViewModel>>
                (await _attendanceService.GetPassengersAsync(id));

            return Json(result);
        }


    }
}
