using System;
using System.IO;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Logging;

using VoyageMe.Infrastructure.DependencyInjections;
using VoyageMe.WebAPI.Infrastructure.Constants.Authorization;
using VoyageMe.WebAPI.Infrastructure.Constants.Claims;
using VoyageMe.WebAPI.Infrastructure.Options;
using VoyageMe.WebAPI.Infrastructure.Security;
using VoyageMe.WebAPI.Infrastructure.Security.ExternalAuthentication;
using VoyageMe.WebAPI.Middleware.IndexResponseMiddleware;
using VoyageMe.WebAPI.Middleware.Security;
using VoyageMe.WebAPI.Infrastructure.AutoMapperConfig;

namespace VoyageMe.WebAPI
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();

            Configuration = builder.Build();

            Enviroment = env;
        }

        public IConfigurationRoot Configuration { get; }

        public IHostingEnvironment Enviroment { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDataAccessServices(Configuration);
            services.AddBusinesLogicServices();
            services.AddSecurityServices();
            services.AddAutoMapper();

            services.Configure<JwtAuthenticationOptions>(Configuration.GetSection("Authentication"));

            // Add framework services.
            services.AddMvc();

            services.AddAuthorization(options =>
            {
                options.AddPolicy(AuthorizationPolicyNames.PhoneConfirmedOnly, policy => policy.RequireClaim(SecurityClaimNames.PhoneConfirmed));
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            ExternalAuth externalAuth = app.ApplicationServices.GetService<ExternalAuth>();
            IConfigurationSection socialAuthConfigurations = Configuration.GetSection("SocialNetworks");

            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseStaticFiles(new StaticFileOptions
            {
                FileProvider = new PhysicalFileProvider(
                    Path.Combine(env.WebRootPath, env.EnvironmentName)),
                RequestPath = new PathString()
            });

            app.UseIndexResponse();
            app.UseFacebookAuthentication(externalAuth.CreateFacebookOptions(socialAuthConfigurations));
            app.UseGoogleAuthentication(externalAuth.CreateGoogleOptions(socialAuthConfigurations));
            app.UseVkAuthentication(externalAuth.CreateVkOptions(socialAuthConfigurations));
            app.UseTokenAuthentication();
            app.UseMvc();
        }
    }
}
