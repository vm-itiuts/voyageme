﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VoyageMe.WebAPI.Models.Route;

namespace VoyageMe.WebAPI.Models.Search
{
    public class RouteSummaryViewModel
    {
        public int Id { get; set; }

        public string FormattedStartPlace { get; set; }

        public string FormattedEndPlace { get; set; }

        public DateTime StartDateTime { get; set; }

        public int Price { get; set; }

        public int FreeSeats { get; set; }

        public UserSummaryViewModel Driver { get; set; }

        public List<WayPointViewModel> WayPoints { get; set; }
    }
}
