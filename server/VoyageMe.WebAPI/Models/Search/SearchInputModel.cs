﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using VoyageMe.Common.Pagination;

namespace VoyageMe.WebAPI.Models.Search
{
    public class SearchInputModel
    {
        [Required]
        public string StartPlaceId { get; set; }

        [Required]
        public string EndPlaceId { get; set; }

        public DateTime? Date { get; set; }

        [Required]
        public Pagination Pagination { get; set; }
    }
}