﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace VoyageMe.WebAPI.Models.Feedback
{
    public class FeedbackInputModel
    {
        public int Id { get; set; }

        [Required]
        [Range(0, int.MaxValue)]
        public int RouteId { get; set; }

        [Required]
        [Range(0, int.MaxValue)]
        public int DriverId { get; set; }

        [Required]
        [Range(0, int.MaxValue)]
        public int PassengerId { get; set; }

        public string Message { get; set; }

        [Required]
        [Range(1, 5)]
        public int Rating { get; set; }
    }
}
