﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VoyageMe.WebAPI.Models.Feedback
{
    public class PendingFeedbackViewModel
    {
        public int Id { get; set; }

        public int RouteId { get; set; }

        public int PassengerId { get; set; }

        public int DriverId { get; set; }

        public RouteViewModel Route { get; set; }
    }
}
