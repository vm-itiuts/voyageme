﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VoyageMe.WebAPI.Models.Feedback
{
    public class RouteViewModel
    {
        public int Id { get; set; }

        public string FormattedStartPlace { get; set; }

        public string FormattedEndPlace { get; set; }

        public DateTime StartDateTime { get; set; }

        public int Price { get; set; }
    }
}
