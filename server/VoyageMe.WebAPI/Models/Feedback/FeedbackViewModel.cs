﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VoyageMe.WebAPI.Models.Search;

namespace VoyageMe.WebAPI.Models.Feedback
{
    public class FeedbackViewModel
    {
        public string Message { get; set; }

        public int Rating { get; set; }

        public DateTime AddDate { get; set; }

        public UserSummaryViewModel Passenger { get; set; }
    }
}
