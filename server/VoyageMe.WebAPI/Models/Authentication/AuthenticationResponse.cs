﻿using Newtonsoft.Json;

namespace VoyageMe.WebAPI.Models.Authentication
{
    public abstract class AuthenticationResponse
    {
        public virtual string ToJson()
        {
            string result = JsonConvert.SerializeObject(this, Formatting.Indented);

            return result;
        }
    }
}
