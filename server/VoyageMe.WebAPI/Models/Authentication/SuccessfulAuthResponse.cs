﻿using Newtonsoft.Json;

namespace VoyageMe.WebAPI.Models.Authentication
{
    public class SuccessfulAuthResponse : AuthenticationResponse
    {
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }

        [JsonProperty("refresh_token")]
        public string RefreshToken { get; set; }

        [JsonProperty("expires_in")]
        public int ExpiresIn { get; set; }

        [JsonProperty("first_name")]
        public string FirstName { get; set; }
    }
}
