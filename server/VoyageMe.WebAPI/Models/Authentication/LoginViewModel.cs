﻿using System.ComponentModel.DataAnnotations;

namespace VoyageMe.WebAPI.Models.Authentication
{
    public class LoginViewModel
    {
        [Required]
        [EmailAddress]
        [MaxLength(50)]
        public string Email { get; set; }

        [Required]
        [MaxLength(50)]
        public string Password { get; set; }
    }
}
