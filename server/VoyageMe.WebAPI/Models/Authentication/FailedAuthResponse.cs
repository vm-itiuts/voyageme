﻿using Newtonsoft.Json;

using VoyageMe.Common.Validation.Errors;

namespace VoyageMe.WebAPI.Models.Authentication
{
    public class FailedAuthResponse : AuthenticationResponse
    {
        [JsonProperty("error")]
        public string Error { get; set; }

        [JsonProperty("error_description")]
        public string ErrorDescription { get; set; }

        [JsonProperty("error_code")]
        public int ErrorCode { get; set; }

        public FailedAuthResponse()
        {
        }

        public FailedAuthResponse(AuthError error)
        {
            this.Error = error.Name;
            this.ErrorDescription = error.Message;
            this.ErrorCode = error.Code;
        }
    }
}
