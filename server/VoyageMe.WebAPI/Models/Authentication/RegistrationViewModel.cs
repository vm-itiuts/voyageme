﻿using System;
using System.ComponentModel.DataAnnotations;

namespace VoyageMe.WebAPI.Models.Authentication
{
    public class RegistrationViewModel
    {
        [Required]
        [EmailAddress]
        [MaxLength(50)]
        public string Email { get; set; }

        [Required]
        [MinLength(7)]
        [MaxLength(50)]
        public string Password { get; set; }

        [Required]
        [MaxLength(50)]
        public string FirstName { get; set; }

        [Required]
        [MaxLength(50)]
        public string SecondName { get; set; }

        // TODO [RegularExpression(@"")] for phone number
        [MaxLength(15)]
        public string PhoneNumber { get; set; }

        public DateTime? DateOfBirth { get; set; }
    }
}
