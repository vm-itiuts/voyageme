﻿using System.ComponentModel.DataAnnotations;

namespace VoyageMe.WebAPI.Models.Authentication
{
    public class RefreshTokenViewModel
    {
        [Required]
        public string RefreshToken { get; set; }
    }
}
