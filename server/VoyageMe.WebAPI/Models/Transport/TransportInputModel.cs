﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using VoyageMe.Common.Models;

namespace VoyageMe.WebAPI.Models.Transport
{
    public class TransportInputModel
    {
        public int Id { get; set; }

        [Required]
        [MaxLength(30)]
        public string Model { get; set; }

        [Required]
        [Range(0, int.MaxValue)]
        public int OwnerId { get; set; }

        [Required]
        [MaxLength(10)]
        public string Number { get; set; }
        
        [Required]
        [Range(0, int.MaxValue)]
        public int TypeId { get; set; }

        [Required]
        [Range(1900, int.MaxValue)]
        public int Year { get; set; }

        public IEnumerable<string> Images { get; set; }

        public TransportType TransportType
        {
            get
            {
                return (TransportType)this.TypeId;
            }
        }
    }
}
