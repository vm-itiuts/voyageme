﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VoyageMe.Common.Models;

namespace VoyageMe.WebAPI.Models.Transport
{
    public class TransportViewModel
    {
        public int Id { get; set; }

        public string Model { get; set; }

        public string Number { get; set; }

        public int TypeId { get; set; }

        public string Type
        {
            get { return ((TransportType)TypeId).ToString(); }
        }

        public int Year { get; set; }
        
        public IEnumerable<string> Images { get; set; }
    }
}
