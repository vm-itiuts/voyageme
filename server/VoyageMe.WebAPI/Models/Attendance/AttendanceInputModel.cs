﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace VoyageMe.WebAPI.Models.Attendance
{
    public class AttendanceInputModel
    {
        [Required]
        [Range(0, int.MaxValue)]
        public int DriverId { get; set; }

        [Required]
        [Range(0, int.MaxValue)]
        public int PassengerId { get; set; }

        [Required]
        [Range(0, int.MaxValue)]
        public int RouteId { get; set; }
    }
}
