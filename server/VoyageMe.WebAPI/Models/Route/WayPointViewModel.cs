﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VoyageMe.WebAPI.Models.Route
{
    public class WayPointViewModel
    {
        public int Id { get; set; }

        public string PlaceId { get; set; }

        public string FormattedPlace { get; set; }

        public int OrderNumber { get; set; }
    }
}
