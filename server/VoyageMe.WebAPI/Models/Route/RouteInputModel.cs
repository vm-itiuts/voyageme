﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace VoyageMe.WebAPI.Models.Route
{
    public class RouteInputModel
    {
        public int Id { get; set; }

        [Required]
        public string StartPlaceId { get; set; }

        [Required]
        public string FormattedStartPlace { get; set; }

        [Required]
        public string EndPlaceId { get; set; }

        [Required]
        public string FormattedEndPlace { get; set; }

        [Required]
        public DateTime StartDateTime { get; set; }

        [Required]
        [Range(0, double.MaxValue)]
        public double Price { get; set; }

        [Required]
        [Range(0, int.MaxValue)]
        public int BookTypeId { get; set; }

        [Required]
        [Range(0, int.MaxValue)]
        public int SeatsNumber { get; set; }

        [RegularExpression(@"^\d{3}$")]
        public int? Code { get; set; }

        [Required]
        [Range(0, int.MaxValue)]
        public int TransportId { get; set; }

        [Required]
        [Range(0, int.MaxValue)]
        public int DriverId { get; set; }

        public List<WayPointInputModel> WayPoints { get; set; }
    }
}
