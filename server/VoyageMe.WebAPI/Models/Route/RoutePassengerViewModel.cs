﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VoyageMe.WebAPI.Models.Route
{
    public class RoutePassengerViewModel
    {
        public int Id { get; set; }

        public int BookedSeats { get; set; }

        public bool? Approved { get; set; }

        public UserViewModel Passenger { get; set; }

    }
}
