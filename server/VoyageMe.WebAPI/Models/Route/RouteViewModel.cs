﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using VoyageMe.Common.Models;
using VoyageMe.WebAPI.Models.Transport;

namespace VoyageMe.WebAPI.Models.Route
{
    public class RouteViewModel
    {
        public int Id { get; set; }

        public string StartPlaceId { get; set; }

        public string FormattedStartPlace { get; set; }

        public string EndPlaceId { get; set; }

        public string FormattedEndPlace { get; set; }

        public DateTime StartDateTime { get; set; }

        public double Price { get; set; }

        public int BookTypeId { get; set; }
        
        public string BookType { get { return ((BookType)BookTypeId).ToString(); } }

        public int SeatsNumber { get; set; }

        public DateTime AddDate { get; set; }

        public TransportViewModel Transport { get; set; }

        public UserViewModel Driver { get; set; }

        public List<WayPointViewModel> WayPoints { get; set; }

        public List<RoutePassengerViewModel> Passengers { get; set; }
    }

}
