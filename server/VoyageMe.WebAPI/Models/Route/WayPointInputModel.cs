﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace VoyageMe.WebAPI.Models.Route
{
    public class WayPointInputModel
    {
        [Required]
        public string PlaceId { get; set; }
        
        [Required]
        public string FormattedPlace { get; set; }

        [Required]
        [Range(0, int.MaxValue)]
        public int OrderNumber { get; set; }
    }
}
