﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using VoyageMe.Common.Models;

namespace VoyageMe.WebAPI.Models.Booking
{
    public class BookingRejectionInputModel
    {
        [Required]
        [Range(0, int.MaxValue)]
        public int BookingId { get; set; }

        [Required]
        [Range(0, int.MaxValue)]
        public int RouteId { get; set; }
        
        [Required]
        [Range(0, int.MaxValue)]
        public int UserId { get; set; }
    }
}
