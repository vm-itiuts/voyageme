﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using VoyageMe.Common.Models;

namespace VoyageMe.WebAPI.Models.Booking
{
    public class NewBookInputModel
    {
        [Required]
        [Range(0, int.MaxValue)]
        public int RouteId { get; set; }
        
        [Required]
        [Range(0, int.MaxValue)]
        public int NumberOfSeats { get; set; }

        [Required]
        [Range(0, int.MaxValue)]
        public int BookTypeId { get; set; }

        [Required]
        [Range(0, int.MaxValue)]
        public int PassengerId { get; set; }

        public BookType BookType
        {
            get { return (BookType)BookTypeId; }
            set { BookTypeId = (int)value; }
        }
    }
}
