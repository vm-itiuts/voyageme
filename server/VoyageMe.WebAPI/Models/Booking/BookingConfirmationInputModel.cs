﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using VoyageMe.Common.Models;

namespace VoyageMe.WebAPI.Models.Booking
{
    public class BookingConfirmationInputModel
    {
        [Required]
        [Range(0, int.MaxValue)]
        public int RouteId { get; set; }

        [Required]
        [Range(0, int.MaxValue)]
        public int BookingId { get; set; }
        
        [RegularExpression(@"^\d{3}$")]
        public int? Code { get; set; }

        [Required]
        [Range(0, int.MaxValue)]
        public int BookTypeId { get; set; }

        public BookType BookType
        {
            get { return (BookType)BookTypeId; }
            set { BookTypeId = (int)value; }
        }

        [Required]
        [Range(0, int.MaxValue)]
        public int UserId { get; set; }
    }
}
