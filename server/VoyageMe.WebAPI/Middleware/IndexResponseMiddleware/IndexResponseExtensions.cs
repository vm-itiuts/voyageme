﻿using Microsoft.AspNetCore.Builder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VoyageMe.WebAPI.Middleware.IndexResponseMiddleware
{
    public static class IndexResponseExtensions
    {
        public static IApplicationBuilder UseIndexResponse(this IApplicationBuilder app)
        {
            app.UseMiddleware<IndexResponseMiddleware>();

            return app;
        }
    }
}
