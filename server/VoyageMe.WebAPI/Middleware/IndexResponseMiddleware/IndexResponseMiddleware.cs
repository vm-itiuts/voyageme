﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.StaticFiles;
using Microsoft.Extensions.FileProviders;
using Microsoft.Net.Http.Headers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace VoyageMe.WebAPI.Middleware.IndexResponseMiddleware
{
    public class IndexResponseMiddleware
    {
        private const string Index = "index.html";
        private const string DefaultContentType = "text/plain";


        private readonly RequestDelegate _next;
        private readonly IHostingEnvironment _env;
        private readonly IFileProvider _fileProvider;
        private readonly IContentTypeProvider _contentTypeProvider;
        private IFileInfo _fileInfo;
        private HttpContext _context;

        public IndexResponseMiddleware(RequestDelegate next, IHostingEnvironment env)
        {
            _next = next;
            _env = env;
            _fileProvider = env.WebRootFileProvider;
            _contentTypeProvider = new FileExtensionContentTypeProvider();
        }

        public Task Invoke(HttpContext context)
        {
            _context = context;
            if (IsApiRequest() || IsSocialSignin())
            {
                return _next(context);
            }
            else if (IsStaticFile())
            {
                return SendSatusAsync(StatusCodes.Status404NotFound);
            }
            else
            {
                _fileInfo = _fileProvider.GetFileInfo(Path.Combine(_env.EnvironmentName, Index));
                if (_fileInfo.Exists)
                {
                    ApplyResponseHeaders(StatusCodes.Status200OK);
                    return _context.Response.SendFileAsync(_fileInfo, _context.RequestAborted);
                }
                else
                {
                    return SendSatusAsync(StatusCodes.Status404NotFound);
                }
            }
        }

        #region Private Instance Methods
        private bool IsStaticFile()
        {
            Regex regex = new Regex(@"^.*\.(js|ttf|png|html|jpg|css|svg|ico)$");
            bool result = regex.IsMatch(_context.Request.Path.Value);
            return result;
        }

        private bool IsApiRequest()
        {
            bool result = _context.Request.Path.Value.StartsWith("/api/");
            return result;
        }

        private bool IsSocialSignin()
        {
            bool result = _context.Request.Path.Value.StartsWith("/signin-");
            return result;
        }

        private void ApplyResponseHeaders(int statusCode)
        {
            _context.Response.StatusCode = statusCode;

            if (statusCode < 400)
            {
                string contentType;
                _contentTypeProvider.TryGetContentType(_fileInfo.PhysicalPath, out contentType);

                _context.Response.ContentType = contentType ?? DefaultContentType;
                _context.Response.GetTypedHeaders().Headers[HeaderNames.AcceptRanges] = "bytes";
            }
        } 

        private Task SendSatusAsync(int statusCode)
        {
            ApplyResponseHeaders(statusCode);
            return Task.CompletedTask;
        }
        #endregion
    }
}
