﻿using Microsoft.AspNetCore.Builder;

namespace VoyageMe.WebAPI.Middleware.Security
{
    public static class AuthenticationExtensions
    {
        public static IApplicationBuilder UseTokenAuthentication(this IApplicationBuilder app)
        {
            app.UseMiddleware<AuthenticationMiddleware>();

            return app;
        }
    }
}
