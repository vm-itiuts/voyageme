﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;

using VoyageMe.Common.Validation.Errors;
using VoyageMe.WebAPI.Models.Authentication;
using VoyageMe.WebAPI.Infrastructure.Security.Interface.Token;
using VoyageMe.WebAPI.Infrastructure.Security.Interface;
using VoyageMe.WebAPI.Infrastructure.Exceptions.Token;
using VoyageMe.WebAPI.Infrastructure.Constants;
using VoyageMe.WebAPI.Infrastructure.Constants.Claims;
using VoyageMe.WebAPI.Infrastructure.Exceptions;
using VoyageMe.WebAPI.Infrastructure.Options;

namespace VoyageMe.WebAPI.Middleware.Security
{
    public class AuthenticationMiddleware
    {
        private const string HeaderName = "Authorization";

        private readonly RequestDelegate _next;
        private readonly IAccessTokenManager _jwtTokenManager;
        private readonly IAuthResponseGenerator _authResponseGenerator;
        private readonly JwtAuthenticationOptions _authenticationOptions;

        public AuthenticationMiddleware(
            RequestDelegate next, 
            IAccessTokenManager jwtTokenManager,
            IAuthResponseGenerator authResponseGenerator,
            IOptions<JwtAuthenticationOptions> authenticationOptions)
        {
            _next = next;
            _jwtTokenManager = jwtTokenManager;
            _authResponseGenerator = authResponseGenerator;
            _authenticationOptions = authenticationOptions.Value;
        }

        public Task Invoke(HttpContext context)
        {
            if (this.IsAuthHeaderCheckNotNeeded(context))
            {
                return _next(context);
            }

            string authHeader = context.Request.Headers[HeaderName];

            if (authHeader != null && authHeader.StartsWith("Bearer", StringComparison.OrdinalIgnoreCase))
            {
                string[] tokenParts = authHeader.Split(' ');

                if (tokenParts.Length > 1)
                {
                    string token = tokenParts[1];

                    try
                    {
                        JwtPayload tokenPayload = _jwtTokenManager.DecodeToObject<JwtPayload>(token, _authenticationOptions.SecretKey, true);

                        context.User = new ClaimsPrincipal(this.CreateIdentity(tokenPayload));
                    }
                    catch (SignatureVerificationException)
                    {
                        // Invalid signature of token (Token is forgery)
                        return this.WriteErrorInHttpResponseAsync(context, AuthError.CreateAccessTokenIsForgeryError());
                    }
                    catch (InvalidBase64Exception)
                    {
                        return this.WriteErrorInHttpResponseAsync(context, AuthError.CreateAccessTokenIsForgeryError());
                    }
                    catch (TokenExpiredException)
                    {
                        return this.WriteErrorInHttpResponseAsync(context, AuthError.CreateAccessTokenExpiredError());
                    }
                }
            }

            return _next(context);
        }

        private ClaimsIdentity CreateIdentity(JwtPayload payload)
        {
            if (payload == null)
            {
                throw new ArgumentNullException(nameof(payload));
            }

            ClaimsIdentity identity = new ClaimsIdentity("BearerToken");

            identity.AddClaim(new Claim(IdentityClaimNames.Email, (string)payload[IdentityClaimNames.Email]));
            identity.AddClaim(new Claim(IdentityClaimNames.Id, payload[IdentityClaimNames.Id].ToString()));

            if (payload.ContainsKey(SecurityClaimNames.EmailConfirmed) && (bool)payload[SecurityClaimNames.EmailConfirmed])
            {
                identity.AddClaim(new Claim(SecurityClaimNames.EmailConfirmed, string.Empty));
            }

            if (payload.ContainsKey(SecurityClaimNames.PhoneConfirmed) && (bool)payload[SecurityClaimNames.PhoneConfirmed])
            {
                identity.AddClaim(new Claim(SecurityClaimNames.PhoneConfirmed, string.Empty));
            }

            foreach (string permissionName in this.ExtractPermissionList(payload))
            {
                identity.AddClaim(new Claim(permissionName, string.Empty));
            }

            return identity;
        }

        private IEnumerable<string> ExtractPermissionList(JwtPayload payload)
        {
            var permissions = new List<string>();

            if (payload.ContainsKey(SecurityClaimNames.Permissions))
            {
                string permissionsString = (string)payload[SecurityClaimNames.Permissions];

                permissions.AddRange(permissionsString.Split(','));
            }

            return permissions;
        }

        private async Task WriteErrorInHttpResponseAsync(HttpContext context, AuthError error)
        {
            context.Response.StatusCode = StatusCodes.Status401Unauthorized;
            var x = context.Response.ContentType = HttpContentTypes.ApplicationJson;

            FailedAuthResponse errorResponse = _authResponseGenerator.GenerateError(error);
            await context.Response.WriteAsync(errorResponse.ToJson());
        }

        private bool IsAuthHeaderCheckNotNeeded(HttpContext context)
        {
            // '/api/unp/' is unprotected api methods. (like AccountController.RefreshToken)
            // For such methods checking of 'Authorization' header will not be performed
            bool result = context.Request.Path.Value.StartsWith("/api/unp/");

            return result;
        }
    }
}
