﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VoyageMe.Common.Models;

namespace VoyageMe.DAL.Abstract.Repositories
{
    public interface IPendingFeedbackRepository : IRepository<PendingFeedback, int>
    {
    }
}
