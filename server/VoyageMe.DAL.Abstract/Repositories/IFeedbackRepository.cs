﻿using VoyageMe.Common.Models;

namespace VoyageMe.DAL.Abstract.Repositories
{
    public interface IFeedbackRepository : IRepository<Feedback, int>
    {
    }
}
