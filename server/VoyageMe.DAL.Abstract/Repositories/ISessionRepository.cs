﻿using VoyageMe.Common.Models;

namespace VoyageMe.DAL.Abstract.Repositories
{
    public interface ISessionRepository : IRepository<Session, int>
    {
    }
}
