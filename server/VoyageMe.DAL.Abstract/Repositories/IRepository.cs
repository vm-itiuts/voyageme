﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq.Expressions;
using System.Threading.Tasks;

using VoyageMe.Common.Pagination;

namespace VoyageMe.DAL.Abstract.Repositories
{
    public interface IRepository<TEntity, TKey> where TEntity : class
    {
        IDbConnection Connection { get; }

        IRepositoryTransaction BeginTransaction();

        #region Synchronous methods

        TEntity Find<TChild1>(Expression<Func<TEntity, bool>> predicate, Expression<Func<TEntity, object>> tChild1, IRepositoryTransaction transaction = null);

        TEntity Find(Expression<Func<TEntity, bool>> predicate, IRepositoryTransaction transaction = null);

        TEntity Find(IRepositoryTransaction transaction = null);

        IEnumerable<TEntity> FindAll(IRepositoryTransaction transaction = null);

        IEnumerable<TEntity> FindAll(Pagination pagination, IRepositoryTransaction transaction = null);

        IEnumerable<TEntity> FindAll(Expression<Func<TEntity, bool>> predicate, IRepositoryTransaction transaction = null);

        IEnumerable<TEntity> FindAll(Expression<Func<TEntity, bool>> predicate, Pagination pagination, IRepositoryTransaction transaction = null);

        IEnumerable<TEntity> FindAll<TChild1>(Expression<Func<TEntity, object>> tChild1, IRepositoryTransaction transaction = null);

        IEnumerable<TEntity> FindAll<TChild1>(Expression<Func<TEntity, object>> tChild1, Pagination pagination, IRepositoryTransaction transaction = null);

        IEnumerable<TEntity> FindAll<TChild1>(Expression<Func<TEntity, bool>> predicate, Expression<Func<TEntity, object>> tChild1, IRepositoryTransaction transaction = null);

        IEnumerable<TEntity> FindAll<TChild1>(Expression<Func<TEntity, bool>> predicate, Expression<Func<TEntity, object>> tChild1, Pagination pagination, IRepositoryTransaction transaction = null);

        bool Insert(TEntity instance, IRepositoryTransaction transaction = null);

        bool Delete(TKey id, IRepositoryTransaction transaction = null);

        bool Update(TEntity instance, IRepositoryTransaction transaction = null);

        IEnumerable<TEntity> FindAllBetween(object from, object to, Expression<Func<TEntity, object>> btwField, IRepositoryTransaction transaction = null);

        IEnumerable<TEntity> FindAllBetween(object from, object to, Expression<Func<TEntity, object>> btwField, Pagination pagination, IRepositoryTransaction transaction = null);

        IEnumerable<TEntity> FindAllBetween(object from, object to, Expression<Func<TEntity, object>> btwField, Expression<Func<TEntity, bool>> predicate, IRepositoryTransaction transaction = null);

        IEnumerable<TEntity> FindAllBetween(object from, object to, Expression<Func<TEntity, object>> btwField, Expression<Func<TEntity, bool>> predicate, Pagination pagination, IRepositoryTransaction transaction = null);

        IEnumerable<TEntity> FindAllBetween(DateTime from, DateTime to, Expression<Func<TEntity, object>> btwField, IRepositoryTransaction transaction = null);

        IEnumerable<TEntity> FindAllBetween(DateTime from, DateTime to, Expression<Func<TEntity, object>> btwField, Pagination pagination, IRepositoryTransaction transaction = null);

        IEnumerable<TEntity> FindAllBetween(DateTime from, DateTime to, Expression<Func<TEntity, object>> btwField, Expression<Func<TEntity, bool>> predicate, IRepositoryTransaction transaction = null);

        IEnumerable<TEntity> FindAllBetween(DateTime from, DateTime to, Expression<Func<TEntity, object>> btwField, Expression<Func<TEntity, bool>> predicate, Pagination pagination, IRepositoryTransaction transaction = null);


        #endregion

        #region Async Methods

        Task<TEntity> FindAsync<TChild1>(Expression<Func<TEntity, object>> tChild1, IRepositoryTransaction transaction = null);

        Task<TEntity> FindAsync<TChild1>(Expression<Func<TEntity, bool>> predicate, Expression<Func<TEntity, object>> tChild1, IRepositoryTransaction transaction = null);

        Task<TEntity> FindAsync(Expression<Func<TEntity, bool>> predicate, IRepositoryTransaction transaction = null);

        Task<TEntity> FindAsync(IRepositoryTransaction transaction = null);

        Task<IEnumerable<TEntity>> FindAllAsync(IRepositoryTransaction transaction = null);

        Task<IEnumerable<TEntity>> FindAllAsync(Pagination pagination, IRepositoryTransaction transaction = null);

        Task<IEnumerable<TEntity>> FindAllAsync(Expression<Func<TEntity, bool>> predicate, IRepositoryTransaction transaction = null);

        Task<IEnumerable<TEntity>> FindAllAsync(Expression<Func<TEntity, bool>> predicate, Pagination pagination, IRepositoryTransaction transaction = null);

        Task<IEnumerable<TEntity>> FindAllAsync<TChild1>(Expression<Func<TEntity, object>> tChild1, IRepositoryTransaction transaction = null);

        Task<IEnumerable<TEntity>> FindAllAsync<TChild1>(Expression<Func<TEntity, object>> tChild1, Pagination pagination, IRepositoryTransaction transaction = null);

        Task<IEnumerable<TEntity>> FindAllAsync<TChild1>(Expression<Func<TEntity, bool>> predicate, Expression<Func<TEntity, object>> tChild1, IRepositoryTransaction transaction = null);

        Task<IEnumerable<TEntity>> FindAllAsync<TChild1>(Expression<Func<TEntity, bool>> predicate, Expression<Func<TEntity, object>> tChild1, Pagination pagination, IRepositoryTransaction transaction = null);

        Task<bool> InsertAsync(TEntity instance, IRepositoryTransaction transaction = null);

        Task<bool> DeleteAsync(TKey id, IRepositoryTransaction transaction = null);

        Task<bool> UpdateAsync(TEntity instance, IRepositoryTransaction transaction = null);

        Task<IEnumerable<TEntity>> FindAllBetweenAsync(object from, object to, Expression<Func<TEntity, object>> btwField, IRepositoryTransaction transaction = null);

        Task<IEnumerable<TEntity>> FindAllBetweenAsync(object from, object to, Expression<Func<TEntity, object>> btwField, Pagination pagination, IRepositoryTransaction transaction = null);

        Task<IEnumerable<TEntity>> FindAllBetweenAsync(object from, object to, Expression<Func<TEntity, object>> btwField, Expression<Func<TEntity, bool>> predicate, IRepositoryTransaction transaction = null);

        Task<IEnumerable<TEntity>> FindAllBetweenAsync(object from, object to, Expression<Func<TEntity, object>> btwField, Expression<Func<TEntity, bool>> predicate, Pagination pagination, IRepositoryTransaction transaction = null);

        Task<IEnumerable<TEntity>> FindAllBetweenAsync(DateTime from, DateTime to, Expression<Func<TEntity, object>> btwField, IRepositoryTransaction transaction = null);

        Task<IEnumerable<TEntity>> FindAllBetweenAsync(DateTime from, DateTime to, Expression<Func<TEntity, object>> btwField, Pagination pagination, IRepositoryTransaction transaction = null);

        Task<IEnumerable<TEntity>> FindAllBetweenAsync(DateTime from, DateTime to, Expression<Func<TEntity, object>> btwField, Expression<Func<TEntity, bool>> predicate, IRepositoryTransaction transaction = null);

        Task<IEnumerable<TEntity>> FindAllBetweenAsync(DateTime from, DateTime to, Expression<Func<TEntity, object>> btwField, Expression<Func<TEntity, bool>> predicate, Pagination pagination, IRepositoryTransaction transaction = null);

        #endregion
    }
}
