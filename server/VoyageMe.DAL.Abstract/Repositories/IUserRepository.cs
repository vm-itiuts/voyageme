﻿using VoyageMe.Common.Models;

namespace VoyageMe.DAL.Abstract.Repositories
{
    public interface IUserRepository : IRepository<User, int>
    {
    }
}
