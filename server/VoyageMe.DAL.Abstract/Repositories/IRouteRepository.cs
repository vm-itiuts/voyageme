﻿using VoyageMe.Common.Models;

namespace VoyageMe.DAL.Abstract.Repositories
{
    public interface IRouteRepository : IRepository<Route, int>
    {
    }
}
