﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using VoyageMe.Common.Models;
using VoyageMe.Common.Pagination;

namespace VoyageMe.DAL.Abstract.Repositories
{
    public interface IRouteSearchRepository
    {
        Task<IEnumerable<Route>> FindAllAsync(Expression<Func<Route, bool>> predicate, Pagination pagination,
            params Expression<Func<Route, object>>[] includes);
    }
}