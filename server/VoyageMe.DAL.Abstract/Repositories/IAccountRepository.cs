﻿using VoyageMe.Common.Models;

namespace VoyageMe.DAL.Abstract.Repositories
{
    public interface IAccountRepository : IRepository<Account, int>
    {
    }
}
