﻿using VoyageMe.Common.Models;

namespace VoyageMe.DAL.Abstract.Repositories
{
    public interface ITransportImageRepository : IRepository<TransportImage, int>
    {
    }
}
