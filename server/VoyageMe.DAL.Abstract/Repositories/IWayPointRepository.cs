﻿using VoyageMe.Common.Models;

namespace VoyageMe.DAL.Abstract.Repositories
{
    public interface IWayPointRepository : IRepository<WayPoint, int>
    {
    }
}
