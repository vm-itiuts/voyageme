﻿using VoyageMe.Common.Models;

namespace VoyageMe.DAL.Abstract.Repositories
{
    public interface IRoutePassengersRepository : IRepository<RoutePassenger, int>
    {
    }
}
