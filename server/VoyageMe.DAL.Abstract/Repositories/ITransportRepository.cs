﻿using VoyageMe.Common.Models;

namespace VoyageMe.DAL.Abstract.Repositories
{
    public interface ITransportRepository : IRepository<Transport, int>
    {
    }
}
