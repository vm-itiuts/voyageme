﻿using System.Data;

namespace VoyageMe.DAL.Abstract.Repositories
{
	public interface IRepositoryTransaction : IDbTransaction
	{
		 IDbTransaction Transaction { get; }
	}
}