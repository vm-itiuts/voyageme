﻿using System.Data;

using MicroOrm.Dapper.Repositories;

using VoyageMe.Common.Models;
using VoyageMe.DAL.Abstract.Repositories;

namespace VoyageMe.DAL.Repositories
{
    public class UserRepository : DapperRepository<User, int>, IUserRepository
    {
        public UserRepository(IDbConnection connection) : base(connection)
        {
        }
    }
}
