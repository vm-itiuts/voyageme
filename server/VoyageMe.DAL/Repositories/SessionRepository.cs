﻿using System.Data;

using MicroOrm.Dapper.Repositories;

using VoyageMe.Common.Models;
using VoyageMe.DAL.Abstract.Repositories;

namespace VoyageMe.DAL.Repositories
{
    public class SessionRepository : DapperRepository<Session, int>, ISessionRepository
    {
        public SessionRepository(IDbConnection connection) : base(connection)
        {
        }
    }
}
