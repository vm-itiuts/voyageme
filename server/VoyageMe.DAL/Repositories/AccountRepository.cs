﻿using System.Data;

using MicroOrm.Dapper.Repositories;

using VoyageMe.Common.Models;
using VoyageMe.DAL.Abstract.Repositories;

namespace VoyageMe.DAL.Repositories
{
    public class AccountRepository : DapperRepository<Account, int>, IAccountRepository
    {
        public AccountRepository(IDbConnection connection) : base(connection)
        {
        }
    }
}
