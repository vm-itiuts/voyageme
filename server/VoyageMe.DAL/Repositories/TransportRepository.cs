﻿using System.Data;

using MicroOrm.Dapper.Repositories;

using VoyageMe.Common.Models;
using VoyageMe.DAL.Abstract.Repositories;

namespace VoyageMe.DAL.Repositories
{
    public class TransportRepository : DapperRepository<Transport, int>, ITransportRepository
    {
        public TransportRepository(IDbConnection connection) : base(connection)
        {
        }
    }
}
