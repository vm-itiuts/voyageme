﻿using System.Data;

using MicroOrm.Dapper.Repositories;

using VoyageMe.Common.Models;
using VoyageMe.DAL.Abstract.Repositories;

namespace VoyageMe.DAL.Repositories
{
    public class RouteRepository : DapperRepository<Route, int>, IRouteRepository
    {
        public RouteRepository(IDbConnection connection) : base(connection)
        {
        }
    }
}
