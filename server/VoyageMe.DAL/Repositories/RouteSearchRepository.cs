﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VoyageMe.Common.EntityFramework;
using VoyageMe.Common.Models;
using VoyageMe.DAL.Abstract.Repositories;

namespace VoyageMe.DAL.Repositories
{
    public class RouteSearchRepository : EFRepository<Route, int>, IRouteSearchRepository
    {
        public RouteSearchRepository(AppDbContext context) : base(context) { }
    }
}