﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using MicroOrm.Dapper.Repositories;
using VoyageMe.Common.Models;
using VoyageMe.DAL.Abstract.Repositories;

namespace VoyageMe.DAL.Repositories
{
    public class PendingFeedbackRepository : DapperRepository<PendingFeedback, int>, IPendingFeedbackRepository
    {
        public PendingFeedbackRepository(IDbConnection connection) : base(connection)
        {
        }
    }
}
