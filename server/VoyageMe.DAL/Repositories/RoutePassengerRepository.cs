﻿using System.Data;

using MicroOrm.Dapper.Repositories;

using VoyageMe.Common.Models;
using VoyageMe.DAL.Abstract.Repositories;

namespace VoyageMe.DAL.Repositories
{
    public class RoutePassengerRepository : DapperRepository<RoutePassenger, int>, IRoutePassengersRepository
    {
        public RoutePassengerRepository(IDbConnection connection) : base(connection)
        {
        }
    }
}
