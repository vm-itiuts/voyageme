﻿using System.Data;

using MicroOrm.Dapper.Repositories;

using VoyageMe.Common.Models;
using VoyageMe.DAL.Abstract.Repositories;

namespace VoyageMe.DAL.Repositories
{
    public class TransportImageRepository : DapperRepository<TransportImage, int>, ITransportImageRepository
    {
        public TransportImageRepository(IDbConnection connection) : base(connection)
        {
        }
    }
}
