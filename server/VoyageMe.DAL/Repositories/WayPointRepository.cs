﻿using System.Data;

using MicroOrm.Dapper.Repositories;

using VoyageMe.Common.Models;
using VoyageMe.DAL.Abstract.Repositories;

namespace VoyageMe.DAL.Repositories
{
    public class WayPointRepository : DapperRepository<WayPoint, int>, IWayPointRepository
    {
        public WayPointRepository(IDbConnection connection) : base(connection)
        {
        }
    }
}
