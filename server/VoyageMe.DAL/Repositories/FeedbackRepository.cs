﻿using System.Data;

using MicroOrm.Dapper.Repositories;

using VoyageMe.Common.Models;
using VoyageMe.DAL.Abstract.Repositories;

namespace VoyageMe.DAL.Repositories
{
    public class FeedbackRepository : DapperRepository<Feedback, int>, IFeedbackRepository
    {
        public FeedbackRepository(IDbConnection connection) : base(connection)
        {
        }
    }
}
