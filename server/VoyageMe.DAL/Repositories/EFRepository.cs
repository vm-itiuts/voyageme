﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using VoyageMe.Common.EntityFramework;
using VoyageMe.Common.Pagination;

namespace VoyageMe.DAL.Repositories
{
    public class EFRepository<TEntity, TKey> where TEntity : class
    {
        private readonly AppDbContext _context;
        private readonly DbSet<TEntity> _dbSet;

        public EFRepository(AppDbContext context)
        {
            _context = context;
            _dbSet = context.Set<TEntity>();
        }

        public async Task<IEnumerable<TEntity>> FindAllAsync(Expression<Func<TEntity, bool>> predicate,
            Pagination pagination, params Expression<Func<TEntity, object>>[] includes)
        {
            IQueryable<TEntity> query = _dbSet;

            if (predicate != null)
            {
                query = query.Where(predicate);
            }

            foreach (var include in includes)
            {
                query = query.Include(include);
            }

            if (pagination != null)
            {
                query = query
                    .Skip((pagination.PageNumber - 1) * pagination.ItemsPerPage)
                    .Take(pagination.ItemsPerPage);
            }

            return await query.ToListAsync();
        }
    }
}