﻿using System;
using System.Data;
using VoyageMe.DAL.Abstract.Repositories;

namespace MicroOrm.Dapper.Repositories
{
    public class RepositoryTransaction : IRepositoryTransaction
    {
        private readonly IDbConnection _connection;
        private readonly IDbTransaction _innerTransaction;

        public RepositoryTransaction(IDbConnection connection)
        {
            _connection = connection;
            _connection.Open();
            _innerTransaction = _connection.BeginTransaction();
        }

        public IDbConnection Connection => _innerTransaction.Connection;

        public IsolationLevel IsolationLevel => _innerTransaction.IsolationLevel;

        public IDbTransaction Transaction => _innerTransaction;

        public void Commit()
        {
            _innerTransaction.Commit();
        }

        public void Rollback()
        {
            _innerTransaction.Rollback();
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    _connection.Close();
                    _innerTransaction.Dispose();
                }

                disposedValue = true;
            }
        }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion

    }
}