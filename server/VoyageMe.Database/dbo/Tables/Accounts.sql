﻿CREATE TABLE [dbo].[Accounts] (
    [Id]                     INT            IDENTITY (100017, 11) NOT NULL,
    [Email]                  NVARCHAR (50)  NULL,
    [PasswordHash]           NVARCHAR (MAX) NULL,
    [Salt]                   NVARCHAR (MAX) NULL,
    [EmailConfirmed]         BIT            DEFAULT ((0)) NOT NULL,
    [PhoneConfirmed]         BIT            DEFAULT ((0)) NOT NULL,
    [CreationDate]           DATETIME       NOT NULL,
    [ExternalAuthProviderId] INT            NULL,
    [ExternalId]             NVARCHAR (50)  NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Accounts_ExternalAuthenticationProviders] FOREIGN KEY ([ExternalAuthProviderId]) REFERENCES [dbo].[ExternalAuthenticationProviders] ([Id])
);

