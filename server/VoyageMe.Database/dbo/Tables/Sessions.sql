﻿CREATE TABLE [dbo].[Sessions] (
    [Id]                         INT            IDENTITY (1, 1) NOT NULL,
    [RefreshToken]               NVARCHAR (70)  NOT NULL,
    [AccessToken]                NVARCHAR (MAX) NOT NULL,
    [AccountId]                 INT  NOT NULL,
    [RefreshTokenExpirationDate] DATETIME       NOT NULL,
    [SessionActive]              BIT            NOT NULL,
    [AddDate]                    DATETIME       NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC), 
    CONSTRAINT [FK_Sessions_ToAccounts] FOREIGN KEY ([AccountId]) REFERENCES [Accounts]([Id])
);

