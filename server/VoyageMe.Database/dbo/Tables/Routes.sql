﻿CREATE TABLE [dbo].[Routes] (
    [Id]            INT            IDENTITY (1, 1) NOT NULL,
    [StartPlaceId]  NVARCHAR (MAX) NOT NULL,
    [FormattedStartPlace]  NVARCHAR (MAX) NULL,
    [EndPlaceId]    NVARCHAR (MAX) NOT NULL,
    [FormattedEndPlace]  NVARCHAR (MAX) NULL,
    [StartDateTime] DATETIME       NOT NULL,
    [Price]         FLOAT (53)     NOT NULL,
    [TransportId]   INT            NOT NULL,
    [BookTypeId]    INT            NOT NULL,
    [SeatsNumber]   INT            NOT NULL,
    [DriverId]      INT            NOT NULL,
    [AddDate]       DATETIME       NOT NULL,
    [Code]          INT            NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Routes_ToUsers] FOREIGN KEY ([DriverId]) REFERENCES [dbo].[Users] ([Id])
);

