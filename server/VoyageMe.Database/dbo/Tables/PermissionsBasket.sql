﻿CREATE TABLE [dbo].[PermissionsBasket] (
    [AccountId]    INT NOT NULL,
    [PermissionId] INT NOT NULL,
    PRIMARY KEY CLUSTERED ([AccountId] ASC, [PermissionId] ASC),
    CONSTRAINT [FK_RolesBasket_ToRoles] FOREIGN KEY ([PermissionId]) REFERENCES [dbo].[Permissions] ([Id]),
    CONSTRAINT [FK_RolesBasket_ToUsers] FOREIGN KEY ([AccountId]) REFERENCES [dbo].[Users] ([Id])
);

