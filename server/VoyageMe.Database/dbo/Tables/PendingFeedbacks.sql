﻿CREATE TABLE [dbo].[PendingFeedbacks] (
    [Id]          INT IDENTITY (1, 1) NOT NULL,
    [RouteId]     INT NOT NULL,
    [DriverId]    INT NOT NULL,
    [PassengerId] INT NOT NULL,
    [Looked] BIT NOT NULL DEFAULT 0, 
    PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_PendingFeedbacks_ToUsers_1] FOREIGN KEY ([DriverId]) REFERENCES [dbo].[Users] ([Id]),
    CONSTRAINT [FK_PendingFeedbacks_ToUsers_2] FOREIGN KEY ([PassengerId]) REFERENCES [dbo].[Users] ([Id]),
    CONSTRAINT [FK_PendingFeedbacks_ToRoutes] FOREIGN KEY ([RouteId]) REFERENCES [dbo].[Routes] ([Id])
);