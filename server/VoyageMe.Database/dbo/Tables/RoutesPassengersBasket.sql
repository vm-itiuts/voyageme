﻿CREATE TABLE [dbo].[RoutesPassengersBasket] (
    [Id]          INT IDENTITY (1, 1) NOT NULL,
    [RouteId]     INT NOT NULL,
    [PassengerId] INT NOT NULL,
    [BookedSeats] INT NOT NULL,
    [Approved]    BIT NULL,
    [Attended] BIT NULL, 
    PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_RoutesPassengersBasket_ToRoutes] FOREIGN KEY ([RouteId]) REFERENCES [dbo].[Routes] ([Id]) ON DELETE CASCADE,
    CONSTRAINT [FK_RoutesPassengersBasket_ToUsers] FOREIGN KEY ([PassengerId]) REFERENCES [dbo].[Users] ([Id])
);

