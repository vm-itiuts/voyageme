﻿CREATE TABLE [dbo].[Transports] (
    [Id]      INT           IDENTITY (1, 1) NOT NULL,
    [Model]    NVARCHAR (30) NOT NULL,
    [OwnerId] INT           NOT NULL,
    [Number]  NVARCHAR (10) NOT NULL,
    [TypeId]  INT           NOT NULL,
    [Year]    INT           NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Transports_ToTransportTypes] FOREIGN KEY ([TypeId]) REFERENCES [dbo].[TransportTypes] ([Id]),
    CONSTRAINT [FK_Transports_ToUsers] FOREIGN KEY ([OwnerId]) REFERENCES [dbo].[Users] ([Id]) ON DELETE CASCADE
);

