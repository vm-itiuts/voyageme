﻿CREATE TABLE [dbo].[Users] (
    [Id]           INT            NOT NULL,
    [FirstName]    NVARCHAR (50)  NOT NULL,
    [SecondName]   NVARCHAR (50)  NOT NULL,
    [PhoneNumber]  NVARCHAR (15)  NULL,
    [DateOfBirth]  DATE           NULL,
    [PhotoUrl]     NVARCHAR (MAX) NULL,
    [PositiveRank] INT            DEFAULT ((0)) NOT NULL,
    [NegativeRank] INT            DEFAULT ((0)) NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Users_Accounts] FOREIGN KEY ([Id]) REFERENCES [dbo].[Accounts] ([Id])
);

