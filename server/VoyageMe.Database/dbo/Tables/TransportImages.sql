﻿CREATE TABLE [dbo].[TransportImages] (
    [Id]          INT            IDENTITY (1, 1) NOT NULL,
    [Url]         NVARCHAR (MAX) NOT NULL,
    [TransportId] INT            NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_TransportImages_ToTransport] FOREIGN KEY ([TransportId]) REFERENCES [dbo].[Transports] ([Id]) ON DELETE CASCADE
);

