﻿CREATE TABLE [dbo].[ExternalAuthenticationProviders] (
    [Id]   INT           NOT NULL,
    [Name] NVARCHAR (20) NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);

