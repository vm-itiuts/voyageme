﻿CREATE TABLE [dbo].[WayPoints] (
    [Id]          INT            IDENTITY (1, 1) NOT NULL,
    [RouteId]     INT            NOT NULL,
    [PlaceId]     NVARCHAR (MAX) NOT NULL,
    [FormattedPlace]  NVARCHAR (MAX) NULL,
    [OrderNumber] INT            NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_WayPoints_ToRoutes] FOREIGN KEY ([RouteId]) REFERENCES [dbo].[Routes] ([Id]) ON DELETE CASCADE
);

