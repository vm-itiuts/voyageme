﻿CREATE TABLE [dbo].[Feedbacks] (
    [Id]          INT            IDENTITY (1, 1) NOT NULL,
    [DriverId]    INT            NOT NULL,
    [PassengerId] INT            NOT NULL,
    [RouteId]     INT            NOT NULL,
    [Message]     NVARCHAR (200) NULL,
    [Rating]      INT            NOT NULL,
    [AddDate] DATETIME NOT NULL, 
    PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Feedbacks_ToUsers_1] FOREIGN KEY ([DriverId]) REFERENCES [dbo].[Users] ([Id]),
    CONSTRAINT [FK_Feedbacks_ToUsers_2] FOREIGN KEY ([PassengerId]) REFERENCES [dbo].[Users] ([Id]),
    CONSTRAINT [FK_Feedbacks_ToRoutes] FOREIGN KEY ([RouteId]) REFERENCES [dbo].[Routes] ([Id])
);

