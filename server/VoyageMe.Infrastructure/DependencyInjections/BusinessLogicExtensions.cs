﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using VoyageMe.BLL.Abstract.Services;
using VoyageMe.BLL.Services;
using VoyageMe.BLL.Factories;
using VoyageMe.BLL.Abstract.Validators;
using VoyageMe.BLL.Validators;
using VoyageMe.BLL.DomainEvents.Abstract;
using VoyageMe.BLL.DomainEvents.Concrete;
using VoyageMe.BLL.DomainEvents.Concrete.Events;
using VoyageMe.BLL.DomainEvents.Concrete.Handlers;

namespace VoyageMe.Infrastructure.DependencyInjections
{
    public static class BusinessLogicExtensions
    {
        public static IServiceCollection AddBusinesLogicServices(this IServiceCollection services)
        {
            services.AddTransient<IRouteService, RouteService>();
            services.AddTransient<IRegistrationService, RegistrationService>();
            services.AddTransient<IAccountService, AccountService>();
            services.AddTransient<IAuthorizationService, AuthorizationService>();
            services.AddTransient<ISearchService, SearchService>();
            services.AddTransient<IBookService, BookService>();
            services.AddTransient<ITransportService, TransportService>();
            services.AddTransient<IAttendanceService, AttendanceService>();
            services.AddTransient<IFeedbackService, FeedbackService>();
            services.AddTransient<BookStrategyFactory>();

            services.AddDomainEventServices();

            services.AddTransient<IBookValidator, BookValidator>();
            services.AddTransient<IDriverValidator, DriverValidator>();
            services.AddTransient<IFeedbackValidator, FeedbackValidator>();

            return services;
        }

        private static IServiceCollection AddDomainEventServices(this IServiceCollection services)
        {
            services.AddScoped<IEventManager, EventManager>();
            services.AddTransient<IDomainEventHandler<PassengerAttendedEvent>, IncreasePassengerRatingEventHandler>();
            services.AddTransient<IDomainEventHandler<PassengerAttendedEvent>, AllowToAddFeedbackEventHandler>();
            services.AddTransient<IDomainEventHandler<PassengerNotAttendedEvent>, DecreasePassengerRatingEventHandler>();

            return services;
        }
    }
}
