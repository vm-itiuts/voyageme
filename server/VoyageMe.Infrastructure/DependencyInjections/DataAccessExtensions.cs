﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;
using VoyageMe.DAL.Abstract.Repositories;
using VoyageMe.DAL.Repositories;
using System.Data;
using System.Data.SqlClient;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using VoyageMe.Common.EntityFramework;

namespace VoyageMe.Infrastructure.DependencyInjections
{
    public static class DataAccessExtensions
    {
        public static IServiceCollection AddDataAccessServices(this IServiceCollection services, IConfigurationRoot config)
        {
            services.AddScoped<IDbConnection>(provider => new SqlConnection(config["ConnectionString"]));

            services.AddTransient<IAccountRepository, AccountRepository>();
            services.AddTransient<IFeedbackRepository, FeedbackRepository>();
            services.AddTransient<IWayPointRepository, WayPointRepository>();
            services.AddTransient<IRoutePassengersRepository, RoutePassengerRepository>();
            services.AddTransient<IRouteRepository, RouteRepository>();
            services.AddTransient<ITransportImageRepository, TransportImageRepository>();
            services.AddTransient<ITransportRepository, TransportRepository>();
            services.AddTransient<IUserRepository, UserRepository>();
            services.AddTransient<ISessionRepository, SessionRepository>();
            services.AddTransient<IPendingFeedbackRepository, PendingFeedbackRepository>();

            services.AddDbContext<AppDbContext>(options => options.UseSqlServer(config["ConnectionString"]));
            services.AddTransient<IRouteSearchRepository, RouteSearchRepository>();

            return services;
        }
    }
}
