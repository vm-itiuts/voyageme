﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VoyageMe.BLL.Abstract.Models.Booking;
using VoyageMe.Common.Validation;

namespace VoyageMe.BLL.Abstract.Validators
{
    public interface IBookValidator
    {
	    Task<ValidationResult> ValidateConfirmationAsync(BookingConfirmationModel model);

	    Task<ValidationResult> ValidateNewBookAsync(NewBookModel model);

        Task<ValidationResult> ValidateRejectionAsync(BookingRejectionModel model);
    }
}
