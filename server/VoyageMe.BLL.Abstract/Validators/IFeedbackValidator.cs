﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VoyageMe.Common.Validation;

namespace VoyageMe.BLL.Abstract.Validators
{
    public interface IFeedbackValidator
    {
        Task<ValidationResult> ValidateByIdAsync(int id, int passengerId);

        Task<ValidationResult> ValidateByRouteIdAsync(int routeId, int passengerId);
    }
}
