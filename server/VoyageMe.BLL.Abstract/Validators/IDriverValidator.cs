﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VoyageMe.Common.Models;
using VoyageMe.Common.Validation;

namespace VoyageMe.BLL.Abstract.Validators
{
    public interface IDriverValidator
    {
        Task<ValidationResult> ValidateRouteOwnerAsync(int routeId, int driverId);

        ValidationResult ValidateRouteOwner(Route route, int driverId);

        Task<ValidationResult> ValidateTransportOwnerAsync(int transportId, int driverId);
    }
}
