﻿using VoyageMe.Common.Models;

namespace VoyageMe.BLL.Abstract.Models.Booking
{
	public class BookingConfirmationModel
	{
		public int RouteId { get; set; }

		public int BookingId { get; set; }

		public int? Code { get; set; }

		public int BookTypeId { get; set; }

		public int UserId { get; set; }

		public BookType BookType
		{
			get { return (BookType) BookTypeId; }
			set { BookTypeId = (int) value; }
		}

	}
}