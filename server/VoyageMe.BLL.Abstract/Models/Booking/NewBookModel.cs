﻿using VoyageMe.Common.Models;

namespace VoyageMe.BLL.Abstract.Models.Booking
{
	public class NewBookModel
	{
		public int RouteId { get; set; }

		public int NumberOfSeats { get; set; }

		public int BookTypeId { get; set; }

		public int PassengerId { get; set; }

		public BookType BookType
		{
			get { return (BookType) BookTypeId; }
			set { BookTypeId = (int) value; }
		}
	}
}