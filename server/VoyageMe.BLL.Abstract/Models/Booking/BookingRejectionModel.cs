﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VoyageMe.BLL.Abstract.Models.Booking
{
    public class BookingRejectionModel
    {
        public int BookingId { get; set; }

        public int RouteId { get; set; }

        public int UserId { get; set; }
    }
}
