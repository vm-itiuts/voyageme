﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using VoyageMe.Common.Models;

namespace VoyageMe.BLL.Abstract.Services
{
    public interface IRouteService
    {
        Task<Route> GetRouteAsync(int id);

        Task<Route> CreateRouteAsync(Route route);

        Task<Route> UpdateRouteAsync(Route route);

        Task<bool> DeleteRouteAsync(int id);
    }
}
