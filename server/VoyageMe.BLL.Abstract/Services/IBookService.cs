﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VoyageMe.BLL.Abstract.Enums;
using VoyageMe.Common.Models;

namespace VoyageMe.BLL.Abstract.Services
{
    public interface IBookService
    {
        Task<BookResult> BookAsync(RoutePassenger passenger, BookType bookType);

        Task<ConfirmResult> ConfirmAsync(int id, int? code, BookType bookType);

        Task RejectAsync(int id);

        Task<bool> DeleteAsync(int id);
    }
}
