﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VoyageMe.Common.Models;

namespace VoyageMe.BLL.Abstract.Services
{
    public interface IAttendanceService
    {
        Task<IEnumerable<RoutePassenger>> GetPassengersAsync(int routeId);

        Task ConfirmAttendance(int routeId, int passengerId);

        Task RejectAttendance(int routeId, int passengerId);
    }
}
