﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VoyageMe.Common.Models;

namespace VoyageMe.BLL.Abstract.Services
{
    public interface ITransportService
    {
        Task<IEnumerable<Transport>> GetUserTransportsAsync(int userId);

        Task<Transport> CreateTransportAsync(Transport transport);

        Task<Transport> UpdateTransportAsync(Transport transport);

        Task DeleteTransportAsync(int id);
    }
}
