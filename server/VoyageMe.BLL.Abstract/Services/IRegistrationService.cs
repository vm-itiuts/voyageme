﻿using System.Threading.Tasks;

using VoyageMe.Common.Models;

namespace VoyageMe.BLL.Abstract.Services
{
    public interface IRegistrationService
    {
        Task<bool> IsAccountExistsAsync(string email);

        Task RegisterUserAsync(Account account, User user);
    }
}
