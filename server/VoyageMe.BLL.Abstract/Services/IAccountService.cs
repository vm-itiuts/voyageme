﻿using System.Collections.Generic;
using System.Threading.Tasks;

using VoyageMe.Common.Models;
using VoyageMe.Common.Models.Security;

namespace VoyageMe.BLL.Abstract.Services
{
    public interface IAccountService
    {
        Task<Account> GetAccountWithUserByEmailAsync(string email);

        Task<Account> GetAccountWithUserByIdAsync(int id);

        Task<Account> GetAccountWithUserByExternalIdAsync(string externalId, ExternalAuthenticationProvider provider);

        Task<List<Permission>> GetPermissionListAsync(int accountId);

        Task AddExternalAccountToExisting(Account existingAccount, string externalId, ExternalAuthenticationProvider provider);

        // change password

        // confirm password

        // confirm phone number
    }
}
