﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VoyageMe.Common.Models;
using VoyageMe.Common.Pagination;

namespace VoyageMe.BLL.Abstract.Services
{
    public interface IFeedbackService
    {
        Task<IEnumerable<Feedback>> GetDriverFeedbacksAsync(int driverId, Pagination pagination);

        Task<IEnumerable<PendingFeedback>> GetPendingFeedbacksAsync(int userId, Pagination pagination);

        Task<Feedback> CreateFeedbackAsync(Feedback feedback);

        Task SaveFeedbacksAsLooked(IEnumerable<PendingFeedback> feedbacks);

        Task<bool> DeletePendingFeedbackAsync(int id);
    }
}
