﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VoyageMe.Common.Models;
using VoyageMe.Common.Pagination;

namespace VoyageMe.BLL.Abstract.Services
{
    public interface ISearchService
    {
        Task<IEnumerable<Route>> FindRoutesAsync(string startPlaceId, string endPlaceId,
            Pagination pagination, DateTime? date = null);

        Task<IEnumerable<Route>> FindAdditionalRoutesAsync(string startPlaceId, string endPlaceId,
            Pagination pagination, DateTime? date = null);

    }
}