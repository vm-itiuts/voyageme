﻿using System.Threading.Tasks;

using VoyageMe.Common.Models;
using VoyageMe.Common.Validation;

namespace VoyageMe.BLL.Abstract.Services
{
    public interface IAuthorizationService
    {
        Task CreateSession(int accountId, string accessToken, string refreshToken);

        // todo: KeepSessionAlive(int accountId, string accessToken, string refreshToken);

        Task<AuthSessionValidationResult> ValidateSessionAsync(string accessToken, string refreshToken);

        Task CloseSessionAsync(int accountId, string accessToken);

        Task CloseSessionAsync(Session session);

        Task CloseAllSessionAsyncFor(int accountId);
    }
}
