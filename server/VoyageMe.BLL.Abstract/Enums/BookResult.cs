﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VoyageMe.BLL.Abstract.Enums
{
    public enum BookResult
    {
        Auto,
        Manual,
        WithCode,
        Error
    }
}
