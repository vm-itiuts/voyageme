﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VoyageMe.Common.Models;

namespace VoyageMe.Common.EntityFramework
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options) { }

        public DbSet<Route> Routes { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<WayPoint> WayPoints { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>().Ignore(x => x.Feedbacks);
            modelBuilder.Entity<User>().Ignore(x => x.Account);
            modelBuilder.Entity<User>().Ignore(x => x.Cars);
            modelBuilder.Entity<Feedback>().Ignore(x => x.Driver);
            modelBuilder.Entity<Feedback>().Ignore(x => x.Passenger);
        }

    }
}