﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VoyageMe.Common.Validation.Errors
{
    public class AuthError : Error
    {
        public int Code { get; private set; }

        protected AuthError(string name, string message, int code) : base(name, message)
        {
            this.Code = code;
        }

        public static AuthError CreateAccessTokenExpiredError()
        {
            return new AuthError("invalid_token", "The access token has expired.", 39);
        }

        public static AuthError CreateAccessTokenIsForgeryError()
        {
            return new AuthError("invalid_token", "The access token is forgery.", 38);
        }

        public static AuthError CreateRefreshTokenExpiredError()
        {
            return new AuthError("invalid_token", "The refresh token has expired.", 29);
        }

        public static AuthError CreateRefreshTokenAlreadyUsedError()
        {
            return new AuthError("invalid_token", "The refresh token has already been used.", 27);
        }

        public static AuthError CreateRefreshAndAccessTokensMistatchError()
        {
            return new AuthError("invalid_token", "Specified combination of access and refresh token doesn't exist.", 21);
        }
    }
}
