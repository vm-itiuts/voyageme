﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VoyageMe.Common.Validation.Errors
{
    public class FeedbackError : Error
    {
        private FeedbackError(string name, string message) : base(name, message)
        {
        }

        public static FeedbackError CreateNotFoundError()
        {
            return new FeedbackError("", "Feedback is not found");
        }

        public static FeedbackError CreateWrongPassengerError()
        {
            return new FeedbackError("", "You can't delete this pending feedback");
        }

        public static FeedbackError CreateFeedbackPermissionError()
        {
            return new FeedbackError("", "You don't have permission to leave feedback");
        }
    }
}
