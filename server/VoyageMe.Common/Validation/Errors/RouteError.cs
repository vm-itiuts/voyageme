﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VoyageMe.Common.Validation.Errors
{
    public class RouteError : Error
    {
        private RouteError(string name, string message) : base(name, message)
        {
        }

        public static RouteError CreateRouteNotFoundError()
        {
            return new RouteError("", "Route not found");
        }
    }
}
