﻿namespace VoyageMe.Common.Validation.Errors
{
    public class Error
    {
        public string Name { get; private set; }

        public string Message { get; private set; }


        protected Error(string name, string message)
        {
            this.Name = name;
            this.Message = message;
        }
    }
}
