﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VoyageMe.Common.Validation.Errors
{
    public class TransportError : Error
    {
        private TransportError(string name, string message) : base(name, message)
        {
        }

        public static TransportError CreateTransportOwnerError()
        {
            return new TransportError("", "You are not allowed modifying the transport");
        }

        public static TransportError CreateTransportNotFoundError()
        {
            return new TransportError("", "Transport not found");
        }
    }
}
