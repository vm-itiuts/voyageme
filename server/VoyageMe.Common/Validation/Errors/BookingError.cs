﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VoyageMe.Common.Validation.Errors
{
    public class BookingError : Error
    {
        private BookingError(string name, string message) : base(name, message)
        {
        }

        public static BookingError CreateBookTypeError()
        {
            return new BookingError("", "Wrong book type");
        }

        public static BookingError CreateFreeSeatsError()
        {
            return new BookingError("", "Not enough free seats");
        }

        public static BookingError CreateRouteOwnerError()
        {
            return new BookingError("", "You are not allowed modifying the booking");
        }

        public static BookingError CreatePassengerNotFoundError()
        {
            return new BookingError("", "You didn't register for this route");
        }
    }
}
