﻿using System;

using VoyageMe.Common.Models;
using VoyageMe.Common.Validation.Errors;

namespace VoyageMe.Common.Validation
{
    public class AuthSessionValidationResult // todo: inherit from validation result
    {
        public bool Success
        {
            get
            {
                return Error == null;
            }
        }

        public AuthError Error { get; private set; }

        public Session AuthSession { get; private set; }

        private AuthSessionValidationResult()
        {
        }

        public static AuthSessionValidationResult CreateSuccess(Session authSession)
        {
            if (authSession == null)
            {
                throw new ArgumentNullException(nameof(authSession));
            }

            return new AuthSessionValidationResult() { AuthSession = authSession };
        }

        public static AuthSessionValidationResult CreateError(AuthError error)
        {
            if (error == null)
            {
                throw new ArgumentNullException(nameof(error));
            }

            return new AuthSessionValidationResult() { Error = error };
        }
    }
}
