﻿using System.Collections.Generic;
using Microsoft.Extensions.Primitives;
using VoyageMe.Common.Validation.Errors;

namespace VoyageMe.Common.Validation
{
    public class ValidationResult
    {
        private readonly List<Error> errors = new List<Error>();

        public bool IsValid
        {
            get { return Errors.Count == 0; }
        }

        public IReadOnlyList<Error> Errors { get { return errors.AsReadOnly(); } }

        public void AddError(Error error)
        {
            errors.Add(error);
        }

        public void AddRangeErrors(IEnumerable<Error> otherErrors)
        {
            errors.AddRange(otherErrors);
        }

    }
}