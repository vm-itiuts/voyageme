﻿namespace VoyageMe.Common.Pagination
{
    public class Pagination
    {
        public int ItemsPerPage { get; set; }

        public int PageNumber { get; set; }
    }
}
