﻿namespace VoyageMe.Common.Models
{
    public enum TransportType
    {
        Sedan,
        Minivan,
        Minibus,
        Bus
    }
}
