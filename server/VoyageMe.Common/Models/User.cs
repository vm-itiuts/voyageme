﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

using MicroOrm.Dapper.Repositories.Attributes;
using MicroOrm.Dapper.Repositories.Attributes.Joins;

namespace VoyageMe.Common.Models
{
    [Table("Users")]
    public class User
    {
        [Key]
        public int Id { get; set; }

        public string FirstName { get; set; }

        public string SecondName { get; set; }

        public string PhoneNumber { get; set; }

        public DateTime? DateOfBirth { get; set; }

        public string PhotoUrl { get; set; }

        public int PositiveRank { get; set; }

        public int NegativeRank { get; set; }

        [InnerJoin("Accounts", "Id", "Id")]
        public Account Account { get; set; }

        [LeftJoin("Transports", "Id", "OwnerId")]
        public List<Transport> Cars { get; set; }

        [LeftJoin("Feedbacks", "Id", "DriverId")]
        public List<Feedback> Feedbacks { get; set; }
    }
}
