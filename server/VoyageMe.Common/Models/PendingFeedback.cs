﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using MicroOrm.Dapper.Repositories.Attributes;
using MicroOrm.Dapper.Repositories.Attributes.Joins;

namespace VoyageMe.Common.Models
{
    [Table("PendingFeedbacks")]
    public class PendingFeedback
    {
        [Key]
        [Identity]
        public int Id { get; set; }

        public int RouteId { get; set; }

        public int DriverId { get; set; }

        public int PassengerId { get; set; }

        public bool Looked { get; set; }

        [InnerJoin("Routes", "RouteId", "Id")]
        public Route Route { get; set; }

        [InnerJoin("Users", "DriverId", "Id")]
        public User Driver { get; set; }

        [InnerJoin("Users", "PassengerId", "Id")]
        public User Passenger { get; set; }
    }
}
