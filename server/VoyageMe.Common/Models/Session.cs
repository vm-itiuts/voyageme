﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

using MicroOrm.Dapper.Repositories.Attributes;
using MicroOrm.Dapper.Repositories.Attributes.Joins;

namespace VoyageMe.Common.Models
{
    [Table("Sessions")]
    public class Session
    {
        [Key]
        [Identity]
        public int Id { get; set; }

        public string RefreshToken { get; set; }

        public string AccessToken { get; set; }

        public int AccountId { get; set; }

        public DateTime RefreshTokenExpirationDate { get; set; }

        public bool SessionActive { get; set; } = true;

        public DateTime AddDate { get; set; }

        [InnerJoin("Accounts", "AccountId", "Id")]
        public Account Account { get; set; }
    }
}
