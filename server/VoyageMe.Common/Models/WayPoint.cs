﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

using MicroOrm.Dapper.Repositories.Attributes;

namespace VoyageMe.Common.Models
{
    [Table("WayPoints")]
    public class WayPoint
    {
        [Key]
        [Identity]
        public int Id { get; set; }

        public int RouteId { get; set; }

        public string PlaceId { get; set; }

        public string FormattedPlace { get; set; }

        public int OrderNumber { get; set; }
    }
}