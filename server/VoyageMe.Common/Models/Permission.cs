﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

using MicroOrm.Dapper.Repositories.Attributes;

namespace VoyageMe.Common.Models
{
    [Table("Permissions")]
    public class Permission
    {
        [Key]
        [Identity]
        public int Id { get; set; }

        public string Name { get; set; }
    }
}
