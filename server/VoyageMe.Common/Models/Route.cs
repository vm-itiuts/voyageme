﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

using MicroOrm.Dapper.Repositories.Attributes;
using MicroOrm.Dapper.Repositories.Attributes.Joins;

namespace VoyageMe.Common.Models
{
    [Table("Routes")]
    public class Route
    {
        [Key]
        [Identity]
        public int Id { get; set; }

        public string StartPlaceId { get; set; }

        public string FormattedStartPlace { get; set; }

        public string EndPlaceId { get; set; }

        public string FormattedEndPlace { get; set; }

        public DateTime StartDateTime { get; set; }

        public double Price { get; set; }

        public int TransportId { get; set; }

        public int BookTypeId { get; set; }

        public int SeatsNumber { get; set; }

        public int DriverId { get; set; }

        public DateTime AddDate { get; set; }

        public int? Code { get; set; }

        [InnerJoin("Transports", "TransportId", "Id")]
        public Transport Transport { get; set; }

        [InnerJoin("Users", "DriverId", "Id")]
        public User Driver { get; set; }

        [LeftJoin("WayPoints", "Id", "RouteId")]
        public List<WayPoint> WayPoints { get; set; }

        [LeftJoin("RoutesPassengersBasket", "Id", "RouteId")]
        public List<RoutePassenger> Passengers { get; set; }

        [NotMapped]
        public BookType BookType
        {
            get
            {
                return (BookType)this.BookTypeId;
            }
        }
    }
}