﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

using VoyageMe.Common.Models.Security;
using MicroOrm.Dapper.Repositories.Attributes;
using MicroOrm.Dapper.Repositories.Attributes.Joins;

namespace VoyageMe.Common.Models
{
    [Table("Accounts")]
    public class Account
    {
        [Key]
        [Identity]
        public int Id { get; set; }

        public string Email { get; set; }

        public string PasswordHash { get; set; }

        public string Salt { get; set; }

        public bool EmailConfirmed { get; set; }

        public bool PhoneConfirmed { get; set; }

        public DateTime CreationDate { get; set; }

        public string ExternalId { get; set; }

        public int? ExternalAuthProviderId { get; set; }

        [NotMapped]
        public ExternalAuthenticationProvider ExternalAuthProvider
        {
            get
            {
                if (this.ExternalAuthProviderId.HasValue)
                {
                    return ExternalAuthenticationProvider.None;
                }

                return (ExternalAuthenticationProvider)this.ExternalAuthProviderId.Value;
            }

            set
            {
                this.ExternalAuthProviderId = (int)value;
            }
        }

        [InnerJoin("Users", "Id", "Id")]
        public User User { get; set; }

        [LeftJoin("PermissionsBasket", "Id", "AccountId")]
        public List<Permission> Permissions { get; set; }
    }
}
