﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

using MicroOrm.Dapper.Repositories.Attributes;
using MicroOrm.Dapper.Repositories.Attributes.Joins;

namespace VoyageMe.Common.Models
{
    [Table("RoutesPassengersBasket")]
    public class RoutePassenger
    {
        [Key]
        [Identity]
        public int Id { get; set; }

        public int RouteId { get; set; }

        public int PassengerId { get; set; }

        public int BookedSeats { get; set; }

        public bool? Approved { get; set; }

        public bool? Attended { get; set; }
        
        [InnerJoin("Users", "PassengerId", "Id")]
        public User Passenger { get; set; }

        [InnerJoin("Routes", "RouteId", "Id")]
        public Route Route { get; set; }
    }
}
