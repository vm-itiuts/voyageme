﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

using MicroOrm.Dapper.Repositories.Attributes;

namespace VoyageMe.Common.Models
{
    [Table("TransportImages")]
    public class TransportImage
    {
        [Key]
        [Identity]
        public int Id { get; set; }

        public string Url { get; set; }

        public int TransportId { get; set; }
    }
}
