﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

using MicroOrm.Dapper.Repositories.Attributes;
using MicroOrm.Dapper.Repositories.Attributes.Joins;

namespace VoyageMe.Common.Models
{
    [Table("Feedbacks")]
    public class Feedback
    {
        [Key]
        [Identity]
        public int Id { get; set; }

        public int DriverId { get; set; }

        public int PassengerId { get; set; }

        public int RouteId { get; set; }

        public string Message { get; set; }

        public int? Rating { get; set; }

        public DateTime AddDate { get; set; }

        [InnerJoin("Users", "DriverId", "Id")]
        public User Driver { get; set; }

        [InnerJoin("Users", "PassengerId", "Id")]
        public User Passenger { get; set; }

        [InnerJoin("Routes", "RouteId", "Id")]
        public Route Route { get; set; }
    }
}
