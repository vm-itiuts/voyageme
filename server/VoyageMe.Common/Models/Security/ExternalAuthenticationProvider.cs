﻿namespace VoyageMe.Common.Models.Security
{
    public enum ExternalAuthenticationProvider
    {
        None,
        Vk,
        Facebook,
        Google
    }
}
