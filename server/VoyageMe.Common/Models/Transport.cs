﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

using MicroOrm.Dapper.Repositories.Attributes;
using MicroOrm.Dapper.Repositories.Attributes.Joins;

namespace VoyageMe.Common.Models
{
    [Table("Transports")]
    public class Transport
    {
        [Key]
        [Identity]
        public int Id { get; set; }

        public string Model { get; set; }

        public int OwnerId { get; set; }

        public string Number { get; set; }

        public int TypeId { get; set; }

        public int Year { get; set; }

        [InnerJoin("Users", "OwnerId", "Id")]
        public User Owner { get; set; }

        [LeftJoin("TransportImages", "Id", "TransportId")]
        public List<TransportImage> Images { get; set; }

        [NotMapped]
        public TransportType TransportType
        {
            get
            {
                return (TransportType)this.TypeId;
            }
        }
    }
}
