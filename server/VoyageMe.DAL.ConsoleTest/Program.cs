﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

using System.Data.SqlClient;

using VoyageMe.Common.Models;
using VoyageMe.Common.Pagination;
using VoyageMe.DAL.Abstract.Repositories;
using VoyageMe.DAL.Repositories;

namespace VoyageMe.DAL.ConsoleTest
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var connection = new SqlConnection(@"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=VoyageMe;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=True;ApplicationIntent=ReadWrite;MultiSubnetFailover=False");

            IUserRepository userRepo = new UserRepository(connection);
            IRouteRepository routeRepo = new RouteRepository(connection);

            var users = userRepo.FindAllAsync<Transport>(u => u.Cars).Result;

            Route route = routeRepo.FindAllAsync().Result.First();

            bool result = routeRepo.DeleteAsync(route.Id).Result;
            connection.Dispose();
        }
    }
}
