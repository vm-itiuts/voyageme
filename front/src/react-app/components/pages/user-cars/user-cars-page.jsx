import './user-cars-page.less';

import BaseComponent from 'components/base-component';
import Panel from 'components/common/panel/panel';
import EditCarBlock from './edit-car-block/edit-car-block';
import CarListBlock from './car-list-block/car-list-block';

import storeFactory from 'factories/store-factory';

class UserCarsPage extends BaseComponent {
  constructor(props) {
    super(props);

    this.onCarStoreUpdate = this.onCarStoreUpdate.bind(this);
  }

  componentDidMount() {
    storeFactory.car.addChangeListener(this.onCarStoreUpdate);

    this.props.getCars();
  }

  componentWillUnmount() {
    storeFactory.car.removeChangeListener(this.onCarStoreUpdate);
  }

  onCarStoreUpdate() {
    this.setState({
      cars: storeFactory.car.getCars(),
    });
  }

  render() {
    return (
      <div className="container page-wrapper user-cars-page">
        <div className="col-md-offset-2 col-md-8">
          <h1 className="page-header">Машины</h1>

          <Panel title="Добавить машину">
            <EditCarBlock
              errors={this.props.editCarValidationErrors}
              addCar={this.props.addCar}
            />
          </Panel>

          <h2 className="page-header">Список моих машин</h2>

          <CarListBlock
            cars={this.props.cars}
            errors={this.props.addCarValidationErrors}
            addCar={this.props.addCar}
            editCar={this.props.editCar}
            removeCar={this.props.removeCar}
          />
        </div>
      </div>
    );
  }
}

if (process.env.NODE_ENV === 'development') {
  UserCarsPage.propTypes = {
    cars: React.PropTypes.array.isRequired,
    addCarValidationErrors: React.PropTypes.object.isRequired,
    editCarValidationErrors: React.PropTypes.object.isRequired,
  }
}

export default UserCarsPage;
