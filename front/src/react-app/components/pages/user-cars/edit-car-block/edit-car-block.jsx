import './edit-car-block.less';

import BaseComponent from 'components/base-component';
import InputInline from 'components/common/input/input-inline/input-inline';

const CAR_TYPE = { SEDAN: 1, MINIVAN: 2, BUS: 3, };

class EditCarBlock extends BaseComponent {
  constructor(props) {
    super(props);

    this.selectCarType = this.selectCarType.bind(this);
    this.addCar = this.addCar.bind(this);
    this.editCar = this.editCar.bind(this);
    this.removeCar = this.removeCar.bind(this);
    this.getEditButton = this.getEditButton.bind(this);

    this.state = {
      form: getDefaultForm(),
    };
  }

  componentDidMount() {
    if (this.props.car) {
      this.setState({ form: this.props.car });
    }
  }

  selectCarType(target) {
    const form = { ...this.state.form };
    form.typeId = parseInt(target.currentTarget.value, 10);
    this.setState({ form });
  }

  addCar() {
    this.props.addCar(this.state.form)
      .then(() => {
        // if success, then clear form fields
        this.setState({ form: getDefaultForm() });
      });
  }

  editCar() {
    this.props.editCar(this.props.car.id, this.state.form);
  }

  removeCar() {
    this.props.removeCar(this.props.car.id);
  }

  getEditButton() {
    if (this.props.mode === 'edit') {
      return <button className="btn default-inverse" onClick={this.editCar}>Сохранить</button>;
    }

    return <button className="btn default-inverse" onClick={this.addCar}>Добавить</button>;
  }

  // TODO: add car photos
  // TODO: reset fields if panel was closed without click edit button
  render() {
    // unique name for radio buttons. We need it for multiple using on one page. Otherwise radio buttons won't work correct.
    const name = 'car-type-' + (new Date()).getTime();

    return (
      <div>
        <div className="col-xs-6 col-md-6">
          <InputInline
            label="Марка"
            value={this.state.form.model}
            errorMessage={this.props.errors ? this.props.errors.model : []}
            onChange={this.onFormValueChange('model')}
          />
        </div>

        <div className="col-xs-5 col-md-5">
          <InputInline
            label="Регистрационный номер"
            value={this.state.form.number}
            errorMessage={this.props.errors ? this.props.errors.number : []}
            onChange={this.onFormValueChange('number')}
          />
        </div>

        <div className="row">
          <div className="col-xs-12 col-md-12">
            <input
              name={name}
              type="radio"
              value={CAR_TYPE.SEDAN}
              checked={this.state.form.typeId === CAR_TYPE.SEDAN}
              onChange={this.selectCarType}
            /> Седан
            <input
              name={name}
              type="radio"
              value={CAR_TYPE.MINIVAN}
              checked={this.state.form.typeId === CAR_TYPE.MINIVAN}
              onChange={this.selectCarType}
            /> Минивэн
            <input
              name={name}
              type="radio"
              value={CAR_TYPE.BUS}
              checked={this.state.form.typeId === CAR_TYPE.BUS}
              onChange={this.selectCarType}
            /> Автобус
          </div>
        </div>

        <div className="row">
          <div className="col-xs-6 col-md-6">
            <InputInline
              label="Год выпуска"
              value={this.state.form.year}
              errorMessage={this.props.errors ? this.props.errors.year : []}
              onChange={this.onFormValueChange('year')}
            />
          </div>
        </div>

        <div className="btn-group right">
          {this.props.mode === 'edit' ?
            <button className="btn default" onClick={this.removeCar}>Удалить</button> : null
          }

          {this.getEditButton()}
        </div>
      </div>
    );
  }
}

function getDefaultForm() {
  return {
    model: '',
    number: '',
    typeId: CAR_TYPE.SEDAN,
    year: '',
    images: [],
  };
}

if (process.env.NODE_ENV === 'development') {
  EditCarBlock.propTypes = {
    mode: React.PropTypes.string,
    errors: React.PropTypes.object,
  };
}

export default EditCarBlock;
