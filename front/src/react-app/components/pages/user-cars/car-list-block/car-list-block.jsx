import './car-list-block.less';

import Collapse from 'rc-collapse';
import EditCarBlock from '../edit-car-block/edit-car-block';

class CarListBlock extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    if (!this.props.cars || !this.props.cars.length) {
      return <p>У вас пока нет машин</p>;
    }

    return (
      <Collapse accordion={true} className="car-list">
        {this.props.cars.map((car) => {
          const header = car.model + ' ' + car.number;

          return (
            <Collapse.Panel header={header} key={car.id}>
              <EditCarBlock
                mode="edit" car={car} errors={this.props.errors}
                addCar={this.props.addCar}
                editCar={this.props.editCar}
                removeCar={this.props.removeCar}
              />
            </Collapse.Panel>
          );
        })}
      </Collapse>
    );
  }
}

CarListBlock.propTypes = {
  cars: React.PropTypes.array.isRequired,
  errors: React.PropTypes.object,
};

export default CarListBlock;