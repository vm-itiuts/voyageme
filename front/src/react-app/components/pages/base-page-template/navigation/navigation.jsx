import './navigation.less';

import Link from 'react-router/lib/Link';

export default (props) => {
  return (
    <ul className="navigation">
      <li><Link to="/about" activeClassName="active" onClick={props.onSelectCb}>О нас</Link></li>
      <li><Link to="/help" activeClassName="active" onClick={props.onSelectCb}>Помощь</Link></li>
      <li><Link to="/contacts" activeClassName="active" onClick={props.onSelectCb}>Контакты</Link></li>
      <li><Link to="/offers" activeClassName="active" onClick={props.onSelectCb}>Предложения</Link></li>
    </ul>
  );
};
