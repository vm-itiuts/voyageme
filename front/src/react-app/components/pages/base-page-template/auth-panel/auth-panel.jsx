import './auth-panel.less';

import UserProfileMenu from './user-profile-menu/user-profile-menu';

const AuthPanel = (props) => {
  return (
    <div id={props.id} className="auth-panel">
      {props.isAuthenticated ?
        <UserProfileMenu username={props.userData.firstName} photoLink="" logOut={props.logOut} /> :
        <button className="btn login-btn" onClick={() => props.openPopup('auth')}>Вход</button>
      }
    </div>
  );
};

if (process.env.NODE_ENV === 'development') {
  AuthPanel.propTypes = {
    id: React.PropTypes.string,
    isAuthenticated: React.PropTypes.bool.isRequired,
    userData: React.PropTypes.object.isRequired,
    openPopup: React.PropTypes.func.isRequired,
    logOut: React.PropTypes.func.isRequired,
  };
}

export default AuthPanel;

