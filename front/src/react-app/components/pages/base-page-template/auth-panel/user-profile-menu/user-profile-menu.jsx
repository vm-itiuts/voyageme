import './user-profile-menu.less';
import 'react-simple-dropdown/styles/Dropdown.css';
import NoAvatarImg from 'img/no-avatar.png';

import Link from 'react-router/lib/Link';
import Dropdown, { DropdownTrigger, DropdownContent } from 'react-simple-dropdown';

class UserProfileMenu extends React.Component {
  constructor(props) {
    super(props);

    this.close = this.close.bind(this);
  }

  close() {
    this.refs.dropdown.hide();
  }

  render() {
    return (
      <Dropdown className="user-profile-menu" ref="dropdown">
        <DropdownTrigger>
          <span>{this.props.username}</span>

          <div className="user-avatar-wrapper">
            {this.props.photoLink ?
              <img src={this.props.photoLink} alt="" /> :
              <img src={NoAvatarImg} />
            }
          </div>
        </DropdownTrigger>
        <DropdownContent>
          <ul>
            <li><Link to="profile" onClick={this.close}>Мой профиль</Link></li>
            <li><Link to="settings" onClick={this.close}>Настройки</Link></li>
            <li><span className="text-link" onClick={this.props.logOut}>Выйти</span></li>
          </ul>
        </DropdownContent>
      </Dropdown>
    );
  }
}

if (process.env.NODE_ENV === 'development') {
  UserProfileMenu.propTypes = {
    logOut: React.PropTypes.func.isRequired,
    username: React.PropTypes.string.isRequired,
    photoLink: React.PropTypes.string.isRequired,
  };
}

export default UserProfileMenu;