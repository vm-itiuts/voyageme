import './notifications.less';

import NotificationSystem from 'react-notification-system';

class Notifications extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      notifications: [],
    };
  }

  componentWillReceiveProps(nextProps) {
    const comingNotifications = nextProps.notifications;

    if (this.state.notifications === comingNotifications) {
      return;
    }

    this.setState({ notifications: comingNotifications }, function () {
      this.state.notifications.forEach(function (notification) {
        this.refs.notificationSystem.addNotification(notification);
      }, this);
    });

  }

  // TODO: change styles of notifications
  render() {
    return (
      <NotificationSystem ref="notificationSystem" />
    );
  }
}

if (process.env.NODE_ENV === 'development') {
  Notifications.propTypes = {
    notifications: React.PropTypes.array.isRequired,
  };
}

export default Notifications;
