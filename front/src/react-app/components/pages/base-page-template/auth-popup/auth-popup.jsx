import './auth-popup.less';

import vkIcon from 'img/social-web-icons/vk.svg';
import googleIcon from 'img/social-web-icons/google.svg';
import facebook from 'img/social-web-icons/facebook.svg';

import Link from 'react-router/lib/Link';
import BaseComponent from 'components/base-component';
import InputInline from 'components/common/input/input-inline/input-inline';
import InlineSVG from 'svg-inline-react';

class AuthPopup extends BaseComponent {
  constructor(props) {
    super(props);

    this.auth = this.auth.bind(this);
    this.closeAuthPopup = this.closeAuthPopup.bind(this);

    this.state = {
      form: {
        email: '',
        password: ''
      },
    };
  }

  auth() {
    this.props.auth(this.state.form.email, this.state.form.password);
  }

  closeAuthPopup() {
    this.props.closePopup('auth');
  }

  render() {
    return (
      <div className="row">
        <div className="col-xs-12 col-md-12">
          <div className="header">Авторизация</div>

          <InputInline
            label="Email"
            value={this.state.form.email}
            onChange={this.onFormValueChange('email')}
          />

          <InputInline
            label="Пароль"
            value={this.state.form.password}
            type="password"
            onChange={this.onFormValueChange('password')}
          />

          <p className="no-account-block">
            Еще нет аккаунта? <Link to="/sign-up" className="text-btn" onClick={this.closeAuthPopup}>Регистрация</Link>
          </p>

          <div className="social-webs">
            <InlineSVG src={vkIcon} className="vk" />
            <InlineSVG src={googleIcon} className="google" />
            <InlineSVG src={facebook} className="facebook" />
          </div>

          <div className="btn-group right">
            <button className="btn default-inverse" onClick={this.auth}>Вход</button>
            <button className="btn default" onClick={this.closeAuthPopup}>Отмена</button>
          </div>
        </div>
      </div>
    );
  }
}

if (process.env.NODE_ENV === 'development') {
  AuthPopup.propTypes = {
    auth: React.PropTypes.func.isRequired,
    closePopup: React.PropTypes.func.isRequired,
  };
}

export default AuthPopup;
