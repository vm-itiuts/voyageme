import BasePopup from 'components/common/base-popup/base-popup';

import Header from './header/header';
import AuthPanel from './auth-panel/auth-panel';
import Backdrop from './backdrop/backdrop';
import Sidebar from './sidebar/sidebar';
import AuthPopup from './auth-popup/auth-popup';
import Notifications from './notifications/notifications';

import googleMapsLoaderService from 'services/google-maps-loader-service';

class BasePageTemplate extends React.Component {
  constructor(props) {
    super(props);

    googleMapsLoaderService.load(this.props.googleMapsLoadedSuccess);
    this.props.refreshToken();
  }

  render() {
    return (
      <div>
        <Header openSidebar={this.props.openSidebar}>
          <AuthPanel
            id="menu-auth-panel"
            isAuthenticated={this.props.authState.isAuthenticated}
            userData={this.props.authState.userData}
            openPopup={this.props.openPopup}
            logOut={this.props.logOut}
          />
        </Header>

        {this.props.children}

        <Sidebar isOpened={this.props.appState.isSidebarOpened} closeSidebar={this.props.closeSidebar} />

        <Backdrop
          show={this.props.appState.isBackdropShown}
          closeSidebar={this.props.closeSidebar}
          closeAllPopups={this.props.closeAllPopups}
        />

        <BasePopup isOpened={this.props.appState.popupState.auth} className="auth-popup">
          <AuthPopup closePopup={this.props.closePopup} auth={this.props.auth}/>
        </BasePopup>

        <Notifications notifications={this.props.notificationState.notifications}/>
      </div>
    );
  }
}

if (process.env.NODE_ENV === 'development') {
  BasePageTemplate.propTypes = {
    auth: React.PropTypes.func.isRequired,
    refreshToken: React.PropTypes.func.isRequired,
    logOut: React.PropTypes.func.isRequired,
    appState: React.PropTypes.object.isRequired,
    authState: React.PropTypes.object.isRequired,
    notificationState: React.PropTypes.object.isRequired,
  };
}

export default BasePageTemplate
