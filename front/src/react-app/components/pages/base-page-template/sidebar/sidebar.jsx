import './sidebar.less';
import closeIcon from 'img/ic_close_white_48px.svg';

import classnames from 'classnames';

import Navigation from 'components/pages/base-page-template/navigation/navigation';
import InlineSVG from 'svg-inline-react';

const Sidebar = (props) => {
  var classNames = classnames('sidebar-menu', { 'open-state': props.isOpened });

  return (
    <div className={classNames}>
      <InlineSVG src={closeIcon} className="close-btn" onClick={props.closeSidebar} />

      <Navigation onSelectCb={props.closeSidebar} />
    </div>
  );
};

if (process.env.NODE_ENV === 'development') {
  Sidebar.propTypes = {
    isOpened: React.PropTypes.bool.isRequired,
    closeSidebar: React.PropTypes.func.isRequired,
  };
}

export default Sidebar;
