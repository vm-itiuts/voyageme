import './backdrop.less';

import classnames from 'classnames';

const Backdrop = (props) => {
  var closeEverything = function () {
    props.closeSidebar();
    props.closeAllPopups();
  };

  return (
    <div 
      className={classnames('backdrop', { shows: props.show })} 
      onClick={closeEverything}></div>
  );
};

if (process.env.NODE_ENV === 'development') {
  Backdrop.propTypes = {
    show: React.PropTypes.bool.isRequired,
    closeSidebar: React.PropTypes.func.isRequired,
    closeAllPopups: React.PropTypes.func.isRequired,
  };
}

export default Backdrop;
