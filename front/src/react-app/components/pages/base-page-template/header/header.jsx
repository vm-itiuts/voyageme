import './header.less';
import menuIcon from 'img/ic_menu_black_48px.svg';
import logoIcon from 'img/logo.svg';

import InlineSVG from 'svg-inline-react';
import Navigation from '../navigation/navigation';

import history from 'history';

const Header = (props) => {
  function goToHomePage() {
    history.push('/');
  }

  return (
    <div>
      <header>
        <div className="container-fluid">
          <div className="row">
            <div className="header-menu">
              <div className="col-xs-6 col-md-6">
                <InlineSVG src={menuIcon} className="menu-btn" onClick={props.openSidebar} />

                <Navigation />
              </div>

              <div className="col-xs-3 col-xs-offset-3 col-md-3 col-md-offset-3">
                {props.children}
              </div>
            </div>
          </div>
        </div>

        <div className="line"></div>

        <div className="outer-wrapper" onClick={goToHomePage}>
          <div className="inner-wrapper">
            <InlineSVG src={logoIcon} />
          </div>
        </div>
      </header>
    </div>
  )
};

if (process.env.NODE_ENV === 'development') {
  Header.propTypes = {
    openSidebar: React.PropTypes.func.isRequired,
  };
}

export default Header;
