import './create-route-page.less';

import shortid from 'shortid';
import BaseComponent from 'components/base-component';
import GoogleMapsComponent from 'components/common/google-maps-component/google-maps-component';
import AddWaypoints from './add-waypoints/add-waypoints';
import InputInline from 'components/common/input/input-inline/input-inline';
import InputDatepicker from 'components/common/input/input-datepicker/input-datepicker';
import InputSelect from 'components/common/input/input-select/input-select';
import InputInlineMapsAutocomplete from 'components/common/input/input-maps-autocomplete/input-inline-maps-autocomplete';

import storeFactory from 'factories/store-factory';

import BOOK_TYPES from 'constants/book-types';

class CreateRoutePage extends BaseComponent {
  constructor(props) {
    super(props);

    this.state = {
      form: {
        startPlace: { placeId: null },
        endPlace: { placeId: null },
        startDateTime: null,
        bookType: BOOK_TYPES.MANUAL,
        price: '',
        code: '',
        seatsNumber: '',
        waypoints: [],
        transport: {},
      },

      confirmationCodeBlockStyle: { display: 'none' },
    };

    this.onCarStoreUpdate = this.onCarStoreUpdate.bind(this);
    this.addEmptyWaypoint = this.addEmptyWaypoint.bind(this);
    this.setWaypoint = this.setWaypoint.bind(this);
    this.removeWaypoint = this.removeWaypoint.bind(this);
    this.getWaypoints = this.getWaypoints.bind(this);
    this.onBookTypeChange = this.onBookTypeChange.bind(this);
    this.setPlace = this.setPlace.bind(this);
    this.onTransportChange = this.onTransportChange.bind(this);
    this.createRoute = this.createRoute.bind(this);
  }

  componentDidMount() {
    storeFactory.car.addChangeListener(this.onCarStoreUpdate);

    this.props.getCars().then(() => {
      if (this.props.transports.length > 0) {
        this.setState({
          form: {
            ...this.state.form,
            transport: this.props.transports[0]
          }
        });
      }
    });
  }

  componentWillUnmount() {
    storeFactory.car.removeChangeListener(this.onCarStoreUpdate);
  }

  onCarStoreUpdate() {
    const transports = storeFactory.car.getCars();

    if (transports) {
      this.setState({ transports });

      if (transports.length > 0) {
        this.setState({
          form: {
            ...this.state.form,
            transport: transports[0]
          }
        });
      }
    }
  }

  onBookTypeChange(selected) {
    const form = { ...this.state.form };
    let confirmationCodeBlockStyle = null;

    if (selected.value === BOOK_TYPES.CODE.value) {
      confirmationCodeBlockStyle = { display: 'block' };
    } else {
      confirmationCodeBlockStyle = { display: 'none' };
      form.code = '';
    }

    form.bookType = selected;

    this.setState({ form, confirmationCodeBlockStyle });
  }

  onTransportChange(selected) {
    const transport = this.props.transports.find(x => x.id == selected.value);
    const form = {
      ...this.state.form,
      transport
    };

    this.setState({ form });
  }

  setPlace(propName) {
    return (value) => {
      const form = {
        ...this.state.form,
        [propName]: { ...value, placeId: value.place_id }
      };

      this.setState({ form });
    }
  }

  addEmptyWaypoint() {
    const waypoints = [...this.state.form.waypoints];
    waypoints.push({ id: shortid.generate(), location: {} });

    this.setState({
      form: {
        ...this.state.form, waypoints
      }
    });
  }

  removeWaypoint(waypointIndex) {
    const waypoints = [...this.state.form.waypoints];
    waypoints.splice(waypointIndex, 1);

    this.setState({ form: { ...this.state.form, waypoints } });
  }

  getWaypoints() {
    return this.state.form.waypoints
      .filter((waypoint) => waypoint.location.placeId)
      .map((waypoint) => {
        return { location: { placeId: waypoint.location.placeId } };
      });
  }

  setWaypoint(waypoint, location) {
    const index = this.state.form.waypoints.findIndex((i) => i === waypoint);

    if (index === -1) {
      console.error('setWaypoint() unexpected error', arguments);
      return;
    }

    const waypoints = [...this.state.form.waypoints];
    waypoints[index].location = { ...location, placeId: location.place_id };

    this.setState({
      form: { ...this.state.form, waypoints }
    })
  }

  createRoute() {
    const form = {
      ...this.state.form,
      startPlaceId: this.state.form.startPlace.placeId,
      formattedStartPlace: this.state.form.startPlace.formatted_address,
      endPlaceId: this.state.form.endPlace.placeId,
      formattedEndPlace: this.state.form.endPlace.formatted_address,
      waypoints: this.state.form.waypoints
        .filter((waypoint) => waypoint.location.placeId)
        .map((waypoint, index) => ({
          placeId: waypoint.location.placeId,
          formattedPlace: waypoint.location.formatted_address,
          orderNumber: index
        })),
      bookTypeId: this.state.form.bookType.value,
      transportId: this.state.form.transport.id,
    };
    if (form.startDateTime) {
      form.startDateTime = form.startDateTime.format();
    }
    this.props.createRoute(form);
  }

  render() {
    const validationErrors = this.props.createRouteValidationErrors;

    return (
      <div className="container page-wrapper create-route-page">
        <div className="col-lg-offset-2 col-lg-8">
          <h1 className="page-header">Создание маршрута</h1>

          <div className="row">
            <div className="col-xs-6 col-md-6">
              <InputInlineMapsAutocomplete
                label="Откуда"
                errorMessage={validationErrors.startPlace}
                onPlaceSelected={this.setPlace('startPlace')}
                googleMapsLoaded={this.props.googleMapsLoaded}
              />

              <InputInlineMapsAutocomplete
                label="Куда"
                errorMessage={validationErrors.endPlace}
                onPlaceSelected={this.setPlace('endPlace')}
                googleMapsLoaded={this.props.googleMapsLoaded}
              />

              <AddWaypoints
                waypoints={this.state.form.waypoints}
                addEmptyWaypoint={this.addEmptyWaypoint}
                removeWaypoint={this.removeWaypoint}
                setWaypoint={this.setWaypoint}
                googleMapsLoaded={this.props.googleMapsLoaded}
              />
            </div>

            <div className="col-xs-6 col-md-6">
              <GoogleMapsComponent
                ref="googleMap"
                origin={{placeId: this.state.form.startPlace.placeId}}
                destination={{placeId: this.state.form.endPlace.placeId}}
                waypoints={this.getWaypoints()}
                googleMapsLoaded={this.props.googleMapsLoaded}
              />
            </div>
          </div>

          <InputDatepicker
            label="Дата поездки"
            value={this.state.form.startDateTime}
            errorMessage={validationErrors.startDateTime}
            onChange={this.onFormValueChange('startDateTime')}
            readOnly={true}
          />

          <div className="row">
            <div className="col-xs-6 col-md-6">
              <InputSelect
                label="Тип подтверждения"
                options={[BOOK_TYPES.MANUAL, BOOK_TYPES.AUTOMATIC, BOOK_TYPES.CODE]}
                value={ this.state.form.bookType }
                errorMessage={validationErrors.bookTypeId}
                onChange={this.onBookTypeChange}
              />
            </div>

            <div className="col-xs-6 col-md-6" style={this.state.confirmationCodeBlockStyle}>
              <InputInline
                label="Код подтверждения"
                maxLength={3}
                type="number"
                value={this.state.form.code}
                errorMessage={validationErrors.code}
                onChange={this.onFormValueChange('code')}
              />
            </div>
          </div>

          <div className="row">
            <div className="col-xs-6 col-md-6">
              <InputInline
                label="Число мест"
                type="number"
                maxLength={2}
                value={this.state.form.seatsNumber}
                errorMessage={validationErrors.seatsNumber}
                onChange={this.onFormValueChange('seatsNumber')}
              />
            </div>

            <div className="col-xs-6 col-md-6">
              <InputInline
                label="Цена"
                type="number"
                value={this.state.form.price}
                errorMessage={validationErrors.price}
                onChange={this.onFormValueChange('price')}
              />
            </div>
          </div>

          <div className="row">
            <div className="col-xs-6 col-md-6">
              <InputSelect
                label="Машина"
                options={this.props.transports.map(transport => ({value: transport.id, label: transport.model}))}
                value={ { value: this.state.form.transport.id, label: this.state.form.transport.model } }
                errorMessage={validationErrors.bookTypeId}
                onChange={this.onTransportChange}
              />
            </div>
          </div>

          <div className="btn-group right">
            <button className="btn default-inverse" onClick={this.createRoute}>Создать</button>
          </div>
        </div>
      </div>
    );
  }
}

if (process.env.NODE_ENV === 'development') {
  CreateRoutePage.propTypes = {
    googleMapsLoaded: React.PropTypes.bool.isRequired,
    transports: React.PropTypes.array.isRequired,
    createRouteValidationErrors: React.PropTypes.object.isRequired,
  }
}

export default CreateRoutePage;
