import './add-waypoints.less';

import closeIcon from 'img/ic_close_white_48px.svg';

import InlineSVG from 'svg-inline-react';
import InputInlineMapsAutocomplete from 'components/common/input/input-maps-autocomplete/input-inline-maps-autocomplete';

const AddWaypoints = (props) => {
  return (
    <div className="add-waypoints-block">
      <h3 className="text-center">Промежуточные точки</h3>

      {inputsForWaypoints(props)}

      <div className="text-center">
        <button className="btn default" onClick={props.addEmptyWaypoint}>Добавить</button>
      </div>
    </div>
  );
};

function inputsForWaypoints(props) {
  return props.waypoints.map(function (waypoint, index) {
    return (
      <div className="row waypoint" key={waypoint.id}>
        <div className="col-xs-10 col-md-10">
          <InputInlineMapsAutocomplete
            label={'Точка ' + (index + 1)}
            onPlaceSelected={function (location) {
              props.setWaypoint(waypoint, location);
            }}
            googleMapsLoaded={props.googleMapsLoaded}
          />
        </div>

        <div className="col-xs-2 col-md-2">
          <InlineSVG
            src={closeIcon}
            className="remove-btn"
            onClick={function () {
              props.removeWaypoint(index);
            }}
          />
        </div>
      </div>
    );
  });
}

if (process.env.NODE_ENV === 'development') {
  AddWaypoints.propTypes = {
    googleMapsLoaded: React.PropTypes.bool.isRequired,
    waypoints: React.PropTypes.array.isRequired,
    addEmptyWaypoint: React.PropTypes.func.isRequired,
    removeWaypoint: React.PropTypes.func.isRequired,
    setWaypoint: React.PropTypes.func.isRequired,
  };
}

export default AddWaypoints;