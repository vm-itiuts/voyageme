import './profile-page.less';

import history from 'history';
import BaseComponent from 'components/base-component';

class ProfilePage extends BaseComponent {
  constructor(props) {
    super(props);
  }

  goToMyCarsPage() {
    history.push('/user-cars');
  }

  render() {
    return (
      <button className="btn default-inverse" onClick={this.goToMyCarsPage}>Мои машины</button>
    );
  }
}

export default ProfilePage;
