import BaseComponent from 'components/base-component';
import SiteTitleBlock from './site-title-block/site-title-block';
import history from 'history';

class HomePage extends BaseComponent {
  constructor(props) {
    super(props);

    this.createRouteButtonOnClick = this.createRouteButtonOnClick.bind(this);

    this.state = {
      startPlace: null,
      endPlace: null,
      routeDate: null
    };
  }

  createRouteButtonOnClick() {
    if (this.props.isAuthenticated) {
      history.push('/create-route');
    } else {
      this.props.openPopup('auth');
    }
  }

  setPlace(propName) {
    return (value) => {
      this.setState({ [propName]: { ...value, placeId: value.place_id } });
    }
  }

  render() {
    return (
      <div>
        <SiteTitleBlock
          onStartPlaceChange={this.setPlace('startPlace')}
          onEndPlaceChange={this.setPlace('endPlace')}
          onRouteDateChange={this.onStateValueChange('routeDate')}
          createRouteOnClick={this.createRouteButtonOnClick}
          googleMapsLoaded={this.props.googleMapsLoaded}
        />

        {this.props.children}

        <div className="container">
          <h1>ГЛАВНАЯ СТРАНИЦА</h1>

          <h2>ГЛАВНАЯ СТРАНИЦА</h2>

          <h3>ГЛАВНАЯ СТРАНИЦА</h3>

          <div>
            В некотором царстве, в некотором государстве жил-был царь, у него был сын Иван-царевич — и красивый,
            и умный, и славный; об нем песни пели, об нем сказки сказывали, он красным девушкам во сне снился. Пришло
            ему желанье поглядеть на бел свет; берет он у царя-отца благословенье и позволенье и едет на все четыре
            стороны,
            людей посмотреть, себя показать.
          </div>


          <div>
            В некотором царстве, в некотором государстве жил-был царь, у него был сын Иван-царевич — и красивый,
            и умный, и славный; об нем песни пели, об нем сказки сказывали, он красным девушкам во сне снился. Пришло
            ему желанье поглядеть на бел свет; берет он у царя-отца благословенье и позволенье и едет на все четыре
            стороны,
            людей посмотреть, себя показать.
          </div>

          <div>
            В некотором царстве, в некотором государстве жил-был царь, у него был сын Иван-царевич — и красивый,
            и умный, и славный; об нем песни пели, об нем сказки сказывали, он красным девушкам во сне снился. Пришло
            ему желанье поглядеть на бел свет; берет он у царя-отца благословенье и позволенье и едет на все четыре
            стороны,
            людей посмотреть, себя показать.
          </div>
        </div>
      </div>
    );
  };
}

if (process.env.NODE_ENV === 'development') {
  HomePage.propTypes = {
    openPopup: React.PropTypes.func.isRequired,
    googleMapsLoaded: React.PropTypes.bool.isRequired,
  };
}

export default HomePage;