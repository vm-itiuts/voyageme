import './site-title-block.less';

import InputPlaceholder from 'components/common/input/input-placeholder/input-placeholder';
import InputPlaceholderMapsAutocomplete from 'components/common/input/input-maps-autocomplete/input-placeholder-maps-autocomplete';

const SiteTitleBlock = (props) => {
  return (
    <div id="site-title-block">
      <div className="find-route-form">
        <InputPlaceholderMapsAutocomplete
          placeholder="Откуда"
          onPlaceSelected={props.onStartPlaceChange}
          googleMapsLoaded={props.googleMapsLoaded}
        />

        <InputPlaceholderMapsAutocomplete
          placeholder="Куда"
          onPlaceSelected={props.onEndPlaceChange}
          googleMapsLoaded={props.googleMapsLoaded}
        />

        <InputPlaceholder type="date" onChange={props.onRouteDateChange} />
      </div>

      <div className="create-route">
        <button className="btn default-inverse" onClick={props.createRouteOnClick}>Создать маршрут</button>
      </div>
    </div>
  );
};

if (process.env.NODE_ENV === 'development') {
  SiteTitleBlock.propTypes = {
    onStartPlaceChange: React.PropTypes.func.isRequired,
    onEndPlaceChange: React.PropTypes.func.isRequired,
    onRouteDateChange: React.PropTypes.func.isRequired,
    createRouteOnClick: React.PropTypes.func.isRequired,
    googleMapsLoaded: React.PropTypes.bool.isRequired,
  };
}

export default SiteTitleBlock;
