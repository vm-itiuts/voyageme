import './sign-up-page.less';

import BaseComponent from 'components/base-component';
import InputInline from 'components/common/input/input-inline/input-inline';
import InputSelect from 'components/common/input/input-select/input-select';

import COUNTRY_CODES from 'constants/country-codes-constants';

class SignUpPage extends BaseComponent {
  constructor(props) {
    super(props);

    this.state = {
      form: {
        email: '',
        password: '',
        repeatPassword: '',
        firstName: '',
        secondName: '',
        phoneNumber: '',
        countryPhoneCode: COUNTRY_CODES[0],
      }
    };
  }

  signUp() {
    let form = { ...this.state.form };
    form.countryPhoneCode = COUNTRY_CODES.find((i) => i.label === form.countryPhoneCode.label);

    this.props.signUp(form);

    this.setState({ errorMessages: {} });
  }

  // TODO: disable button until response coming
  render() {
    const validationErrors = this.props.signUpValidationErrors;

    return (
      <div className="container page-wrapper sign-up-page">
        <div className="col-md-offset-2 col-md-8">
          <h1 className="page-header">Регистрация</h1>

          <div className="text-center">
            <InputInline
              label="Email"
              value={this.state.form.email}
              errorMessage={validationErrors.email}
              onChange={this.onFormValueChange('email')}
            />

            <div className="row">
              <div className="col-xs-6 col-md-6">
                <InputInline
                  label="Пароль"
                  type="password"
                  value={this.state.form.password}
                  errorMessage={validationErrors.password}
                  onChange={this.onFormValueChange('password')}
                />
              </div>

              <div className="col-xs-6 col-md-6">
                <InputInline
                  label="Повторите пароль"
                  type="password"
                  value={this.state.form.repeatPassword}
                  errorMessage={validationErrors.password}
                  onChange={this.onFormValueChange('repeatPassword')}
                />
              </div>
            </div>

            <InputInline
              label="Имя"
              value={this.state.form.firstName}
              errorMessage={validationErrors.firstName}
              onChange={this.onFormValueChange('firstName')}
            />

            <InputInline
              label="Фамилия"
              value={this.state.form.secondName}
              errorMessage={validationErrors.secondName}
              onChange={this.onFormValueChange('secondName')}
            />

            <div className="row">
              <div className="col-xs-5 col-md-5">
                <InputSelect
                  label="Телефон"
                  value={this.state.form.countryPhoneCode}
                  options={COUNTRY_CODES}
                  onChange={this.onFormValueChange('countryPhoneCode')}
                />
              </div>

              <div className="col-xs-7 col-md-7">
                <InputInline
                  label="Телефон"
                  value={this.state.form.phoneNumber}
                  errorMessage={validationErrors.phoneNumber}
                  onChange={this.onFormValueChange('phoneNumber')}
                />
              </div>
            </div>
          </div>

          <div className="btn-group right">
            <button className="btn default-inverse" onClick={this.signUp.bind(this)}>Регистрация</button>
          </div>
        </div>
      </div>
    );
  }
}

if (process.env.NODE_ENV === 'development') {
  SignUpPage.propTypes = {
    signUpValidationErrors: React.PropTypes.object.isRequired,
    signUp: React.PropTypes.func.isRequired,
  };
}

export default SignUpPage;
