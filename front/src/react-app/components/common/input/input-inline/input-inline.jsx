import './input-inline.less';

import InputInlineView from 'components/common/input/input-inline/input-inline-view';
import ErrorLabel from 'components/common/input/error-label/error-label';
import BaseComponent from 'components/base-component';
import baseInput from '../base-input';
import debugHelper from 'helpers/debug-helper';

class InputInline extends BaseComponent {
  constructor(props) {
    super(props);
  }

  onChange(event) {
    const newValue = event.target.value;

    if (this.props.maxLength && newValue.length > this.props.maxLength) {
      return;
    }

    this.props.onChange(newValue);
  }

  render() {
    const inputClass = baseInput.getInputClass(this.props);
    const componentClass = baseInput.getComponentClass(this.props, 'input-inline');

    return (
      <div className={componentClass}>
        <InputInlineView label={this.props.label} value={this.props.value} />

        <div className="input-wrapper">
          <input
            onChange={this.onChange.bind(this)}
            value={this.props.value}
            type={this.props.type}
            className={inputClass}
          />
          <span className="underline"></span>
        </div>

        <ErrorLabel errorMessage={this.props.errorMessage} />
      </div>
    );
  }
}

if (process.env.NODE_ENV === 'development') {
  InputInline.propTypes = {
    onChange: React.PropTypes.func,
    value: React.PropTypes.oneOfType([
      React.PropTypes.string,
      React.PropTypes.number,
    ]),
    type: React.PropTypes.string,
    label: React.PropTypes.string,
    inputClass: React.PropTypes.string,
    required: React.PropTypes.bool,
    errorMessage: function (props, propName, componentName) {
      if (props.errorMessage !== undefined && !Array.isArray(props.errorMessage)) {
        return new Error(debugHelper.getPropTypeValidationError(propName, componentName));
      }
    },
    maxLength: React.PropTypes.number
  };
}

export default InputInline;
