import classnames from 'classnames';

function InputInlineView(props) {
  const className = classnames({ shifted: props.value });
  return (
    <label className={className}>{props.label}</label>
  );
}

if (process.env.NODE_ENV == 'development') {
  InputInlineView.propTypes = {
    label: React.PropTypes.string.isRequired,
  }
}

export default InputInlineView;
