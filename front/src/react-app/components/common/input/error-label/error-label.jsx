function ErrorLabel(props) {
  const errorMessage = props.errorMessage ? props.errorMessage[0] : null;

  return (
    <div className="error">{errorMessage}</div>
  )
}

export default ErrorLabel;