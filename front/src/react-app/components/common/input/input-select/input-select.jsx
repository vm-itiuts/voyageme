import './input-select.less';
import 'react-dropdown/style.css';

import Dropdown from 'react-dropdown';
import BaseComponent from 'components/base-component';
import InputInlineView from '../input-inline/input-inline-view';
import baseInput from '../base-input';
import ErrorLabel from 'components/common/input/error-label/error-label';

class InputSelect extends BaseComponent {
  constructor(props) {
    super(props);

    this.onChange = this.onChange.bind(this);
  }

  onChange(selected) {
    this.props.onChange({ value: selected.value, label: selected.label });
  }

  render() {
    const inputClass = baseInput.getInputClass(this.props);
    const componentClass = baseInput.getComponentClass(this.props, 'input-select');

    return (
      <div className={componentClass}>
        <div className="input-wrapper">
          <Dropdown
            value={this.props.value}
            options={this.props.options}
            onChange={this.onChange}
            className={inputClass}
          />
        </div>

        <ErrorLabel errorMessage={this.props.errorMessage} />
      </div>
    );
  }
}

if (process.env.NODE_ENV === 'development') {
  InputSelect.propTypes = {
    onChange: React.PropTypes.func,
    options: React.PropTypes.array.isRequired,
    inputClass: React.PropTypes.string,
    required: React.PropTypes.bool,
    value: React.PropTypes.object,
  };
}

export default InputSelect;
