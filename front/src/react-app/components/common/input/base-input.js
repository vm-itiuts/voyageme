import _ from 'helpers/local-lodash';
import classnames from 'classnames';
import debugHelper from 'helpers/debug-helper';

const baseInput = {
  /**
   * @param {Object} props - props of React Component
   * @param {string} props.inputClass
   * @param {string} [baseClass]
   */
  getInputClass(props, baseClass) {
    return classnames('input', props.inputClass || baseClass);
  },

  /**
   * @param {Object} props - props of React Component
   * @param {boolean} [props.required]
   * @param {string} [props.className]
   * @param {string} [baseClass]
   */
  getComponentClass(props, baseClass) {
    return classnames('input-component', baseClass, props.className, {
      default: !props.className,
      required: props.required
    });
  },

  validateValueProp: function (props, propName, componentName) {
    const prop = props[propName];

    if (!_.isObject(prop.obj) || typeof prop.prop !== 'string') {
      return new Error(debugHelper.getPropTypeValidationError(propName, componentName));
    }
  },
};

export default baseInput;
