import BaseInputMapsAutocomplete from './base-input-maps-autocomplete';
import baseInput from '../base-input';

class InputPlaceholderMapsAutocomplete extends BaseInputMapsAutocomplete {
  constructor(props) {
    super(props);
  }

  componentDidUpdate() {
    this.initGoogleMapsIfLoaded();
  }

  render() {
    const inputClass = baseInput.getInputClass(this.props, 'input-placeholder');
    const componentClass = baseInput.getComponentClass(this.props);

    return (
      <div className={componentClass}>
        <input
          ref="input"
          className={inputClass}
          placeholder={this.props.placeholder}
        />
      </div>
    );
  }
}

export default InputPlaceholderMapsAutocomplete;
