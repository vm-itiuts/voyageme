import BaseInputMapsAutocomplete from './base-input-maps-autocomplete';
import InputInlineView from 'components/common/input/input-inline/input-inline-view';
import baseInput from 'components/common/input/base-input';
import ErrorLabel from 'components/common/input/error-label/error-label';

class InputInlineMapsAutocomplete extends BaseInputMapsAutocomplete {
  constructor(props) {
    super(props);

    this.state.value = '';

    this.onChange = this.onChange.bind(this);
  };

  onChange(event) {
    this.setState({ value: event.target.value });
  }

  componentDidUpdate() {
    this.initGoogleMapsIfLoaded();
  }

  render() {
    const inputClass = baseInput.getInputClass(this.props);
    const componentClass = baseInput.getComponentClass(this.props, 'input-inline');

    return (
      <div className={componentClass}>
        <InputInlineView label={this.props.label} value={this.state.value} />

        <div className="input-wrapper">
          <input
            ref="input"
            onChange={this.onChange}
            className={inputClass}
            placeholder=""
          />
          <span className="underline"></span>
        </div>

        <ErrorLabel errorMessage={this.props.errorMessage} />
      </div>
    );
  }
}

if (process.env.NODE_ENV === 'development') {
  InputInlineMapsAutocomplete.propTypes = {
    inputClass: React.PropTypes.string,
    label: React.PropTypes.string
  };
}

export default InputInlineMapsAutocomplete;
