import BaseComponent from 'components/base-component';

class BaseInputMapsAutocomplete extends BaseComponent {
  constructor(props) {
    super(props);

    this.initGoogleMapsIfLoaded = this.initGoogleMapsIfLoaded.bind(this);
    this.onSelected = this.onSelected.bind(this);

    this.state = {
      autocomplete: null,
    };
  }

  componentWillUnmount() {
    // if google object exists and autocomplete was initialized - then clearListeners
    if (window.google && this.state.autocomplete) {
      google.maps.event.clearListeners(this.state.autocomplete, 'place_changed');
    }
  }

  initGoogleMapsIfLoaded() {
    const googleMapsLoaded = this.props.googleMapsLoaded;

    if (googleMapsLoaded && !this.state.autocomplete) {
      this.state.autocomplete = new google.maps.places.Autocomplete(this.refs.input, { types: ['(cities)'] });
      this.state.autocomplete.addListener('place_changed', this.onSelected.bind(this));
    }
  }

  onSelected() {
    if (this.props.onPlaceSelected) {
      this.props.onPlaceSelected(this.state.autocomplete.getPlace());
    }
  }
}

if (process.env.NODE_ENV === 'development') {
  BaseInputMapsAutocomplete.propTypes = {
    onPlaceSelected: React.PropTypes.func.isRequired,
    googleMapsLoaded: React.PropTypes.bool.isRequired,
  };
}

export default BaseInputMapsAutocomplete;
