import 'react-datepicker/dist/react-datepicker.min.css';
import './input-datepicker.less';

import moment from 'moment';
import BaseComponent from 'components/base-component';
import Datepicker from 'react-datepicker';
import baseInput from '../base-input';
import InputInlineView from '../input-inline/input-inline-view';
import ErrorLabel from 'components/common/input/error-label/error-label';

class InputDatepicker extends BaseComponent {
  constructor(props) {
    super(props);
  }

  render() {
    const inputClass = baseInput.getInputClass(this.props);
    const componentClass = baseInput.getComponentClass(this.props, 'input-inline');

    return (
      <div className={componentClass}>
        <InputInlineView label={this.props.label} value={this.props.value} />

        <div className="input-wrapper">
          <Datepicker
            className={inputClass}
            onChange={this.props.onChange}
            selected={this.props.value}
            showYearDropdown={true}
            dateFormat="DD/MM/YYYY"
            readOnly={this.props.readOnly}
            isClearable={true}
          />
          <span className="underline"></span>
        </div>

        <ErrorLabel errorMessage={this.props.errorMessage} />
      </div>
    );
  }
}

if (process.env.NODE_ENV === 'development') {
  InputDatepicker.propTypes = {
    onChange: React.PropTypes.func.isRequired,
    label: React.PropTypes.string,
    inputClass: React.PropTypes.string,
    required: React.PropTypes.bool,
    value: React.PropTypes.object,
    readOnly: React.PropTypes.bool,
  };
}

export default InputDatepicker;
