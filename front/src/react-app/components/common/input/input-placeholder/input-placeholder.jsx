import baseInput from '../base-input';

function InputPlaceholder(props) {
  const inputClass = baseInput.getInputClass(props, 'input-placeholder');
  const componentClass = baseInput.getComponentClass(props);

  return (
    <div className={componentClass}>
      <input
        onChange={(event) => props.onChange(event.target.value)}
        placeholder={props.placeholder}
        className={inputClass}
        type={props.type}
      />
    </div>
  );
}

if (process.env.NODE_ENV === 'development') {
  InputPlaceholder.propTypes = {
    onChange: React.PropTypes.func.isRequired,
    type: React.PropTypes.string,
    placeholder: React.PropTypes.string,
    inputClass: React.PropTypes.string,
    required: React.PropTypes.bool
  };
}

export default InputPlaceholder;
