import './google-maps-component.less';

import Map from './map/map';

// coordinates of Minsk
const DEFAULT_CENTER = { lat: 53.904302, lng: 27.559610 };

let directionsService;

class GoogleMapsComponent extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      origin: null,
      destination: null,
      directions: null,

      googleMapsLoaded: false,
    };

    this.updateRoute = this.updateRoute.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    if (!this.state.googleMapsLoaded && nextProps.googleMapsLoaded) {
      directionsService = new google.maps.DirectionsService();

      this.setState({ googleMapsLoaded: nextProps.googleMapsLoaded });
    }

    else if (nextProps.origin.placeId && nextProps.destination.placeId) {
      this.updateRoute(nextProps.origin, nextProps.destination, nextProps.waypoints);
    }

    else {
      this.setState({ directions: null });
    }
  }

  updateRoute(origin, destination, waypoints) {
    const options = {
      origin, destination, waypoints,
      travelMode: google.maps.TravelMode.DRIVING,
    };

    directionsService.route(options, (result, status) => {
      //TODO: hanlde cross-ocean routes
      if (status === google.maps.DirectionsStatus.OK) {
        this.setState({
          origin, destination, waypoints,
          directions: result,
        });
      }

      else {
        console.error(`error fetching directions ${result}`);
      }
    });
  }

  getContainerElement() {
    return (<div className="google-maps-component"></div>);
  }

  getMapElement() {
    return (<div className="map-element"></div>);
  }

  render() {
    if (!this.state.googleMapsLoaded) {
      return null;
    }

    return (
      <Map
        containerElement={this.getContainerElement()}
        mapElement={this.getMapElement()}
        center={DEFAULT_CENTER}
        directions={this.state.directions}
      />
    );
  }
}

if (process.env.NODE_ENV === 'development') {
  GoogleMapsComponent.propTypes = {
    googleMapsLoaded: React.PropTypes.bool.isRequired,
    origin: React.PropTypes.object,
    destination: React.PropTypes.object,
    waypoints: React.PropTypes.array,
  };
}

export default GoogleMapsComponent;
