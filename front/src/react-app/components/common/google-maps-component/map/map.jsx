import { withGoogleMap, GoogleMap, DirectionsRenderer } from 'react-google-maps/lib';

const SimpleMap = withGoogleMap((props) => (
  <GoogleMap
    defaultZoom={3}
    defaultCenter={props.center}
  >
    {props.directions && <DirectionsRenderer directions={props.directions} />}
  </GoogleMap>
));

export default SimpleMap;