import './panel.less';

const Panel = (props) => {
  return (
    <div className="panel">
      <div className="header">
        <h3>{props.title}</h3>
      </div>
      <div className="body">{props.children}</div>
    </div>
  );
};

if (process.env.NODE_ENV === 'development') {
  Panel.propTypes = {
    title: React.PropTypes.string.isRequired,
  };
}

export default Panel;