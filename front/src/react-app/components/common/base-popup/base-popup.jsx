import './base-popup.less';
import ReactAnimation from 'react-addons-css-transition-group';
import classnames from 'classnames';

const BasePopup = (props) => {
  return (
    <ReactAnimation transitionName="fade" transitionAppearTimeout={200} transitionEnterTimeout={500} transitionLeaveTimeout={500}>
      {
        props.isOpened ?
          <div className={classnames('popup', props.className)}>
            { props.children }
          </div>
          : null
      }
    </ReactAnimation>
  );
};

if (process.env.NODE_ENV === 'development') {
  BasePopup.propTypes = {
    isOpened: React.PropTypes.bool.isRequired,
    className: React.PropTypes.string
  };
}

export default BasePopup;
