class BaseComponent extends React.Component {
  constructor(props) {
    super(props);

    this.onFormValueChange = this.onFormValueChange.bind(this);
    this.inherit = this.inherit.bind(this);
    this.onStateValueChange = this.onStateValueChange.bind(this);
  }

  onFormValueChange(prop) {
    return function (newValue) {
      const form = { ...this.state.form, [prop]: newValue };

      this.setState({ form });
    }.bind(this);
  }

  onStateValueChange(prop) {
    return (newValue) => {
      this.setState({ [prop]: newValue });
    }
  }

  inherit(obj) {
    if (!this.state) {
      this.state = {};
    }

    for (var prop in obj) {
      if (obj.hasOwnProperty(prop) && prop !== 'constructor') {
        this[prop] = obj[prop];
      }
    }

    if (obj.constructor) {
      obj.constructor.call(this);
    }
  }
}

export default BaseComponent;
