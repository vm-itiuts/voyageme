import { Dispatcher } from 'flux';

/**
 * A bridge function between the views and the dispatcher, marking the action
 * as a view action.  Another variant here could be handleServerAction.
 *
 * @param  {string} type - Action constant.
 * @param  {Object} data - The data coming from the view.
 */
class DispatcherClass extends Dispatcher {
  handleViewAction(type, data) {
    this.dispatch({
      source: 'VIEW_ACTION',
      action: { type, data }
    });
  };
}

const AppDispatcher = new DispatcherClass();

export default AppDispatcher;
