import { Provider } from 'react-redux';
import Router from 'containers/router-container';
import DevTools from 'devtools';

class RootContainer extends React.Component {
  render() {
    const { store } = this.props;

    return (
      <Provider store={store}>
        <div>
          <Router />
          <DevTools />
        </div>
      </Provider>
    );
  }
}

export default RootContainer;