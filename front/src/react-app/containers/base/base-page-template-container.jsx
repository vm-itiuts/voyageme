import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import BasePageTemplate from 'components/pages/base-page-template/base-page-template';
import appActions from 'actions/app-actions';
import authActions from 'actions/auth-actions';

class BasePageTemplateContainer extends React.Component {
  render() {
    return (
      <BasePageTemplate
        appState={this.props.app}
        authState={this.props.auth}
        notificationState={this.props.notification}
        {...bindActionCreators(appActions, this.props.dispatch)}
        {...bindActionCreators(authActions, this.props.dispatch)}
      >
        {this.props.children}
      </BasePageTemplate>
    );
  }
}

export default connect(
  (state) => ({
    app: state.app,
    auth: state.auth,
    notification: state.notification,
  })
)(BasePageTemplateContainer);