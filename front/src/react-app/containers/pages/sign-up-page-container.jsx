import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import SignUpPage from 'components/pages/sign-up/sign-up-page';
import authActions from 'actions/auth-actions';
import AUTH_ACTIONS from 'constants/actions/auth-action-constants';

const mapStateToProps = state => ({
  signUpValidationErrors: state.error.validationErrors[AUTH_ACTIONS.SIGN_UP] || {},
});

const mapDispatchToProps = dispatch => ({
  ...bindActionCreators({
    signUp: authActions.signUp
  }, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(SignUpPage);