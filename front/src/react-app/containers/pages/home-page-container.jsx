import { connect } from 'react-redux';
import HomePage from 'components/pages/home/home-page';
import appActions from 'actions/app-actions';

class HomePageContainer extends React.Component {
  render() {
    return (
      <HomePage
        openPopup={this.props.openPopup}
        googleMapsLoaded={this.props.googleMapsLoaded}
        isAuthenticated={this.props.isAuthenticated}
      />
    );
  }
}

export default connect(
  (state) => ({
    googleMapsLoaded: state.app.googleMapsLoaded,
    isAuthenticated: state.auth.isAuthenticated,
  }),
  { openPopup: appActions.openPopup }
)(HomePageContainer);