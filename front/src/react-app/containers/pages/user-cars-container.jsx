import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import UserCarsPage from 'components/pages/user-cars/user-cars-page';
import carActions from 'actions/car-actions';

import CAR_ACTIONS from 'constants/actions/car-action-constants';

const mapStateToProps = state => ({
  cars: state.car.cars,
  addCarValidationErrors: state.error.validationErrors[CAR_ACTIONS.ADD_CAR] || {},
  editCarValidationErrors: state.error.validationErrors[CAR_ACTIONS.EDIT_CAR] || {},
});

const mapDispatchToProps = dispatch => ({ ...bindActionCreators(carActions, dispatch) });

export default connect(mapStateToProps, mapDispatchToProps)(UserCarsPage);