import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import CreateRoutePage from 'components/pages/create-route/create-route-page';
import routeActions from 'actions/route-actions';
import carActions from 'actions/car-actions';
import ROUTE_ACTIONS from 'constants/actions/route-action-constants';

const mapStateToProps = state => ({
  googleMapsLoaded: state.app.googleMapsLoaded,
  transports: state.car.cars,
  createRouteValidationErrors: state.error.validationErrors[ROUTE_ACTIONS.CREATE_ROUTE] || {},
});

const mapDispatchToProps = dispatch => ({
  ...bindActionCreators({
    ...routeActions,
    getCars: carActions.getCars,
  }, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(CreateRoutePage);