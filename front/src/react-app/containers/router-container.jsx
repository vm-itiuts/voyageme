import { connect } from 'react-redux';
import AppRouter from 'router'

const mapStateToProps = state => ({
  isAuthenticated: state.auth.isAuthenticated,
});

export default connect(mapStateToProps)(AppRouter);