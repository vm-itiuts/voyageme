let rootContainer;

if (process.env.NODE_ENV === 'production') {
  rootContainer = require('./root-container.production.jsx').default;
} else {
  rootContainer = require('./root-container.development.jsx').default;
}

export default rootContainer;