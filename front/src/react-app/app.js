import 'bootstrap-grid-only/bootstrap.css';
import 'rc-collapse/assets/index.css';
import 'styles/main.less';

// polyfill the global environment if browser not support Promise
import es6Promise from 'es6-promise';
if (!Promise) {
  window.Promise = es6Promise.Promise;
}

import { render } from 'react-dom';
import RootContainer from 'containers/root-container';
import store from 'store/store';

render(
  <div id="content-wrapper">
    <RootContainer store={store} />
  </div>,
  document.getElementById('react-app')
);
