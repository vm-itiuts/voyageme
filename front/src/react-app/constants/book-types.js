const BOOK_TYPES = {
  MANUAL: { label: 'Ручной', value: 1 },
  AUTOMATIC: { label: 'Автоматический', value: 2 },
  CODE: { label: 'С кодом', value: 3 },
};

export default BOOK_TYPES;