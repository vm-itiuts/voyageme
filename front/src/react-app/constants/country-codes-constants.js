const COUNTRY_CODES = [
  
  // format and regExp not include country code
  { label: 'Belarus(+375)', value: '375', regExp: /^\d{9}$/, format: 'XXXXXXXXX' }
];

export default COUNTRY_CODES;
