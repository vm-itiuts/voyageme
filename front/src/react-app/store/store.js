let configureStore;

if (process.env.NODE_ENV === 'production') {
  configureStore = require('./store.production').default;
} else {
  configureStore = require('./store.development').default;
}

const store = configureStore();

export default store;