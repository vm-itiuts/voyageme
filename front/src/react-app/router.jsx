import Route from 'react-router/lib/Route';
import Router from 'react-router/lib/Router';
import IndexRoute from 'react-router/lib/IndexRoute';
import history from 'history';

// import app modules (page containers)
import BasePageTemplateContainer from 'containers/base/base-page-template-container';
import HomePageContainer from 'containers/pages/home-page-container';
import CreateRoutePageContainer from 'containers/pages/create-route-page-container';
import SignUpPageContainer from 'containers/pages/sign-up-page-container';
import UserCarsPageContainer from 'containers/pages/user-cars-container.jsx';
import ProfilePage from 'components/pages/profile/profile-page';

export default class AppRouter extends React.Component {
  constructor(props) {
    super(props);
    
    this.preventUnauthorizedAccess = this.preventUnauthorizedAccess.bind(this);
    this.preventAuthorizedAccess = this.preventAuthorizedAccess.bind(this);
  }
  
  preventUnauthorizedAccess(a, replace) {
    if (!this.props.isAuthenticated) {
      replace('/');
    }
  }

  preventAuthorizedAccess(a, replace) {
    if (this.props.isAuthenticated) {
      replace('/');
    }
  }

  render() {
    return (
      <Router onUpdate={() => window.scrollTo(0, 0)} history={history}>
        <Route path="/" component={BasePageTemplateContainer}>
          <IndexRoute component={HomePageContainer} />

          <Route path="about" component={HomePageContainer} />
          <Route path="contacts" component={HomePageContainer} />
          <Route path="offers" component={HomePageContainer} />
          <Route path="help" component={HomePageContainer} />
          <Route path="create-route" component={CreateRoutePageContainer} onEnter={this.preventUnauthorizedAccess} />
          <Route path="sign-up" component={SignUpPageContainer} onEnter={this.preventAuthorizedAccess}/>

          <Route path="profile" component={ProfilePage} onEnter={this.preventUnauthorizedAccess} />
          <Route path="settings" component={HomePageContainer} onEnter={this.preventUnauthorizedAccess} />
          <Route path="user-cars" component={UserCarsPageContainer} onEnter={this.preventUnauthorizedAccess} />
        </Route>
      </Router>
    );
  }
}
