const stringHelper = {
  format(string, values) {
    let result = string;

    values.forEach(function (value, index) {
      result = result.replace('{' + index + '}', value);
    });

    return result;
  }
};

export default stringHelper;
