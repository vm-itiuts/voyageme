import _ from 'helpers/local-lodash';
import stringHelper from '../helpers/string-helper';
import messages from 'resources/messages';

function Validation(value, fieldName) {
  this.value = value;
  this.fieldName = fieldName;
}

/**
 * @static
 * @returns {Promise} If there is an error - reject Object of format {username: ['error message']},
 * otherwise resolve unchanged form object
 */
Validation.handleForm = function (form) {
  var errors = {};
  var hasError = false;

  _.forOwn(form, function (value, prop) {
    if (value.hasError) {
      errors[prop] = value.errorMessages;
      hasError = true;
    }
  });

  if (hasError) {
    return Promise.reject(errors);
  }

  return Promise.resolve(form);
};

Validation.prototype.required = function () {
  if (!this.value) {
    this._appendError(messages.required);
  }

  return this;
};

Validation.prototype.min = function (minValue) {
  if (this.value < minValue) {
    const errorMessage = stringHelper.format(messages.min, [minValue]);
    this._appendError(errorMessage);
  }

  return this;
};

Validation.prototype.max = function (minValue) {
  if (this.value > minValue) {
    const errorMessage = stringHelper.format(messages.max, [minValue]);
    this._appendError(errorMessage);
  }

  return this;
};

Validation.prototype.minLength = function (minValue) {
  if (this.value.length < minValue) {
    const errorMessage = stringHelper.format(messages.minLength, [minValue]);
    this._appendError(errorMessage);
  }

  return this;
};

Validation.prototype.maxLength = function (maxValue) {
  if (this.value.length > maxValue) {
    const errorMessage = stringHelper.format(messages.maxLength, [maxValue]);
    this._appendError(errorMessage);
  }

  return this;
};

Validation.prototype.passwordMismatch = function (repeatPassword) {
  if (this.value !== repeatPassword) {
    this._appendError(messages.passwordMismatch);
  }

  return this;
};

Validation.prototype.isInt = function () {
  const value = Number(this.value);
  const isInt = typeof value === 'number'
    && isFinite(value)
    && Math.floor(value) === value;
  
  if (!isInt) {
    this._appendError(messages.notInt);
  }

  return this;
};

Validation.prototype.isEmail = function () {
  const regExp = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  const validEmail = regExp.test(this.value);

  if (!validEmail) {
    this._appendError(messages.invalidEmail);
  }

  return this;
};

Validation.prototype.length = function (length) {
  if (this.value.length !== length) {
    this._appendError(stringHelper.format(messages.length, [length]));
  }

  return this;
};

Validation.prototype.isPhoneNumber = function (regExp, format) {
  const validPhoneNumber = regExp.test(this.value);

  if (!validPhoneNumber) {
    this._appendError(stringHelper.format(messages.invalidPhoneNumber, [format]));
  }

  return this;
};

Validation.prototype._appendError = function (errorMessage) {
  this.hasError = true;

  if (this.errorMessages && this.errorMessages.length) {
    this.errorMessages.push(errorMessage);
  } else {
    this.errorMessages = [errorMessage];
  }
};

Validation.prototype.constructor = Validation;

export default Validation;
