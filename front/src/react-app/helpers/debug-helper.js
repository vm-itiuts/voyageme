const debugHelper = {
  getPropTypeValidationError(propName, componentName) {
    return 'Invalid prop `' + propName + '` supplied to' +
      ' `' + componentName + '`. Validation failed.';
  }
};

export default debugHelper;