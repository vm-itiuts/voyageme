//import { something } from 'lodash';

const localLodash = {
  /**
   * forEach loop for objects
   */
  forOwn(object, callback, context) {
    context = context === undefined ? null : context;

    for (let prop in object) {
      if (object.hasOwnProperty(prop)) {
        callback.call(context, object[prop], prop);
      }
    }
  },

  /**
   * map method for objects
   */
  mapObject(object, callback, context) {
    context = context === undefined ? null : context;
    let array = [];

    for (let prop in object) {
      if (object.hasOwnProperty(prop)) {
        array.push(callback.call(context, object[prop], prop));
      }
    }

    return array;
  },

  isObject(param) {
    return typeof param === 'object' && param !== null;
  },
};

export default localLodash;
