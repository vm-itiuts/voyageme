import superagent from 'superagent';

import errorActions from 'actions/error-actions';
import { authActionCreators } from 'actions/auth-actions';

let authorized = false;

const request = {
  send(dispatch, url, method, data) {
    const headers = localStorage.accessToken ? formAuthorizationHeader(localStorage.accessToken) : {};

    return new Promise((resolve, reject) => {
      superagent(method, url)
        .send(data)
        .set(headers)
        .end(function (err, res) {
          return err ? resolve({ error: handleError(res, dispatch) }) : resolve({ success: res });
        });
    });
  },

  signUp(dispatch, form) {
    return this.send(dispatch, '/api/register', 'POST', form)
      .then((response) => {
        if (response.success) {
          setAuthorizedStatus(response.success.body);
        }

        return response;
      });
  },

  auth(dispatch, email, password) {
    return this.send(dispatch, '/api/login', 'POST', { email, password })
      .then((response) => {
        if (response.success) {
          setAuthorizedStatus(response.success.body);
        }

        return response;
      });
  },

  // TODO: send logout request here
  logOut() {
    delete localStorage.accessToken;
    delete localStorage.refreshToken;
    delete localStorage.user;
    authorized = false;
  },

  refreshToken(dispatch) {
    const refreshToken = localStorage.refreshToken;
    const accessToken = localStorage.accessToken;

    if (!refreshToken && !accessToken) {
      this.logOut();
      return Promise.reject();
    }

    return new Promise((resolve, reject) => {
      superagent('POST', '/api/unp/auth/refresh')
        .send({ refreshToken })
        .set(formAuthorizationHeader(accessToken))
        .end((err, res) => {
          const body = res.body;

          if (err) {
            if (res.status === 500) {
              dispatch(errorActions.setInternalServerError());
              dispatch(authActionCreators.logOut());
            }

            else if (res.status === 400) {
              var subcode = body.error_code;

              // CODE 21: The refresh token doesn't owned by specified access token
              // CODE 27: The refresh token has already been used
              // CODE 29: The refresh token has expired
              if (subcode === 21 || subcode === 27 || subcode === 29) {
                dispatch(authActionCreators.logOut());
              }
            }

            this.logOut();
            return reject();
          }

          // success
          else {
            setAuthorizedStatus(body);
            const showNotification = false;
            dispatch(authActionCreators.auth({ firstName: body.first_name }, showNotification));
            resolve(res);
          }
        });
    });
  }
};

function setAuthorizedStatus(authData) {
  localStorage.accessToken = authData.access_token;
  localStorage.refreshToken = authData.refresh_token;
  localStorage.user = JSON.stringify({ firstName: authData.first_name });
  authorized = true;
}

/**
 * this method created for dividing general errors from specific request errors
 * (i.e general error: 500, 401; specific request error: 400, 404)
 * By this dividing we can stop handling errors if we get 500, 401 status by returning 'undefined'.
 * Otherwise method returns response object.
 *
 * @returns {undefined|Object} returns Object if error will be handled further, otherwise return undefined
 */
function handleError(response, dispatch) {
  if (response.status === 500) {
    dispatch(errorActions.setInternalServerError());
  }

  else if (response.status === 401) {
    // TODO: handle 401
  }

  else {
    return response;
  }
}

function formAuthorizationHeader(accessToken) {
  return { Authorization: 'bearer ' + accessToken };
}

export default request;