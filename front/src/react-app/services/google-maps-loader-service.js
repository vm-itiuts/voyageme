const googleMapsLoaderService = {
  load(onSuccess) {
    const googleMapsScript = document.createElement('script');
    const key = 'AIzaSyDAEMoCtio3I-xScNn1yPDP3TbiBkB4og8';

    googleMapsScript.src = 'http://maps.google.com/maps/api/js?key=' + key + '&libraries=geometry,places,visualization';
    document.body.appendChild(googleMapsScript);

    googleMapsScript.onload = function () {
      onSuccess();
    };
  }
};

export default googleMapsLoaderService;
