import CarStore from 'stores/car-store';
import RouteStore from 'stores/route-store';

const carStore = new CarStore();
const routeStore = new RouteStore();

const storeFactory = {
  car: carStore,
  route: routeStore,
};

export default storeFactory;
