import carActions from 'actions/car-actions';
import routeActions from 'actions/route-actions';

const actionFactory = {
  car: carActions,
  route: routeActions,
};

export default actionFactory;