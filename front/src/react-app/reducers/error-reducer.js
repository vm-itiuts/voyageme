import ERROR_ACTIONS from 'constants/actions/error-action-constants';

const defaultState = {
  validationErrors: {},
};

function errorReducer(state = defaultState, action) {
  switch (action.type) {
    case ERROR_ACTIONS.VALIDATION_ERROR:
      return handlers.validationErrors(state, action.where, action.errors);

    default:
      return state;
  }
}

const handlers = {
  validationErrors(state, where, errors) {
    return {
      ...state,
      validationErrors: {
        [where]: errors,
      },
    };
  },
};

export default errorReducer;
