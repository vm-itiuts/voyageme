import ROUTE_ACTIONS from 'constants/actions/route-action-constants';
import history from 'history';

function getDefaultState() {
  return {};
}

function routeReducer(state = getDefaultState(), action) {
  switch (action.type) {
    case ROUTE_ACTIONS.CREATE_ROUTE:
      return handlers.createRoute(state, state.route);

    default:
      return state;
  }
}

const handlers = {
  createRoute(state, route) {
    history.push('/');
    return state;
  }
};

export default routeReducer;