import { combineReducers } from 'redux';

// reducers
import app from './app-reducer';
import auth from './auth-reducer';
import error from './error-reducer';
import notification from './notification-reducer';
import car from  './car-reducer';
import route from  './route-reducer';

const rootReducer = combineReducers({
  app,
  auth,
  error,
  notification,
  car,
  route,
});

export default rootReducer;