import history from 'history';
import AUTH_ACTIONS from 'constants/actions/auth-action-constants';

function getDefaultState() {
  let defaultState = {
    isAuthenticated: false,
    userData: {},
  };

  /**
   * Set auth state if localStorage contains user data. This will be updated after first request to server.
   * But for the app start we need it because user will be redirected from secured pages after page refresh
   */
  if (localStorage.user) {
    defaultState.isAuthenticated = true;
    defaultState.userData = JSON.parse(localStorage.user);
  }

  return defaultState;
}

function authReducer(state = getDefaultState(), action) {
  switch (action.type) {
    case AUTH_ACTIONS.SIGN_UP:
      return handlers.signUp(state, action.userData);

    case AUTH_ACTIONS.AUTH:
      return handlers.auth(state, action.userData);

    case AUTH_ACTIONS.LOG_OUT:
      return handlers.logOut(state);

    default:
      return state;
  }
}

const handlers = {
  signUp(state, userData) {
    history.push('/');
    return this.auth(state, userData);
  },

  auth(state, userData) {
    return {
      ...state,
      userData: {
        firstName: userData.firstName,
      },
      isAuthenticated: true,
    };
  },

  logOut(state) {
    history.push('/');
    return {
      ...state,
      userData: {},
      isAuthenticated: false,
    };
  },
};

export default authReducer;
