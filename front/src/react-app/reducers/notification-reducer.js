import ERROR_ACTIONS from 'constants/actions/error-action-constants';
import AUTH_ACTIONS from 'constants/actions/auth-action-constants';
import CAR_ACTION_CONSTANTS from 'constants/actions/car-action-constants';
import NOTIFICATION_ACTIONS from 'constants/actions/notification-action-constants';
import ROUTE_ACTIONS from 'constants/actions/route-action-constants';
import messages from 'resources/messages';

const defaultState = {
  notifications: [],
};

function appReducer(state = defaultState, action) {
  switch (action.type) {
    case NOTIFICATION_ACTIONS.NOTIFICATION:
      return handlers.handleComingNotifications(state, action.notifications);

    case AUTH_ACTIONS.SIGN_UP:
      return handlers.generateNotification(state, 'success', messages.successSignUp);

    case AUTH_ACTIONS.AUTH:
      return handlers.generateNotification(state, 'success', messages.successAuth, action.showNotification);

    case CAR_ACTION_CONSTANTS.REMOVE_CAR:
      return handlers.generateNotification(state, 'success', messages.successRemoveCar);

    case CAR_ACTION_CONSTANTS.ADD_CAR:
      return handlers.generateNotification(state, 'success', messages.successAddCar);

    case CAR_ACTION_CONSTANTS.EDIT_CAR:
      return handlers.generateNotification(state, 'success', messages.successEditCar);

    case ERROR_ACTIONS.INTERNAL_SERVER_ERROR:
      return handlers.generateNotification(state, 'error', messages.internalServerError);

    case ROUTE_ACTIONS.CREATE_ROUTE:
      return handlers.generateNotification(state, 'success', messages.successCreateRoute);

    default:
      return state;
  }
}

const handlers = {
  handleComingNotifications(state, comingNotifications) {
    return {
      ...state,
      notifications: comingNotifications,
    };
  },

  generateNotification(state, type, message, showNotification = true) {
    if (!showNotification) {
      return state;
    }

    return {
      ...state,
      notifications: [{ level: type, message }],
    };
  }
};

export default appReducer;
