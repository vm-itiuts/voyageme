import _ from 'helpers/local-lodash';
import APP_ACTIONS from 'constants/actions/app-action-constants';
import AUTH_ACTIONS from 'constants/actions/auth-action-constants';

const defaultState = {
  googleMapsLoaded: false,

  isSidebarOpened: false,

  isBackdropShown: false,

  popupState: {
    auth: false,
  },
};

function appReducer(state = defaultState, action) {
  switch (action.type) {
    case APP_ACTIONS.GOOGLE_MAPS_LOADED:
      return handlers.googleMapsLoaded(state);

    case APP_ACTIONS.OPEN_SIDEBAR:
      return handlers.openSidebar(state);

    case APP_ACTIONS.CLOSE_SIDEBAR:
      return handlers.closeSidebar(state);

    case APP_ACTIONS.OPEN_POPUP:
      return handlers.openPopup(state, action.popup);

    case APP_ACTIONS.CLOSE_POPUP:
      return handlers.closePopup(state, action.popup);

    case APP_ACTIONS.CLOSE_ALL_POPUPS:
    case AUTH_ACTIONS.AUTH:
      return handlers.closeAllPopups(state);

    default:
      return state;
  }
}

const handlers = {
  googleMapsLoaded(state) {
    return {
      ...state,
      googleMapsLoaded: true,
    };
  },

  openSidebar(state) {
    let result = {
      ...state,
      isSidebarOpened: true,
    };
    result.isBackdropShown = getBackdropState(result);

    return result;
  },

  closeSidebar(state) {
    let result = {
      ...state,
      isSidebarOpened: false,
    };
    result.isBackdropShown = getBackdropState(result);

    return result;
  },

  openPopup(state, popup) {
    let result = {
      ...state,
      popupState: {
        ...state.popupState,
        [popup]: true
      }
    };
    result.isBackdropShown = getBackdropState(result);

    return result;
  },

  closePopup(state, popup) {
    let result = {
      ...state,
      popupState: {
        ...state.popupState,
        [popup]: false
      }
    };
    result.isBackdropShown = getBackdropState(result);

    return result;
  },

  closeAllPopups(state) {
    let popupState = {};

    _.forOwn(state.popupState, function (value, prop) {
      popupState[prop] = false;
    });

    let result = { ...state, popupState };
    result.isBackdropShown = getBackdropState(result);

    return result;
  },
};

function getBackdropState(state) {
  return state.isSidebarOpened || isAnyPopupOpened(state);
}

function isAnyPopupOpened(state) {
  var isAnyPopupOpened = false;

  _.forOwn(state.popupState, function (isOpened) {
    if (isOpened) {
      isAnyPopupOpened = true;
    }
  });

  return isAnyPopupOpened;
}

export default appReducer;
