import CAR_ACTIONS from 'constants/actions/car-action-constants';

function getDefaultState() {
  return {
    cars: [],
  };
}

function carReducer(state = getDefaultState(), action) {
  switch (action.type) {
    case CAR_ACTIONS.ADD_CAR:
      return handlers.addCar(state, action.car);

    case CAR_ACTIONS.EDIT_CAR:
      return handlers.editCar(state, action.car);

    case CAR_ACTIONS.REMOVE_CAR:
      return handlers.removeCar(state, action.id);

    case CAR_ACTIONS.GET_CARS:
      return handlers.getCars(state, action.cars);

    default:
      return state;
  }
}

const handlers = {
  addCar(state, newCar) {
    const cars = state.cars.slice();
    cars.push(newCar);

    return { ...state, cars };
  },

  editCar(state, editedCar) {
    const editedCarIndex = state.cars.findIndex((i) => i.id === editedCar.id);
    const cars = state.cars.slice();
    cars[editedCarIndex] = editedCar;

    return { ...state, cars };
  },

  removeCar(state, carId) {
    const removedCarIndex = state.cars.findIndex((i) => i.id === carId);
    const cars = state.cars.slice();
    cars.splice(removedCarIndex, 1);

    return { ...state, cars };
  },

  getCars(state, cars) {
    return { ...state, cars };
  },
};

export default carReducer;