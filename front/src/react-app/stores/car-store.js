import BaseStore from './base/base-store';

import CAR_ACTION_CONSTANTS from 'constants/actions/car-action-constants';

let cars = [];

class UserStore extends BaseStore {
  constructor(props) {
    super(props);

    this.handledActions = {};
    this.handledActions[CAR_ACTION_CONSTANTS.ADD_CAR] = handleAddCar;
    this.handledActions[CAR_ACTION_CONSTANTS.EDIT_CAR] = handleEditCar;
    this.handledActions[CAR_ACTION_CONSTANTS.REMOVE_CAR] = handleRemoveCar;
    this.handledActions[CAR_ACTION_CONSTANTS.GET_CARS] = handleGetCars;
  }

  getCars() {
    return cars;
  }
}

function handleAddCar(data) {
  cars.push(data);
}

function handleEditCar(data) {
  const id = cars.findIndex((i) => i.id === data.id);
  cars[id] = data;
}

function handleRemoveCar(data) {
  const id = cars.findIndex((i) => i.id === data.id);
  cars.splice(id, 1);
}

function handleGetCars(data) {
  cars = data;
}

export default UserStore;