import AppDispatcher from 'dispatcher/app-dispatcher';
import EventEmitter from 'events';

const CHANGE_EVENT = 'CHANGE_EVENT';

export default class BaseStore extends EventEmitter {
  constructor() {
    super();

    this.handledActions = {};
    this.dispatchToken = AppDispatcher.register(this.handleActionCallback.bind(this));
  }

  emitChange() {
    this.emit(CHANGE_EVENT);
  }

  addChangeListener(callback) {
    this.on(CHANGE_EVENT, callback);
  }

  removeChangeListener(callback) {
    this.removeListener(CHANGE_EVENT, callback);
  }

  handleActionCallback(payload) {
    const action = payload.action;
    const type = action.type;

    if (this.handledActions[type]) {
      this.handledActions[type](action.data);
      this.emitChange();
    }
  }
};
