import history from 'history';
import BaseStore from './base/base-store';

import ROUTE_ACTIONS from 'constants/actions/route-action-constants';

export default class RouteStore extends BaseStore {
  constructor(props) {
    super(props);

    this.handledActions = {};
    this.handledActions[ROUTE_ACTIONS.CREATE_ROUTE] = handleCreateRoute;
  }
}

function handleCreateRoute(data) {
  history.push('/');
}