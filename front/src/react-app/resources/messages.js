const messages = {
  passwordMismatch: 'Пароли не совпадают',
  required: 'Это поле обязательно для заполнения',
  min: 'Минимальное значение не менее {0}',
  max: 'Максимальное значение не более {0}',
  minLength: 'Минимальная длина поля {0}',
  maxLength: 'Максимальная длина поля {0}',
  notInt: 'Значение поля должно быть целым числом',
  length: 'Длина поля должна быть {0}',
  invalidEmail: 'Невалидный email',
  invalidPhoneNumber: 'Валидный формат номера телефона {0}',
  emailAlreadyExists: 'Пользователь с таким email уже существует',
  invalidUsernameOrPassword: 'Неверное имя пользователя или пароль',
  
  successSignUp: 'Вы успешно зарегестрировались',
  successAuth: 'Вы успешно авторизовались',
  
  successAddCar: 'Машина была успешно добавлена',
  successEditCar: 'Данные машины были успешно обновлены',
  successRemoveCar: 'Машина была успешно удалена',
  carNotFound: 'Похоже, у вас уже нет этой машины. Обновите страницу для актуальных данных',
  internalServerError: 'К сожалению что-то пошло не так, повторите попытку позже',

  successCreateRoute: 'Маршрут создан'
};

export default messages;
