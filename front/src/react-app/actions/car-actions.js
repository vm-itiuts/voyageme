import request from 'services/request-service';
import Validation from 'helpers/validation';
import notificationActions from 'actions/notification-actions';
import errorActions from 'actions/error-actions';

import CAR_ACTION_CONSTANTS from 'constants/actions/car-action-constants';
import messages from 'resources/messages';

const carActions = {
  addCar: (car) => ({ type: CAR_ACTION_CONSTANTS.ADD_CAR, car }),
  editCar: (car) => ({ type: CAR_ACTION_CONSTANTS.EDIT_CAR, car }),
  removeCar: (id) => ({ type: CAR_ACTION_CONSTANTS.REMOVE_CAR, id }),
  getCars: (cars) => ({ type: CAR_ACTION_CONSTANTS.GET_CARS, cars })
};

const carAsyncActions = {
  addCar(form) {
    return (dispatch) => {
      return validateAddCarForm(form)
        .then(() => {
          return request.send(dispatch, '/api/transport', 'POST', form)
            .then((response) => {
              dispatch(carActions.addCar(response.success.body))
            })
            .catch((errorResponse) => {
              if (errorResponse.status === 400) {
                return Promise.reject({
                  model: errorResponse.body.model,
                  number: errorResponse.body.number,
                  year: errorResponse.body.year,
                });
              } else {
                console.error('addCar() unexpected error', errorResponse);
              }
            });
        })
        .catch((validationErrors) => {
          dispatch(errorActions.setValidationErrors(CAR_ACTION_CONSTANTS.ADD_CAR, validationErrors));
          return Promise.reject(validationErrors);
        });
    }
  },

  editCar(id, form) {
    return (dispatch) => {
      return validateAddCarForm(form)
        .then(() => {
          return request.send(dispatch, '/api/transport/' + id, 'PUT', form)
            .then((successResponse) => {
              dispatch(carActions.editCar(successResponse.success.body))
            })
            .catch((errorResponse) => {
              console.log('errorResponse', errorResponse);
            });
        })
        .catch((validationErrors) => {
          dispatch(errorActions.setValidationErrors(CAR_ACTION_CONSTANTS.EDIT_CAR, validationErrors));
          return Promise.reject(validationErrors);
        });
    }
  },

  removeCar(id) {
    return (dispatch) => {
      const url = '/api/transport/' + id;

      return request.send(dispatch, url, 'DELETE')
        .then((response) => {
          dispatch(carActions.removeCar(id));
        })
        .catch((errorResponse) => {
          if (errorResponse.status === 404) {
            const notification = [{ level: 'error', message: messages.carNotFound }];
            dispatch(notificationActions.showNotification(notification));
          } else {
            console.error('removeCar() unexpected error', errorResponse);
          }
        });
    }
  },

  getCars() {
    return (dispatch) => {
      return request.send(dispatch, '/api/transport', 'GET')
        .then((response) => {
          dispatch(carActions.getCars(response.success.body))
        })
        .catch((error) => {
          console.error('getCars() unexpected error', error);
        });
    }
  }
};

function validateAddCarForm(form) {
  // car can not be made later then current year
  const MAX_YEAR = new Date().getFullYear();

  const validatedForm = {
    model: new Validation(form.model, 'model')
      .required()
      .maxLength(30),
    number: new Validation(form.number, 'number')
      .required()
      .maxLength(10),
    year: new Validation(form.year, 'year')
      .required()
      .isInt()
      .min(1900)
      .max(MAX_YEAR)
  };

  return Validation.handleForm(validatedForm);
}

export default carAsyncActions;

