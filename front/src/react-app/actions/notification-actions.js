import NOTIFICATION_ACTIONS from 'constants/actions/notification-action-constants';

const notificationActionCreators = {
  showNotification: (notifications) => ({ type: NOTIFICATION_ACTIONS.NOTIFICATION, notifications }),
};

export default notificationActionCreators;