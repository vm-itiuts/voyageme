import ERROR_ACTIONS from 'constants/actions/error-action-constants';

const errorActions = {
  setValidationErrors: (where, errors) => ({ type: ERROR_ACTIONS.VALIDATION_ERROR, where, errors }),

  setInternalServerError: () => ({ type: ERROR_ACTIONS.INTERNAL_SERVER_ERROR })
};

export default errorActions;