import request from 'services/request-service';
import Validation from 'helpers/validation';
import errorActions from 'actions/error-actions';
import notificationActions from 'actions/notification-actions';

import messages from 'resources/messages';
import AUTH_ACTIONS from 'constants/actions/auth-action-constants';

const authActionCreators = {
  signUp: (userData) => ({ type: AUTH_ACTIONS.SIGN_UP, userData }),

  auth: (userData, showNotification) => ({ type: AUTH_ACTIONS.AUTH, userData, showNotification }),

  logOut: () => ({ type: AUTH_ACTIONS.LOG_OUT }),
};

const authActions = {
  signUp(form) {
    return (dispatch) => {
      return validateSignUpForm(form)
        .then(() => {
          form = { ...form };
          form.phoneNumber = form.countryPhoneCode.value + form.phoneNumber;

          return request.signUp(dispatch, form);
        })
        .then((response) => {
          if (response.success) {
            const userData = { firstName: response.success.body.first_name };
            dispatch(authActionCreators.signUp(userData));
          }

          else if (response.error) {

            // status 409: this email already exists
            if (response.error.status === 409) {
              const notifications = [{ level: 'error', message: messages.emailAlreadyExists }];
              dispatch(notificationActions.showNotification(notifications));
            }

            else if (response.error.status === 400) {
              return Promise.reject({
                email: errorResponse.body.Email,
                password: errorResponse.body.Password,
                firstName: errorResponse.body.FirstName,
                secondName: errorResponse.body.SecondName,
                phoneNumber: errorResponse.body.PhoneNumber
              });
            }

            else {
              console.error('signUp() unhandled error', response);
            }
          }
        })
        .catch((errors) => {
          dispatch(errorActions.setValidationErrors(AUTH_ACTIONS.SIGN_UP, errors));
        });
    };
  },

  auth(email, password) {
    return (dispatch) => {
      return request.auth(dispatch, email, password)
        .then((response) => {
          if (response.success) {
            const userData = { firstName: response.success.body.first_name };
            dispatch(authActionCreators.auth(userData));
          }

          else if (response.error) {
            if (response.error.status === 400) {
              const notifications = [{ level: 'error', message: messages.invalidUsernameOrPassword }];
              dispatch(notificationActions.showNotification(notifications));
            }

            else {
              console.error('auth() unhandled error', response);
            }
          }
        });
    };
  },

  refreshToken() {
    return (dispatch) => {
      request.refreshToken(dispatch)
        .catch(() => ({}));
    };
  },

  logOut() {
    return (dispatch) => {
      request.logOut(dispatch);
      dispatch(authActionCreators.logOut());
    };
  }
};

function validateSignUpForm(form) {
  const validatedForm = {
    email: new Validation(form.email, 'email')
      .required()
      .isEmail(),
    firstName: new Validation(form.firstName, 'firstName')
      .required()
      .minLength(2)
      .maxLength(30),
    secondName: new Validation(form.secondName, 'secondName')
      .required()
      .minLength(2)
      .maxLength(30),
    password: new Validation(form.password, 'password')
      .minLength(7)
      .maxLength(30)
      .passwordMismatch(form.repeatPassword),
    phoneNumber: new Validation(form.phoneNumber, 'phoneNumber')
      .required()
      .isPhoneNumber(form.countryPhoneCode.regExp, form.countryPhoneCode.format)
  };

  return Validation.handleForm(validatedForm);
}

export default authActions;

export { authActionCreators };