import APP_ACTIONS from 'constants/actions/app-action-constants';

const appActions = {
  googleMapsLoadedSuccess() {
    return { type: APP_ACTIONS.GOOGLE_MAPS_LOADED };
  },

  openSidebar() {
    return { type: APP_ACTIONS.OPEN_SIDEBAR };
  },

  closeSidebar() {
    return { type: APP_ACTIONS.CLOSE_SIDEBAR };
  },

  openPopup(popup) {
    return { type: APP_ACTIONS.OPEN_POPUP, popup };
  },

  closePopup(popup) {
    return { type: APP_ACTIONS.CLOSE_POPUP, popup };
  },

  closeAllPopups() {
    return { type: APP_ACTIONS.CLOSE_ALL_POPUPS };
  },
};

export default appActions;