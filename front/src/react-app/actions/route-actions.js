import request from 'services/request-service';
import Validation from 'helpers/validation';

import errorActions from 'actions/error-actions';
import ROUTE_ACTIONS from 'constants/actions/route-action-constants';

import BOOK_TYPES from 'constants/book-types';

const actions = {
  createRoute(route) {
    return {
      type: ROUTE_ACTIONS.CREATE_ROUTE,
      route,
    };
  }
}

const asyncActions = {
  createRoute(route) {
    return (dispatch) => {
      return validateCreateRouteForm(route)
        .then(() => {

          return request.send(dispatch, '/api/route', 'POST', route)
            .then(response => {
              dispatch(actions.createRoute(response.success.body))
            })
            .catch(errorResponse => {
              if (errorResponse.status === 400) {
                return Promise.reject({
                  startPlace: errorResponse.body.startPlaceId,
                  endPlace: errorResponse.body.endPlaceId,
                  startDateTime: errorResponse.body.startDateTime,
                  price: errorResponse.body.price,
                  bookType: errorResponse.body.bookTypeId,
                  seatsNumber: errorResponse.body.seatsNumber,
                  transportId: errorResponse.body.transportId,
                  code: errorResponse.body.code
                });
              } else {
                console.error('createRoute() unexpected error', errorResponse);
              }
            })
        })
        .catch(errors => {
          dispatch(errorActions.setValidationErrors(ROUTE_ACTIONS.CREATE_ROUTE, errors));
          return Promise.reject(errors);
        })
    }
  }
}

function validateCreateRouteForm(form) {
  const validatedForm = { //TODO: add waypoints validation
    startPlace: new Validation(form.startPlaceId, 'startPlaceId')
      .required(),
    endPlace: new Validation(form.endPlaceId, 'endPlaceId')
      .required(),
    startDateTime: new Validation(form.startDateTime, 'startDateTime')
      .required(),
    price: new Validation(form.price, 'price')
      .required()
      .min(0),
    bookType: new Validation(form.bookTypeId, 'bookType')
      .required()
      .min(0)
      .isInt(),
    seatsNumber: new Validation(form.seatsNumber, 'seatsNumber')
      .required()
      .min(0)
      .isInt(),
    transportId: new Validation(form.transportId, 'transportId')
      .required()
      .min(0)
      .isInt(),
  };

  if (form.bookTypeId == BOOK_TYPES.CODE.value) {
    validatedForm.code = new Validation(form.code, 'code')
      .required()
      .min(100)
      .max(999)
      .isInt();
  }
  return Validation.handleForm(validatedForm);
}

export default asyncActions;