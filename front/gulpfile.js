const gulp = require('gulp');
const jscs = require('gulp-jscs');

// TODO: add code errors rules(perhaps jshint)
gulp.task('jscs', () => {
  return gulp.src('src/react-app/**/*.js')
    .pipe(jscs({configPath: '.jscsrc'}))
    .pipe(jscs.reporter());
});