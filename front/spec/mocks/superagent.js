const superagentMethods = {
  send: function () {
    return this;
  },
  set: function () {
    return this;
  },
  end: function (callback) {
    callback();
  },
};

module.exports = superagentMethods;