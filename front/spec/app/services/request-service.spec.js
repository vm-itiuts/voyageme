import request from 'services/request-service';
import superagentMethods from '../../mocks/superagent';
import errorActions from 'actions/error-actions';

const dispatchMock = {
  dispatch: () => undefined,
};

const superagentMock = function () {
  return superagentMethods;
};

// TODO: cover another methods
describe('request-service', function () {
  beforeEach(function () {
    request.__Rewire__('superagent', superagentMock);
  });

  describe('method send', function () {
    it('should return resolved promise for success response', function (done) {
      const expectedResponse = {};
      spyOn(superagentMethods, 'end').and.callFake(function (callback) {
        callback(null, expectedResponse);
      });

      request.send(dispatchMock.dispatch)
        .then((response) => {
          expect(response.success).toBe(expectedResponse);
          done();
        });
    });

    it('should return resolved promise with error response if its status not 401 or 500', function (done) {
      const expectedResponse = { status: 400 };
      spyOn(superagentMethods, 'end').and.callFake(function (callback) {
        const error = {};
        callback(error, expectedResponse);
      });

      request.send(dispatchMock.dispatch)
        .then((response) => {
          expect(response.error).toBe(expectedResponse);
          done();
        });
    });

    it('should return resolved promise without success and without error response if its status 401', function (done) {
      const response = { status: 401 };
      spyOn(superagentMethods, 'end').and.callFake(function (callback) {
        const error = {};
        callback(error, response);
      });

      request.send(dispatchMock.dispatch)
        .then((response) => {
          expect(response.success).toBe(undefined);
          expect(response.error).toBe(undefined);
          done();
        });
    });

    it('should return resolved promise without success and without error response if its status 500', function (done) {
      const response = { status: 500 };
      spyOn(superagentMethods, 'end').and.callFake(function (callback) {
        const error = {};
        callback(error, response);
      });

      request.send(dispatchMock.dispatch)
        .then((response) => {
          expect(response.success).toBe(undefined);
          expect(response.error).toBe(undefined);
          done()
        });
    });

    it('should send INTERNAL_SERVER_ERROR action if response status is 500', function (done) {
      const response = { status: 500 };
      spyOn(superagentMethods, 'end').and.callFake(function (callback) {
        const error = {};
        callback(error, response);
      });
      spyOn(dispatchMock, 'dispatch');

      request.send(dispatchMock.dispatch)
        .then(() => {
          expect(dispatchMock.dispatch).toHaveBeenCalledWith(errorActions.setInternalServerError());
          done();
        });
    });
  });
});