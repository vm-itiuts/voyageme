import _ from 'lodash';
import deepfreeze from 'deepfreeze';
import appReducer from 'reducers/app-reducer';
import appActions from 'actions/app-actions';
import { authActionCreators } from 'actions/auth-actions';

describe('app-reducer', function () {
  describe('action GOOGLE_MAPS_LOADED', function () {
    it('should return new state', function () {
      const stateBefore = { googleMapsLoaded: false };
      const stateAfter = { googleMapsLoaded: true };

      deepfreeze(stateBefore);

      expect(
        appReducer(stateBefore, appActions.googleMapsLoadedSuccess())
      ).toEqual(stateAfter);
    });
  });

  describe('action OPEN_SIDEBAR', function () {
    it('should open sidebar', function () {
      const stateBefore = { isSidebarOpened: false };

      deepfreeze(stateBefore);

      const stateAfter = appReducer(stateBefore, appActions.openSidebar());

      expect(stateAfter.isSidebarOpened).toBe(true);
    });

    it('should show backdrop', function () {
      const stateBefore = {
        isSidebarOpened: false,
        isBackdropShown: false,
      };

      deepfreeze(stateBefore);

      const stateAfter = appReducer(stateBefore, appActions.openSidebar());

      expect(stateAfter.isBackdropShown).toBe(true);
    });
  });

  describe('action CLOSE_SIDEBAR', function () {
    it('should close sidebar', function () {
      const stateBefore = { isSidebarOpened: true };

      deepfreeze(stateBefore);

      const stateAfter = appReducer(stateBefore, appActions.closeSidebar());

      expect(stateAfter.isSidebarOpened).toBe(false);
    });

    it('should hide backdrop if there are no opened popups', function () {
      const stateBefore = {
        isSidebarOpened: true,
        isBackdropShown: false,
        popupState: {
          auth: false, notExistingPopup: false,
        }
      };

      const stateAfter = appReducer(stateBefore, appActions.closeSidebar());

      expect(stateAfter.isBackdropShown).toBe(false);
    });

    it('should not hide backdrop if there are opened popups', function () {
      const stateBefore = {
        isSidebarOpened: true,
        isBackdropShown: false,
        popupState: {
          auth: true, notExistingPopup: false,
        }
      };

      const stateAfter = appReducer(stateBefore, appActions.closeSidebar());

      expect(stateAfter.isBackdropShown).toBe(true);
    });
  });

  describe('action OPEN_POPUP', function () {
    it('should open popup', function () {
      const stateBefore = {
        popupState: { auth: false },
      };
      const popup = 'auth';

      deepfreeze(stateBefore);

      const stateAfter = appReducer(stateBefore, appActions.openPopup(popup));

      expect(stateAfter.popupState[popup]).toBe(true);
    });

    it('should show backdrop', function () {
      const stateBefore = {
        popupState: { auth: false },
        isBackdropShown: false,
      };

      deepfreeze(stateBefore);

      const stateAfter = appReducer(stateBefore, appActions.openPopup());

      expect(stateAfter.isBackdropShown).toBe(true);
    });
  });

  describe('action CLOSE_POPUP', function () {
    it('should close popup', function () {
      const popup = 'auth';
      const stateBefore = {
        popupState: { [popup]: true },
      };

      deepfreeze(stateBefore);

      const stateAfter = appReducer(stateBefore, appActions.closePopup(popup));

      expect(stateAfter.popupState[popup]).toBe(false);
    });

    it('should hide backdrop if there are no opened popups and backdrop is not shown', function () {
      const stateBefore = {
        popupState: { auth: true },
        isSidebarOpened: false,
      };
      const popup = 'auth';

      deepfreeze(stateBefore);

      const stateAfter = appReducer(stateBefore, appActions.closePopup(popup));

      expect(stateAfter.isBackdropShown).toBe(false);
    });

    it('should not hide backdrop if there is opened popup after closing another', function () {
      const stateBefore = {
        popupState: {
          auth: true,
          anotherStillOpenedPopup: true
        },
      };
      const popup = 'auth';

      deepfreeze(stateBefore);

      const stateAfter = appReducer(stateBefore, appActions.closePopup(popup));

      expect(stateAfter.isBackdropShown).toBe(true);
    });

    it('should not hide backdrop sidebar still opened', function () {
      const stateBefore = {
        popupState: { auth: true },
        isSidebarOpened: true,
      };
      const popup = 'auth';

      deepfreeze(stateBefore);

      const stateAfter = appReducer(stateBefore, appActions.closePopup(popup));

      expect(stateAfter.isBackdropShown).toBe(true);
    });
  });

  describe('actions CLOSE_ALL_POPUPS and AUTH', function () {
    const actions = {
      CLOSE_ALL_POPUPS: appActions.closeAllPopups(),
      AUTH: authActionCreators.auth(),
    };

    _.forEach(actions, function (action, key) {

      it(key + ' should close all popups', function () {
        const stateBefore = {
          popupState: {
            auth: true, signUp: true, notExistingPopup: true
          },
        };

        deepfreeze(stateBefore);

        const stateAfter = appReducer(stateBefore, action);

        expect(stateAfter.popupState.auth).toBe(false);
        expect(stateAfter.popupState.signUp).toBe(false);
        expect(stateAfter.popupState.notExistingPopup).toBe(false);
      });

      it(key + ' should hide backdrop if sidebar is closed', function () {
        const stateBefore = {
          popupState: {
            auth: true, signUp: true, notExistingPopup: true
          },
          isSidebarOpened: false,
          isBackdropShown: true,
        };

        deepfreeze(stateBefore);

        const stateAfter = appReducer(stateBefore, action);

        expect(stateAfter.isBackdropShown).toBe(false);
      });

      it(key + ' should not hide backdrop if sidebar is opened', function () {
        const stateBefore = {
          popupState: {
            auth: true, signUp: true, notExistingPopup: true
          },
          isSidebarOpened: true,
          isBackdropShown: true,
        };

        deepfreeze(stateBefore);

        const stateAfter = appReducer(stateBefore, action);

        expect(stateAfter.isBackdropShown).toBe(true);
      });
    });
  });
});