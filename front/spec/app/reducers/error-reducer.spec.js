import deepfreeze from 'deepfreeze';
import errorReducer from 'reducers/error-reducer';
import errorActions from 'actions/error-actions';

describe('error-reducer', function () {
  describe('action VALIDATION_ERROR', function () {
    it('should set validation error', function () {
      const stateBefore = { validationErrors: {} };

      deepfreeze(stateBefore);

      const where = 'SOME_ACTION';
      const errors = { field1: {}, field2: {} };
      const stateAfter = errorReducer(stateBefore, errorActions.setValidationErrors(where, errors));
      
      expect(stateAfter.validationErrors[where]).toBe(errors);
    });
  });
});