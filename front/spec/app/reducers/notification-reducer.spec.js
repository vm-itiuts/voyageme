import deepfreeze from 'deepfreeze';
import notificationReducer from 'reducers/notification-reducer';
import notificationActions from 'actions/notification-actions';

import ERROR_ACTIONS from 'constants/actions/error-action-constants';
import AUTH_ACTIONS from 'constants/actions/auth-action-constants';
import CAR_ACTION_CONSTANTS from 'constants/actions/car-action-constants';
import ROUTE_ACTIONS from 'constants/actions/route-action-constants';
import messages from 'resources/messages';

describe('notification-reducer', function () {
  describe('action NOTIFICATION', function () {
    it('should set passed notifications', function () {
      const stateBefore = { notifications: [] };

      deepfreeze(stateBefore);

      const notification = { level: 'success', message: 'some message' };
      const notifications = [notification];
      const stateAfter = notificationReducer(stateBefore, notificationActions.showNotification(notifications));

      expect(stateAfter.notifications[0].level).toBe(notification.level);
      expect(stateAfter.notifications[0].message).toBe(notification.message);
    });
  });

  describe('should show notification on listed actions', function () {
    const actions = [
      { action: AUTH_ACTIONS.SIGN_UP, type: 'success', message: messages.successSignUp },
      { action: AUTH_ACTIONS.AUTH, type: 'success', message: messages.successAuth, showNotification: false },
      { action: CAR_ACTION_CONSTANTS.REMOVE_CAR, type: 'success', message: messages.successRemoveCar },
      { action: CAR_ACTION_CONSTANTS.ADD_CAR, type: 'success', message: messages.successAddCar },
      { action: CAR_ACTION_CONSTANTS.EDIT_CAR, type: 'success', message: messages.successEditCar },
      { action: ERROR_ACTIONS.INTERNAL_SERVER_ERROR, type: 'error', message: messages.internalServerError },
      { action: ROUTE_ACTIONS.CREATE_ROUTE, type: 'success', message: messages.successCreateRoute },
    ];

    actions.forEach(function (action) {
      it('action ' + action.action, function () {
        const stateBefore = { notifications: [] };

        deepfreeze(stateBefore);
        const comingAction = { type: action.action, showNotification: action.showNotification };
        const stateAfter = notificationReducer(stateBefore, comingAction);

        if (action.hasOwnProperty('showNotification') && !action.showNotification) {
          expect(stateAfter.notifications).toEqual(stateBefore.notifications);
        } else {
          expect(stateAfter.notifications[0].level).toBe(action.type);
          expect(stateAfter.notifications[0].message).toBe(action.message);
        }
      });
    });
  });
});