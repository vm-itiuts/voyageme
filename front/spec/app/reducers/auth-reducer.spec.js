import deepfreeze from 'deepfreeze';
import authReducer from 'reducers/auth-reducer';
import { authActionCreators } from 'actions/auth-actions';

describe('auth-reducer', function () {
  describe('action SIGN_UP', function () {
    it('should set auth state and user data', function () {
      const stateBefore = { isAuthenticated: false, userData: {} };

      deepfreeze(stateBefore);

      const userData = { firstName: 'Jack' };
      const stateAfter = authReducer(stateBefore, authActionCreators.signUp(userData));

      expect(stateAfter.isAuthenticated).toBe(true);
      expect(stateAfter.userData.firstName).toBe(userData.firstName);
    });
  });

  describe('action AUTH', function () {
    it('should set auth state and user data', function () {
      const stateBefore = { isAuthenticated: false, userData: {} };

      deepfreeze(stateBefore);

      const userData = { firstName: 'Jack' };
      const stateAfter = authReducer(stateBefore, authActionCreators.auth(userData));

      expect(stateAfter.isAuthenticated).toBe(true);
      expect(stateAfter.userData.firstName).toBe(userData.firstName);
    });
  });

  describe('action LOG_OUT', function () {
    it('should set unauthorized state and reset user data', function () {
      const stateBefore = { isAuthenticated: true, userData: { firstName: 'Andy' } };

      deepfreeze(stateBefore);

      const stateAfter = authReducer(stateBefore, authActionCreators.logOut());

      expect(stateAfter.isAuthenticated).toBe(false);
      expect(stateAfter.userData).toEqual({});
    });
  });
});