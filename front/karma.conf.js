const path = require('path');

const TEST_SRC_FILES = path.resolve(__dirname, 'spec/app');
const APP_SRC_FILES = path.resolve(__dirname, 'src/react-app');

module.exports = function (config) {
  config.set({
    basePath: './',
    frameworks: ['jasmine'],
    files: [
      { pattern: './spec/app/**/*.spec.js', watched: false },
    ],
    exclude: [
      'node_modules'
    ],
    preprocessors: {
      './spec/app/**/*.spec.js': ['webpack'],
    },
    reporters: ['progress'],
    port: 9876,
    colors: true,
    logLevel: config.LOG_INFO,
    autoWatch: true,
    browsers: ['Chrome'],
    singleRun: false,
    webpack: configureWebpack(),
    webpackServer: {
      noInfo: true
    },
  });
};

function configureWebpack() {
  return {
    devtool: 'inline-source-map',
    resolve: {
      extensions: ['', '.js'],
      root: [TEST_SRC_FILES, APP_SRC_FILES]
    },
    module: {
      loaders: [
        {
          test: /\.js$/,
          loader: 'babel',
          exclude: [
            /node_modules/,
          ],
          query: {
            plugins: ['babel-plugin-rewire', 'transform-object-rest-spread'],
            presets: ['es2015', 'react']
          }
        }
      ]
    },
    externals: {
      cheerio: 'window',
      'react/addons': true,
      'react/lib/ExecutionEnvironment': true,
      'react/lib/ReactContext': true
    },
    node: {
      fs: 'empty'
    }
  };
}